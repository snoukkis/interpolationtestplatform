#version 150

in uvec2 vertexPositionAttribute;

out PatchData
{
	uvec2 gridIndex;
} PatchOut;

void main()
{
	PatchOut.gridIndex = vertexPositionAttribute;
	gl_Position = vec4(vertexPositionAttribute, 0.0, 1.0);
}
