#include "PhysicalObject.h"
#include <vector>

std::vector<glm::dvec3> PhysicalObject::getState()
{
	return state;
}

void PhysicalObject::setState(std::vector<glm::dvec3> input)
{
	state = input;
}
