#version 150

layout(triangles) in;
layout(triangle_strip, max_vertices=3) out;

in VertexData
{
	vec3 viewPosition;
	vec2 texCoord;
} VertexIn[];

in VertexFlatData
{
	flat bool stickyPosX;
	flat bool stickyNegX;
	flat bool stickyPosY;
	flat bool stickyNegY;
} VertexFlatIn[];

out FragmentData
{
	vec2 texCoord;
	vec2 realTexCoord;
} FragmentOut;

out FragmentFlatData
{
	flat vec4 nearPosition1;
	flat vec4 nearPosition2;
} FragmentFlatOut;

uniform float offsetten;

uniform bool flipGridBorder;

void main()
{
	FragmentOut.realTexCoord = FragmentOut.texCoord = vec2(0.0, 0.0);
	FragmentFlatOut.nearPosition1 = vec4(0.0, 0.0, 0.0, 1.0);
	FragmentFlatOut.nearPosition2 = vec4(0.0, 0.0, 0.0, 1.0);

	vec4 projectedPosition[3];
	
	bool isScreenEdgePatch = false;
	
	for(int i = 0; i < 3; i++)
	{
		projectedPosition[i] = gl_in[i].gl_Position;
		
		if(!flipGridBorder)
		{
			if(VertexFlatIn[i].stickyPosX)
			{
				isScreenEdgePatch = true;
			
				projectedPosition[i].x = +1.0 * projectedPosition[i].w;
				projectedPosition[i].z -= 0.01 * projectedPosition[i].w;
			}
			else if(VertexFlatIn[i].stickyNegX)
			{
				isScreenEdgePatch = true;
			
				projectedPosition[i].x = -1.0 * projectedPosition[i].w;
				projectedPosition[i].z -= 0.01 * projectedPosition[i].w;
			}
			if(VertexFlatIn[i].stickyPosY)
			{
				isScreenEdgePatch = true;
			
				projectedPosition[i].y = +1.0 * projectedPosition[i].w;
				projectedPosition[i].z -= 0.01 * projectedPosition[i].w;
			}
			else if(VertexFlatIn[i].stickyNegY)
			{
				isScreenEdgePatch = true;
			
				projectedPosition[i].y = -1.0 * projectedPosition[i].w;
				projectedPosition[i].z -= 0.01 * projectedPosition[i].w;
			}
		}
	}
	
	if(isScreenEdgePatch)
	{
		for(int i = 0; i < 3; i++)
		{
			gl_Position = projectedPosition[i];
			
			FragmentOut.realTexCoord = FragmentOut.texCoord = VertexIn[i].texCoord;
			
			EmitVertex();
		}
	}
	else
	{
		vec3 side[3];
		float sideLength[3];
		
		// process sides
		for(int i = 0; i < 3; i++)
		{
			// get side vectors
			side[i] = VertexIn[(i + 1) % 3].viewPosition - VertexIn[(i + 2) % 3].viewPosition;
			sideLength[i] = length(side[i]);
		}
		
		// calculate surface normal
		vec3 normal;
		normal.x = (side[0].y * side[1].z) - (side[0].z * side[1].y);
		normal.y = (side[0].z * side[1].x) - (side[0].x * side[1].z);
		normal.z = (side[0].x * side[1].y) - (side[0].y * side[1].x);
		
		vec3 center = VertexIn[0].viewPosition + VertexIn[1].viewPosition + VertexIn[2].viewPosition;
		//center /= 3.0;
		center = normalize(center);

		// make unit normal
		normal = normalize(normal);
		
		float cosineOfAngle = dot(normal, center);
		float angle = acos(cosineOfAngle);
		float angleDeg = degrees(angle);
		
		bool useBackgroundColor = abs(cosineOfAngle) < 0.087; // about 5 degrees
		//bool useBackgroundColor = abs(angleDeg - 90) < 5;
		//useBackgroundColor = false;
		
		if(useBackgroundColor)
		{
			float distanceFromLastRenderEyePosition[3];
			for(int i = 0; i < 3; i++)
			{
				// distance from eye position
				distanceFromLastRenderEyePosition[i] = length(VertexIn[i].viewPosition);
			}
			
			int farIndex = 0;
			int middleIndex = 1;
			int nearIndex = 2;
			int temp;
			
			// sort by distance
			if(distanceFromLastRenderEyePosition[farIndex] < distanceFromLastRenderEyePosition[nearIndex])
			{
				temp = farIndex;
				farIndex = nearIndex;
				nearIndex = temp;
			}
			if(distanceFromLastRenderEyePosition[farIndex] < distanceFromLastRenderEyePosition[middleIndex])
			{
				temp = farIndex;
				farIndex = middleIndex;
				middleIndex = temp;
			}
			if(distanceFromLastRenderEyePosition[middleIndex] < distanceFromLastRenderEyePosition[nearIndex])
			{
				temp = middleIndex;
				middleIndex = nearIndex;
				nearIndex = temp;
			}
			
			// decide if middle is also pretty far
			
			float middleDistanceFromNear = distanceFromLastRenderEyePosition[middleIndex] - distanceFromLastRenderEyePosition[nearIndex];
			float farDistanceFromNear = distanceFromLastRenderEyePosition[farIndex] - distanceFromLastRenderEyePosition[nearIndex];

			float relativeDistance = middleDistanceFromNear / farDistanceFromNear;
			
			bool middleIsAlsoPrettyFar = relativeDistance > 0.25;
			
			// process vertices
			for(int i = 0; i < 3; i++)
			{
				gl_Position = projectedPosition[i];
				FragmentOut.realTexCoord = VertexIn[i].texCoord;

				FragmentFlatOut.nearPosition1 = projectedPosition[nearIndex];
				if(middleIsAlsoPrettyFar)
				{
					// nearPosition1 and nearPosition2 degenerate to a point
					FragmentFlatOut.nearPosition2 = projectedPosition[nearIndex];
				}
				else
				{
					// nearPosition1 and nearPosition2 form a line
					FragmentFlatOut.nearPosition2 = projectedPosition[middleIndex];
				}

				if(i == farIndex)
				{
					FragmentOut.texCoord = VertexIn[i].texCoord;
				}
				else if(i == middleIndex)
				{
					if(middleIsAlsoPrettyFar)
					{
						FragmentOut.texCoord = VertexIn[i].texCoord;
					}
					else
					{
						FragmentOut.texCoord = VertexIn[farIndex].texCoord;
					}
				}
				else if(i == nearIndex)
				{
					if(middleIsAlsoPrettyFar)
					{
						FragmentOut.texCoord = VertexIn[middleIndex].texCoord;
					}
					else
					{
						FragmentOut.texCoord = VertexIn[farIndex].texCoord;
					}
				}
				
				EmitVertex();
			}
		}
		else
		{
			for(int i = 0; i < 3; i++)
			{
				gl_Position = projectedPosition[i];
				
				FragmentOut.realTexCoord = FragmentOut.texCoord = VertexIn[i].texCoord;
				
				EmitVertex();
			}
		}
	}
	
	EndPrimitive();
}
