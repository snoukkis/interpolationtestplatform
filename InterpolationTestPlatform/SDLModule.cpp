#include "SDLModule.h"

#include "UtilModule.h"

#include <SDL.h>
#include <SDL_ttf.h> 
#include <SDL_syswm.h>
#include <gl\glew.h>
#include <SDL_opengl.h>
#include <stdio.h>
#include <iostream>
#include <sstream>
#include <string>
#include <algorithm>
#define GLM_SWIZZLE
#include <glm/glm.hpp>
#include <glm/gtx/rotate_vector.hpp>

#include <math.h>

SDLModule::SDLModule(int argc, char* argv[])
{
	readArgs(argc, argv);

	//Start up SDL and create window
	if (!init())
	{
		printf("Failed to initialize!\n");
	}

	drawableObjects.push_back(&worldcubeObject);
	drawableObjects.push_back(&sponzaObject);
	drawableObjects.push_back(&bouncyObject);

	worldcubeObject.setDrawEnabled(true);
	sponzaObject.setDrawEnabled(true);
	bouncyObject.setDrawEnabled(false);

	std::string filename = UtilModule::getDateTimeString();
	reportFileName = "report_" + filename + ".csv";
	logFileName = "log_" + filename + ".csv";

	logFile.open(logFileName, std::ios::out | std::ios::app);
	if (logFile.is_open())
	{
		logFile << "timestamp;msg" << std::endl;
	}

	logBuffer.precision(15);
	logFile.precision(15);

	gameModule = new GameModule(reportFileName);
	physicsModule = new PhysicsModule(bouncyObject, bouncyObject);
	glModule = new GLModule(physicsModule, drawableObjects, getScreenWidth(), getScreenHeight());

	//Main loop flag

	//Enable text input
	SDL_StartTextInput();

	resetCamera();

	// start all three timers at the same moment
	appLifetimeTimer.restart();

	masterFrameTimer.restartRelativeTo(appLifetimeTimer);
	physicsUpdateTimer.restartRelativeTo(masterFrameTimer);
	gameStateTimer.restartRelativeTo(masterFrameTimer);

	frameSkipCounter = getFrameSkip();
}

SDLModule::~SDLModule()
{
	if (logFile.is_open())
	{
		logFile << logBuffer.rdbuf();

		logFile << appLifetimeTimer.peekRelativeTo(masterFrameTimer) << ";" << "closing file" << std::endl;

		logFile.close();
	}

	delete glModule;
	delete physicsModule;
	delete gameModule;

	//Destroy window
	SDL_DestroyWindow(gWindow);
	gWindow = NULL;

	//Free the sound effects
	Mix_FreeChunk(hitSound);
	Mix_FreeChunk(missSound);
	Mix_FreeChunk(timeoutSound);
	Mix_FreeChunk(gunSound);

	//Free the music
	Mix_FreeMusic(music);

	//Quit SDL_mixer
	Mix_CloseAudio();

	//Quit SDL_ttf
	TTF_Quit();

	//Quit SDL subsystems
	SDL_Quit();
}

void SDLModule::onHudInfoChanged()
{
	hudInfoChanged = true;
}

void SDLModule::readArgs(int argc, char* argv[])
{
	char keyArray[1024];
	char valueArray[1024];

	for (int i = 1; i < argc; i++)
	{
		char* arg = argv[i];

		std::stringstream ss(arg);

		ss.getline(keyArray, 1024, '=');
		ss.getline(valueArray, 1024, '\0');

		std::string key(keyArray);
		std::string value(valueArray);

		if (key == "targetFps")
		{
			try
			{
				setInitTargetFps(std::stoi(value));
			}
			catch (std::invalid_argument ia)
			{
				std::cout << "value not integer: " << key << "=" << value << std::endl;

				exit(1);
			}
			catch (std::out_of_range oor)
			{
				std::cout << "value out of range: " << key << "=" << value << std::endl;

				exit(1);
			}

			if (getInitTargetFps() == 0)
			{
				setFrameSkip(0x7FFFFFFF); // max signed int 32
			}
			else if (getInitTargetFps() < 0)
			{
				std::cout << "value must be at least zero: " << key << "=" << value << std::endl;

				exit(1);
			}
		}
		else if (key == "frameSkip")
		{
			if (value == "inf" || value == "infinite")
			{
				setFrameSkip(0x7FFFFFFF); // max signed int 32
			}
			else
			{
				try
				{
					setFrameSkip(std::stoi(value));
				}
				catch (std::invalid_argument ia)
				{
					std::cout << "value not integer: " << key << "=" << value << std::endl;

					exit(1);
				}
				catch (std::out_of_range oor)
				{
					std::cout << "value out of range: " << key << "=" << value << std::endl;

					exit(1);
				}

				if (getFrameSkip() < 1)
				{
					std::cout << "value must be larger than zero: " << key << "=" << value << std::endl;

					exit(1);
				}
			}
		}
		else if (key == "screenWidth")
		{
			try
			{
				setScreenWidth(std::stoi(value));
			}
			catch (std::invalid_argument ia)
			{
				std::cout << "value not integer: " << key << "=" << value << std::endl;

				exit(1);
			}
			catch (std::out_of_range oor)
			{
				std::cout << "value out of range: " << key << "=" << value << std::endl;

				exit(1);
			}

			if (getScreenWidth() < 1)
			{
				std::cout << "value must be larger than zero: " << key << "=" << value << std::endl;

				exit(1);
			}
		}
		else if (key == "screenHeight")
		{
			try
			{
				setScreenHeight(std::stoi(value));
			}
			catch (std::invalid_argument ia)
			{
				std::cout << "value not integer: " << key << "=" << value << std::endl;

				exit(1);
			}
			catch (std::out_of_range oor)
			{
				std::cout << "value out of range: " << key << "=" << value << std::endl;

				exit(1);
			}

			if (getScreenHeight() < 1)
			{
				std::cout << "value must be larger than zero: " << key << "=" << value << std::endl;

				exit(1);
			}
		}
		else if (key == "drawHudInfo")
		{
			if (value == "true")
			{
				setDrawHudInfo(true);
			}
			else if (value == "false")
			{
				setDrawHudInfo(false);
			}
			else
			{
				std::cout << "unsupported value: " << key << "=" << value << std::endl;

				exit(1);
			}
		}
		else if (key == "inverseY")
		{
			if (value == "true")
			{
				setInverseY(true);
			}
			else if (value == "false")
			{
				setInverseY(false);
			}
			else
			{
				std::cout << "unsupported value: " << key << "=" << value << std::endl;

				exit(1);
			}
		}
		else if (key == "interpolate")
		{
			if (value == "true")
			{
				setInterpolate(true);
			}
			else if (value == "false")
			{
				setInterpolate(false);
			}
			else
			{
				std::cout << "unsupported value: " << key << "=" << value << std::endl;

				exit(1);
			}
		}
		else if (key == "interpolationInputUpdating")
		{
			if (value == "true")
			{
				setInterpolationInputUpdating(true);
			}
			else if (value == "false")
			{
				setInterpolationInputUpdating(false);
			}
			else
			{
				std::cout << "unsupported value: " << key << "=" << value << std::endl;

				exit(1);
			}
		}
		else if (key == "wireframe")
		{
			if (value == "true")
			{
				setWireframe(true);
			}
			else if (value == "false")
			{
				setWireframe(false);
			}
			else
			{
				std::cout << "unsupported value: " << key << "=" << value << std::endl;

				exit(1);
			}
		}
		else if (key == "fullscreen")
		{
			if (value == "true")
			{
				setFullscreen(true);
			}
			else if (value == "false")
			{
				setFullscreen(false);
			}
			else
			{
				std::cout << "unsupported value: " << key << "=" << value << std::endl;

				exit(1);
			}
		}
		else if (key == "vsync")
		{
			if (value == "true")
			{
				setVsync(true);
			}
			else if (value == "false")
			{
				setVsync(false);
			}
			else
			{
				std::cout << "unsupported value: " << key << "=" << value << std::endl;

				exit(1);
			}
		}
		else if (key == "captureMouse")
		{
			if (value == "true")
			{
				setCaptureMouse(true);
			}
			else if (value == "false")
			{
				setCaptureMouse(false);
			}
			else
			{
				std::cout << "unsupported value: " << key << "=" << value << std::endl;

				exit(1);
			}
		}
		else if (key == "developerMode")
		{
			if (value == "true")
			{
				setDeveloperMode(true);
			}
			else if (value == "false")
			{
				setDeveloperMode(false);
			}
			else
			{
				std::cout << "unsupported value: " << key << "=" << value << std::endl;

				exit(1);
			}
		}
		else
		{
			std::cout << "unsupported key: " << key << "=" << value << std::endl;

			exit(1);
		}
	}
}

bool SDLModule::init()
{
	//Initialization flag
	bool success = true;

	//Initialize SDL
	if (SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		printf("SDL could not initialize! SDL Error: %s\n", SDL_GetError());
		success = false;
	}
	else
	{
		TTF_Init();

		//Initialize SDL_mixer
		if (Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 4096) == -1)
		{
			return false;
		}

		Mix_AllocateChannels(16);

		//Load the music
		music = Mix_LoadMUS("music/158733__panzertank15__spaceambient.wav");

		//If there was a problem loading the music
		if (music == NULL)
		{
			return false;
		}

		Mix_PlayMusic(music, -1);

		hitSound = Mix_LoadWAV("sounds/131660__bertrof__game-sound-correct.wav");
		missSound = Mix_LoadWAV("sounds/142608__autistic-lucario__error.wav");
		timeoutSound = Mix_LoadWAV("sounds/146734__fins__timeout.wav");
		gunSound = Mix_LoadWAV("sounds/47252__nthompson__bad-explosion.wav");
		bounceSound = Mix_LoadWAV("sounds/267882__jaenkleynhans__bouncing-ball-3.wav");
		

		//If there was a problem loading the sound effects
		if ((hitSound == NULL) || (missSound == NULL) || (timeoutSound == NULL) || (gunSound == NULL))
		{
			return false;
		}

		//Use OpenGL 3.1 core

		SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 0);
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
		//SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);


		//Create window
		if (getFullscreen())
		{
			gWindow = SDL_CreateWindow("SDL Tutorial", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, getScreenWidth(), getScreenHeight(), SDL_WINDOW_OPENGL | SDL_WINDOW_FULLSCREEN_DESKTOP);

		}
		else
		{
			gWindow = SDL_CreateWindow("SDL Tutorial", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, getScreenWidth(), getScreenHeight(), SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN);
		}

		if (gWindow == NULL)
		{
			printf("Window could not be created! SDL Error: %s\n", SDL_GetError());
			success = false;
		}
		else
		{
			updateWindowSize();

			if (getInitTargetFps() > 0)
			{
				setFrameSkip(getNearestRefreshRateMultiplier(getInitTargetFps()));
			}

			//int jope = SDL_CaptureMouse(SDL_bool::SDL_TRUE);
			int SDL_SetRelativeMouseModeSucceess = SDL_SetRelativeMouseMode(getCaptureMouse() ? SDL_bool::SDL_TRUE : SDL_bool::SDL_FALSE);

			//Create context
			gContext = SDL_GL_CreateContext(gWindow);
			if (gContext == NULL)
			{
				printf("OpenGL context could not be created! SDL Error: %s\n", SDL_GetError());
				success = false;
			}
			else
			{
				//Print OpenGL version string
				printf("%s\n", glGetString(GL_RENDERER));
				printf("%s\n", glGetString(GL_VERSION));

				//Initialize GLEW
				glewExperimental = GL_TRUE;
				GLenum glewError = glewInit();
				if (glewError != GLEW_OK)
				{
					printf("Error initializing GLEW! %s\n", glewGetErrorString(glewError));
				}

				//Use Vsync
				if (SDL_GL_SetSwapInterval(getVsync() ? 1 : 0) < 0)
				{
					printf("Warning: Unable to set VSync! SDL Error: %s\n", SDL_GetError());
				}
			}
		}
	}

	return success;
}

void SDLModule::getWindowSize(int &w, int &h)
{
	updateWindowSize();

	w = getScreenWidth();
	h = getScreenHeight();
}

int SDLModule::getNearestRefreshRateMultiplier(int fps)
{
	int ret = (int)(getRefreshRate() / (float)fps + 0.5);
	return ret > 0 ? ret : 1;
}

void SDLModule::updateWindowSize()
{
	int width;
	int height;

	SDL_GetWindowSize(gWindow, &width, &height);
	setScreenWidth(width);
	setScreenHeight(height);
	sponzaObject.changeAspectRatio((float)width / (float)height);

	SDL_DisplayMode mode = { SDL_PIXELFORMAT_UNKNOWN, 0, 0, 0, 0 };
	SDL_GetWindowDisplayMode(gWindow, &mode);
	setRefreshRate(mode.refresh_rate);
}

void SDLModule::mainLoop()
{
	//While application is running
	while (!quit)
	{
		//SDL_Delay(4);
		double anyFrameTimespan = masterFrameTimer.restartToAtLeastMoreThan(1.0 / getRefreshRate()); // seconds

		handleEvents();

		updateGameState(); // state might have changed due to: passage of time

		if (frameSkipCounter >= getFrameSkip() || forceRealFrame)
		{
			// reached maximum frameskip, do a real render

			frameSkipCounter = 0;
			forceRealFrame = false;
		}

		if (getInterpolationInputUpdating())
		{
			updateCamera(anyFrameTimespan);
		}

		if (frameSkipCounter == 0)
		{
			// sync physics timer & update physics
			double physicsUpdateTimespan = physicsUpdateTimer.restartRelativeTo(masterFrameTimer);

			if (!getInterpolationInputUpdating())
			{
				updateCamera(physicsUpdateTimespan);
			}

			bool bounce = physicsModule->update(physicsUpdateTimespan);

			if (bounce)
			{
				Mix_PlayChannel(-1, bounceSound, 0);
			}

			if (getFrameSkip() == 1)
			{
				glModule->setPolygonMode(getWireframe());
			}
			else
			{
				// don't use wireframe while interpolating (wee need the shaded data)
				glModule->setPolygonMode(false);
			}

			glModule->renderScene();

			// get data from GPU only if we really need it... (slowish)
			if (getDrawHudInfo() || fireState == FireState::FIRED)
			{
				glModule->readHit();
				GLubyte hitId = glModule->getHitId();

				if (hitId != getHitId())
				{
					setHitId(hitId);
				}
			}

			if (fireState == FireState::FIRED)
			{
				//std::cout << hitIdToString(getHitId()) << " got hit!" << std::endl;

				fireState = hitTarget(getHitId()) ? FireState::HIT : FireState::MISS;

				updateGameState();
			}

			glModule->blitOffscreenToScreen();

			/*
			float targetFps = getRefreshRate() / (float)getNearestRefreshRateMultiplier(fps);
			float actualFps = 1 / physicsUpdateTimespan;
			float fpsRatio = actualFps / targetFps;
			if (fpsRatio < 0.99 || fpsRatio > 1.01)
			{
			printf("WARNING: scene rendering too slow: actual FPS (%f) / target FPS (%f) = %f\n", physicsUpdateTimespan, targetFps, fpsRatio);
			}
			//printf("target fps: %f, target mult: %d\n", getRefreshRate() / (float)getNearestRefreshRateMultiplier(fps), getNearestRefreshRateMultiplier(fps));
			*/
		}
		else
		{
			if (getInterpolate())
			{
				double physicsUpdateTimespan = physicsUpdateTimer.peekRelativeTo(masterFrameTimer);

				glModule->setPolygonMode(getWireframe());
				glModule->warp((float)physicsUpdateTimespan);
				//printf("warp (%d)\n", frameSkipCounter);
				//printf("warp time: %f\n", timeSinceRealRender);

				/*
				float targetFps = getRefreshRate();
				float actualFps = 1 / anyFrameTimespan;
				float fpsRatio = actualFps / targetFps;
				if (fpsRatio < 0.99 || fpsRatio > 1.01)
				{
				printf("WARNING: interpolation rendering too slow: actual FPS (%f) / target FPS (%f) = %f\n", actualFps, targetFps, fpsRatio);
				}
				*/
			}
			else
			{
				glModule->blitOffscreenToScreen();
			}
		}

		glModule->renderHudCrosshair();
		glModule->renderHudFeedback(feedbackType);
		if (getDrawHudInfo())
		{
			if (hudInfoChanged)
			{
				glModule->updateHud(getInfoString());

				hudInfoChanged = false;
			}

			glModule->renderHudInfo();
		}

		if (frameSkipCounter == 0)
		{
			//Update screen
			logBuffer << appLifetimeTimer.peekRelativeTo(masterFrameTimer) << ";" << "swapping normal frame" << std::endl;
			SDL_GL_SwapWindow(gWindow);

			frameSkipCounter++;
		}
		else
		{
			if (getInterpolate())
			{
				//Update screen
				logBuffer << appLifetimeTimer.peekRelativeTo(masterFrameTimer) << ";" << "swapping interpolated frame" << std::endl;
				SDL_GL_SwapWindow(gWindow);

				frameSkipCounter++;
			}
			else
			{
				int framesToSkip = getFrameSkip() - frameSkipCounter;

				float skipS = framesToSkip / (float)getRefreshRate();
				int skipMs = (int)(1000 * skipS + 0.5f); // round
				//int skipMs = (int)(1000 * skipMsF); // round down

				Uint32 delayMs = skipS > 1.0f ? 1000 : skipMs; // cap to 1 second to avoid hanging forever
				logBuffer << appLifetimeTimer.peekRelativeTo(masterFrameTimer) << ";" << "sleeping " << delayMs << "ms" << std::endl;
				SDL_Delay(delayMs);

				frameSkipCounter += framesToSkip;
			}
		}
	}

	//Disable text input
	SDL_StopTextInput();
}

void SDLModule::handleEvents()
{
	//Event handler
	SDL_Event e;

	//Handle events on queue
	while (SDL_PollEvent(&e) != 0)
	{
		//User requests quit
		if (e.type == SDL_QUIT)
		{
			quit = true;
		}
		else if (e.type == SDL_WINDOWEVENT)
		{
			if (e.window.event == SDL_WINDOWEVENT_SIZE_CHANGED)
			{
				updateWindowSize();
				glModule->resizeWindow(getScreenWidth(), getScreenHeight());

				// recalculate world corners
				calculateCorners();
			}
		}
		else if (getDeveloperMode() && e.type == SDL_KEYDOWN && e.key.keysym.sym == SDLK_ESCAPE)
		{
			quit = true;
		}
		else if (getDeveloperMode() && ((e.type == SDL_KEYDOWN && e.key.keysym.sym == SDLK_PAUSE) || (e.type == SDL_MOUSEBUTTONDOWN && e.button.button == SDL_BUTTON_RIGHT)))
		{
			pause = !pause;

			if (pause)
			{
				physicsUpdateTimer.pauseRelativeTo(masterFrameTimer);
				gameStateTimer.pauseRelativeTo(masterFrameTimer);
			}
			else
			{
				physicsUpdateTimer.resumeRelativeTo(masterFrameTimer);
				gameStateTimer.resumeRelativeTo(masterFrameTimer);
			}
		}
		else if (getDeveloperMode() && e.type == SDL_KEYDOWN && e.key.keysym.sym == SDLK_h)
		{
			setDrawHudInfo(!getDrawHudInfo());
		}
		else if (e.type == SDL_KEYDOWN && e.key.keysym.sym == SDLK_y)
		{
			setInverseY(!getInverseY());
		}
		else if (getDeveloperMode() && e.type == SDL_KEYDOWN && e.key.keysym.sym == SDLK_1)
		{
			setFrameSkip(1);
		}
		else if (getDeveloperMode() && e.type == SDL_KEYDOWN && e.key.keysym.sym == SDLK_2)
		{
			setFrameSkip(2);
		}
		else if (getDeveloperMode() && e.type == SDL_KEYDOWN && e.key.keysym.sym == SDLK_3)
		{
			setFrameSkip(3);
		}
		else if (getDeveloperMode() && e.type == SDL_KEYDOWN && e.key.keysym.sym == SDLK_4)
		{
			setFrameSkip(4);
		}
		else if (getDeveloperMode() && e.type == SDL_KEYDOWN && e.key.keysym.sym == SDLK_5)
		{
			setFrameSkip(5);
		}
		else if (getDeveloperMode() && e.type == SDL_KEYDOWN && e.key.keysym.sym == SDLK_6)
		{
			setFrameSkip(6);
		}
		else if (getDeveloperMode() && e.type == SDL_KEYDOWN && e.key.keysym.sym == SDLK_7)
		{
			setFrameSkip(7);
		}
		else if (getDeveloperMode() && e.type == SDL_KEYDOWN && e.key.keysym.sym == SDLK_8)
		{
			setFrameSkip(8);
		}
		else if (getDeveloperMode() && e.type == SDL_KEYDOWN && e.key.keysym.sym == SDLK_9)
		{
			setFrameSkip(9);
		}
		else if (getDeveloperMode() && e.type == SDL_KEYDOWN && e.key.keysym.sym == SDLK_0)
		{
			setFrameSkip(0x7FFFFFFF); // max signed int 32

			if (getWireframe())
			{
				frameSkipCounter = getFrameSkip(); // render 1 more clean frame
			}
		}
		else if (getDeveloperMode() && e.type == SDL_KEYDOWN && e.key.keysym.sym == SDLK_F5)
		{
			setFrameSkip(getNearestRefreshRateMultiplier(15));
		}
		else if (getDeveloperMode() && e.type == SDL_KEYDOWN && e.key.keysym.sym == SDLK_F6)
		{
			setFrameSkip(getNearestRefreshRateMultiplier(30));
		}
		else if (getDeveloperMode() && e.type == SDL_KEYDOWN && e.key.keysym.sym == SDLK_F7)
		{
			setFrameSkip(getNearestRefreshRateMultiplier(60));
		}
		else if (getDeveloperMode() && e.type == SDL_KEYDOWN && e.key.keysym.sym == SDLK_F8)
		{
			setFrameSkip(getNearestRefreshRateMultiplier(120));
		}
		else if (getDeveloperMode() && e.type == SDL_KEYDOWN && e.key.keysym.sym == SDLK_BACKSPACE)
		{
			setWireframe(!getWireframe());
		}
		else if (getDeveloperMode() && e.type == SDL_KEYDOWN && e.key.keysym.sym == SDLK_KP_DIVIDE)
		{
			setInterpolate(!getInterpolate());
		}
		else if (getDeveloperMode() && e.type == SDL_KEYDOWN && e.key.keysym.sym == SDLK_KP_MULTIPLY)
		{
			setInterpolationInputUpdating(!getInterpolationInputUpdating());
		}
		else if (getDeveloperMode() && e.type == SDL_KEYDOWN && e.key.keysym.sym == SDLK_KP_PLUS)
		{
			setFrameSkip(getFrameSkip() + 1);
		}
		else if (getDeveloperMode() && e.type == SDL_KEYDOWN && e.key.keysym.sym == SDLK_KP_MINUS)
		{
			setFrameSkip(getFrameSkip() - 1);
			if (getFrameSkip() < 1)
			{
				setFrameSkip(1);
			}
		}
		else if (getDeveloperMode() && e.type == SDL_KEYDOWN && e.key.keysym.sym == SDLK_UP)
		{
			physicsModule->shiftSystem(true);
		}
		else if (getDeveloperMode() && e.type == SDL_KEYDOWN && e.key.keysym.sym == SDLK_DOWN)
		{
			physicsModule->shiftSystem(false);
		}
		else if (getDeveloperMode() && e.type == SDL_KEYDOWN && e.key.keysym.sym == SDLK_c)
		{
			setCaptureMouse(!getCaptureMouse());

			int SDL_SetRelativeMouseModeSucceess = SDL_SetRelativeMouseMode(getCaptureMouse() ? SDL_bool::SDL_TRUE : SDL_bool::SDL_FALSE);
		}
		else if (getDeveloperMode() && e.type == SDL_KEYDOWN && e.key.keysym.sym == SDLK_r)
		{
			resetGame();
		}
		else if (getDeveloperMode() && e.type == SDL_KEYDOWN && e.key.keysym.sym == SDLK_k)
		{
			glModule->changeOffsetten(-1.0f / (1));
		}
		else if (getDeveloperMode() && e.type == SDL_KEYDOWN && e.key.keysym.sym == SDLK_l)
		{
			glModule->changeOffsetten(+1.0f / (1));
		}
		else if (getDeveloperMode() && e.type == SDL_KEYDOWN && e.key.keysym.sym == SDLK_F11)
		{
			setFullscreen(!getFullscreen());

			if (getFullscreen())
			{
				int ret = SDL_SetWindowFullscreen(gWindow, SDL_WINDOW_FULLSCREEN_DESKTOP);
			}
			else
			{
				int ret = SDL_SetWindowFullscreen(gWindow, 0);
			}
		}
		else if (e.type == SDL_KEYDOWN && e.key.keysym.sym == SDLK_RETURN)
		{
			if (gameModule->isTutorial())
			{
				gameModule->endTutorial();

				fireState = FireState::FIRED; // end possibly pending tutorial shot

				updateGameState(); // state might have changed due to: tutorial ended
			}
		}
		else if ((e.type == SDL_KEYDOWN && e.key.keysym.sym == SDLK_SPACE) ||
			(e.type == SDL_MOUSEBUTTONDOWN && e.button.button == SDL_BUTTON_LEFT))
		{
			if (!pause)
			{
				fireState = FireState::FIRED;

				updateGameState(); // state might have changed due to: gun fired
			}
		}
	}
}

void SDLModule::updateGameState()
{
	bool stateChanged;

	do
	{
		stateChanged = false;

		if (gameState == GameState::INTERLUDE)
		{
			if (bouncyObject.getDrawEnabled() && gameStateTimer.peekRelativeTo(masterFrameTimer) > 0.5)
			{
				bouncyObject.setDrawEnabled(false);
				feedbackType = GLModule::FeedbackType::INVISIBLE;
				forceRealFrame = true;
			}

			// interlude timeout reached
			if (gameStateTimer.peekRelativeTo(masterFrameTimer) > gameModule->getInterludeDuration())
			{
				forceRealFrame = true;
				// transition action
				gameStateTimer.restartRelativeTo(masterFrameTimer); // switch from timing interlude to timing shooting timeout
				shot = gameModule->beginShot(); // get specifications for this shot from game
				shot.aimingStarted = appLifetimeTimer.peekRelativeTo(masterFrameTimer);

				setFrameSkip(shot.frameSkip);
				setInterpolate(shot.interpolating);

				glm::dvec3 size = worldOuterPositionMax - worldInnerPositionMax;
				size = glm::abs(size);
				double radius = max(size.x, size.y);

				glm::dvec2 limitMin = worldInnerPositionMin.xy;
				glm::dvec2 limitMax = worldInnerPositionMax.xy;

				glm::dvec2 position(shot.targetInitialX, shot.targetInitialY);
				position.x *= limitMax.x - limitMin.x;
				position.y *= limitMax.y - limitMin.y;
				position += limitMin;
				double depth = (worldInnerPositionMin.z + worldInnerPositionMax.z) / 2.0;

				glm::dvec4 velocity(1, 0, 0, 1.0);
				velocity = glm::rotateZ(velocity, shot.targetDirection);
				glm::normalize(velocity);
				velocity *= shot.targetVelocity * max(limitMax.x - limitMin.x, limitMax.y - limitMin.y);

				physicsUpdateTimer.resumeRelativeTo(masterFrameTimer);
				resetCamera();
				resetScene(limitMin, limitMax, position, depth, velocity.xy, radius);

				bouncyObject.setDrawEnabled(true);
				
				// state transition: INTERLUDE -> AIMING
				//std::cout << "State transition: INTERLUDE -> AIMING. difficulty=" << shot.difficulty << std::endl;
				// std::cout << "difficulty: " << shot.difficulty << std::endl;
				gameState = GameState::AIMING;
				stateChanged = true;
			}
		}
		else if (gameState == GameState::AIMING)
		{
			if (gameStateTimer.peekRelativeTo(masterFrameTimer) > shot.timeOut || fireState == FireState::FIRED)
			{
				shot.shotTaken = gameStateTimer.peekRelativeTo(masterFrameTimer); // mark the time when button was pressed or timeout was reached


				// state transition: AIMING -> REGISTERING
				//std::cout << "State transition: AIMING -> REGISTERING. shot.shotTaken=" << shot.shotTaken << ", timeout=" << (gameStateTimer.peekRelativeTo(masterFrameTimer) > shot.timeOut) << ", fired=" << (fireState == FireState::FIRED) << std::endl;
				gameState = GameState::REGISTERING;
				stateChanged = true;
			}
		}
		else if (gameState == GameState::REGISTERING)
		{
			if (gameStateTimer.peekRelativeTo(masterFrameTimer) > shot.timeOut || fireState == FireState::HIT || fireState == FireState::MISS)
			{
				forceRealFrame = true;
				shot.shotRegistered = gameStateTimer.peekRelativeTo(masterFrameTimer); // mark the time when shot was registered or timeout was reached

				bool noisy = !(shot.shotType == Shot::ShotType::TUTORIAL && !gameModule->isTutorial()); // special case: if tutorial shot was started before tutorial ended just end tutorial silently...

				/*
				if (noisy)
				{
					Mix_PlayChannel(-1, gunSound, 0);
				}
				*/

				if (gameStateTimer.peekRelativeTo(masterFrameTimer) > shot.timeOut)
				{
					//std::cout << "shot.shotRegistered=" << shot.shotRegistered << ", timeout=" << (gameStateTimer.peekRelativeTo(masterFrameTimer) > shot.timeOut) << std::endl;
					// std::cout << "timeout" << std::endl;

					shot.status = Shot::Status::TIME_OUT;
					
					if (noisy)
					{
						feedbackType = GLModule::FeedbackType::TIMEOUT;
						Mix_PlayChannel(-1, timeoutSound, 0);
					}
				}
				else if (fireState == FireState::HIT)
				{
					//std::cout << "shot.shotRegistered=" << shot.shotRegistered << ", hit" << std::endl;
					// std::cout << "hit" << std::endl;

					shot.status = Shot::Status::HIT;
					
					if (noisy)
					{
						feedbackType = GLModule::FeedbackType::HIT;
						Mix_PlayChannel(-1, hitSound, 0);
					}
				}
				else if (fireState == FireState::MISS)
				{
					//std::cout << "shot.shotRegistered=" << shot.shotRegistered << ", miss" << std::endl;
					// std::cout << "miss" << std::endl;

					shot.status = Shot::Status::MISS;
					
					if (noisy)
					{
						feedbackType = GLModule::FeedbackType::MISS;
						Mix_PlayChannel(-1, missSound, 0);
					}
				}

				gameModule->registerShot(shot);

				gameStateTimer.restartRelativeTo(masterFrameTimer); // switch from timing shooting timeout to timing interlude

				// nothing realtime sensitive going on atm.... so good time to do some hard disk writing
				if (logFile.is_open())
				{
					logFile << logBuffer.rdbuf();

					logBuffer.str(std::string());
				}

				if (gameModule->isGameEnded())
				{
					std::cout << "State transition: REGISTERING -> ENDED." << std::endl << std::endl;
					// state transition: REGISTERING -> ENDED
					gameState = GameState::ENDED;
				}
				else
				{
					physicsUpdateTimer.pauseRelativeTo(masterFrameTimer);

					//std::cout << "State transition: REGISTERING -> INTERLUDE." << std::endl << std::endl;
					// state transition: REGISTERING -> INTERLUDE
					gameState = GameState::INTERLUDE;
				}
				stateChanged = true;
			}
		}
		else if (gameState == GameState::ENDED)
		{
			// state transition: nil
			//std::cout << "nil" << std::endl;

			bouncyObject.setDrawEnabled(false);
			feedbackType = GLModule::FeedbackType::INVISIBLE;
			forceRealFrame = true;
		}
		else
		{
			// impossible state unless someone changes the enum
			std::cout << "error" << std::endl;
		}
	} while (stateChanged);
}

void SDLModule::resetGame()
{
	reportFileName = "report_" + UtilModule::getDateTimeString() + ".csv";
	gameModule->reset(reportFileName);

	bouncyObject.setDrawEnabled(false);
	gameStateTimer.restartRelativeTo(masterFrameTimer);
	resetCamera();
	gameState = GameState::INTERLUDE;
}

void SDLModule::resetScene(glm::dvec2 limitMin, glm::dvec2 limitMax, glm::dvec2 position, double depth, glm::dvec2 velocity, double radius)
{
	// reset game environment state

	bouncyObject.resetState();
	bouncyObject.resetState(limitMin, limitMax, position, depth, velocity, radius);

	physicsModule->resetPhysics();
	physicsUpdateTimer.restartRelativeTo(masterFrameTimer);

	fireState = FireState::NOT_FIRED;
	frameSkipCounter = 0;
}

void SDLModule::resetCamera()
{
	physicsModule->resetCamera();

	calculateCorners();
}

void SDLModule::calculateCorners()
{
	double innerMargin = max(getScreenWidth(), getScreenHeight()) / 24.0;

	worldOuterPositionMin = glModule->fromDevicePositionToWorldPosition(glm::dvec3(0, 0, worldPositionDepth));
	worldOuterPositionMax = glModule->fromDevicePositionToWorldPosition(glm::dvec3(getScreenWidth(), getScreenHeight(), worldPositionDepth));
	
	worldInnerPositionMin = glModule->fromDevicePositionToWorldPosition(glm::dvec3(innerMargin, innerMargin, worldPositionDepth));
	worldInnerPositionMax = glModule->fromDevicePositionToWorldPosition(glm::dvec3(getScreenWidth() - innerMargin, getScreenHeight() - innerMargin, worldPositionDepth));
}

void SDLModule::updateCamera(double time)
{
	int x;
	int y;
	int numkeys;

	// eat input
	Uint32 relativeMouseState = SDL_GetRelativeMouseState(&x, &y);
	const Uint8 *state = SDL_GetKeyboardState(&numkeys);

	// use input only if user is allowed to move
	if (gameState != GameState::INTERLUDE && (!pause || getDeveloperMode()))
	{
		physicsModule->cameraX_rotation_angle += physicsModule->camera_rotation_sensitivity * x;
		physicsModule->cameraY_rotation_angle += physicsModule->camera_rotation_sensitivity * y * (getInverseY() ? -1.0f : 1.0f);
		if (physicsModule->cameraY_rotation_angle > M_PI / 2)
		{
			physicsModule->cameraY_rotation_angle = (float)(M_PI / 2);
		}
		else if (physicsModule->cameraY_rotation_angle < -M_PI / 2)
		{
			physicsModule->cameraY_rotation_angle = (float)(-M_PI / 2);
		}

		if (physicsModule->cameraX_rotation_angle > M_PI)
		{
			physicsModule->cameraX_rotation_angle -= 2.0f * (float)M_PI;
		}
		else if (physicsModule->cameraX_rotation_angle < -M_PI)
		{
			physicsModule->cameraX_rotation_angle += 2.0f * (float)M_PI;
		}
		//printf("mouse: %d    %d    %d\n", x, y, relativeMouseState);
		//printf("%f    %f\n", cameraX_rotation_angle, cameraY_rotation_angle);

		if (getDeveloperMode())
		{
			glm::vec4 move(0, 0, 0, 1.0);

			bool sneak = false;

			if (state[SDL_SCANCODE_W])
			{
				move.z -= 1;
				//printf("W\n");
			}
			if (state[SDL_SCANCODE_S])
			{
				move.z += 1;
				//printf("S\n");
			}
			if (state[SDL_SCANCODE_A])
			{
				move.x -= 1;
				//printf("A\n");
			}
			if (state[SDL_SCANCODE_D])
			{
				move.x += 1;
				//printf("D\n");
			}
			if (state[SDL_SCANCODE_LSHIFT])
			{
				sneak = true;
				//printf("LSHIFT\n");
			}
			if (state[SDL_SCANCODE_LCTRL])
			{
				move.y -= 1;
			}
			if (state[SDL_SCANCODE_LALT])
			{
				move.y += 1;
			}

			move = glm::rotateY(move, -physicsModule->cameraX_rotation_angle);

			glm::normalize(move);

			move *= (float)(time * physicsModule->movementSpeed); // in meters

			if (sneak)
			{
				move *= 0.25;
			}

			physicsModule->cameraX_position += move.x;
			physicsModule->cameraY_position += move.y;
			physicsModule->cameraZ_position += move.z;

			if (physicsModule->cameraY_position < 0)
			{
				physicsModule->cameraY_position = 0;
			}
		}
	}

	if (state[SDL_SCANCODE_TAB])
	{
		printf("pos: %f / %f / %f, rot: %f, %f\n", physicsModule->cameraX_position, physicsModule->cameraY_position, physicsModule->cameraZ_position, physicsModule->cameraX_rotation_angle, physicsModule->cameraY_rotation_angle);
	}
}

std::string SDLModule::hitIdToString(GLubyte hitId)
{
	if (hitId == worldcubeObject.getHitId())
	{
		return "worldcube";
	}
	else if (hitId == sponzaObject.getHitId())
	{
		return "sponza";
	}
	else if (hitId == bouncyObject.getHitId())
	{
		return "lego dudes";
	}
	else
	{
		return "N/A";
	}
}

bool SDLModule::hitTarget(GLubyte hitId)
{
	return hitId == bouncyObject.getHitId();
}

std::string SDLModule::getInfoString()
{
	std::stringstream ss;

	ss << "object in sight: " << hitIdToString(getHitId()) << std::endl;

	ss << "interpolation ";
	if (getInterpolate())
	{
		ss << "on";
	}
	else
	{
		ss << "off";
	}
	ss << std::endl;

	ss << "updating input during interpolation: ";
	if (getInterpolationInputUpdating())
	{
		ss << "on";
	}
	else
	{
		ss << "off";
	}
	ss << std::endl;

	ss << "wireframe mode ";
	if (getWireframe())
	{
		ss << "on";
	}
	else
	{
		ss << "off";
	}
	ss << std::endl;

	if (getFrameSkip() == 0x7FFFFFFF)
	{
		ss << "target fps: 0 (I-frame ratio 1:infinite)" << std::endl;
	}
	else
	{
		ss << "target fps: " << getRefreshRate() / (float)getFrameSkip() << " (I-frame ratio 1:" << getFrameSkip() << ")" << std::endl;
	}

	ss << getScreenWidth() << " x " << getScreenHeight() << " @ " << getRefreshRate() << std::endl;

	std::string ret = ss.str();
	return ret;
}

int main(int argc, char* argv[])
{
	SDLModule sdlModule(argc, argv);

	sdlModule.mainLoop();

	return 0;
}
