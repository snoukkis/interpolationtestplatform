#version 330

#define NUM_LIGHTS %NUM_LIGHTS%

#define PHONG 0
#define BLINN_PHONG 1
#define REFLECTION_MODEL PHONG

layout(location = 0) out vec4 color;
layout(location = 1) out vec4 velocityOut;
layout(location = 2) out uint hitOut;

uniform vec3 velocity;

struct Transformations
{
	mat4 fromModelToViewNormal;

	mat4 fromModelToView;
	mat4 fromViewToProjection;
};
uniform Transformations transformations;

struct Textures
{
	// sampler2DArray normal;
	// sampler2DArray ambient;
	sampler2DArray diffuse;
	samplerCube sky;
	// sampler2DArray specular;
};
uniform Textures textures;

struct Light
{
	vec3 position;

	vec3 ambient;
	vec3 diffuse;
	vec3 specular;
};
uniform Light lights[NUM_LIGHTS];
uniform Light light;

in Vertex
{
	vec3 position;
	vec3 normal;
	vec3 texcoord;
    vec2 texcoordScale;
} vertexIn;

in Material
{
	vec3 emission;
	vec3 ambient;
	vec3 diffuse;
	vec3 specular;
	float shininess;
} materialIn;

uniform uint hitId;

void main()
{
	const bool cullBackFace = false;
	
	if(gl_FrontFacing || !cullBackFace)
	{
		hitOut = hitId;

		// set depth before moving on to color shading...
		gl_FragDepth = gl_FragCoord.z;

		velocityOut = vec4(velocity, 1.0);

		// initially no light has hit the fragment
		color = vec4(0.0, 0.0, 0.0, 1.0);

		// check if we are to use vertex color or sample a texture instead
		vec3 diffuseColor;
		if(vertexIn.texcoord.z >= 0)
		{
			// to 0 ... 1
			vec2 texcoord = fract(vertexIn.texcoord.xy);
			
			// to 0 ... texcoordScale
			texcoord *= vertexIn.texcoordScale;

			diffuseColor = texture(textures.diffuse, vec3(texcoord, vertexIn.texcoord.z)).rgb;
		}
		else
		{
			diffuseColor = materialIn.diffuse;
		}

		// get the normal
		vec3 normal = normalize(vertexIn.normal);
		normal *= gl_FrontFacing ? 1 : -1;
		
		for(int lightIndex = 0; lightIndex < NUM_LIGHTS; lightIndex++)
		{
			// figure out light direction
			vec3 lightDir = lights[lightIndex].position - vertexIn.position;
			
			float distance = length(lightDir);
			lightDir /= distance;
			distance = distance * distance;

			// calculate diffuse contribution
			float lambertian = max(dot(lightDir,normal), 0.0);
		
			// calculate specular contribution
			float specular;

			if(lambertian > 0.0)
			{
				vec3 viewDir = normalize(-vertexIn.position);
			
				if(REFLECTION_MODEL == PHONG)
				{
					vec3 reflectDir = reflect(-lightDir, normal);
					float specAngle = max(dot(reflectDir, viewDir), 0.0);
					specular = pow(specAngle, materialIn.shininess);
				}
				else // if(REFLECTION_MODEL == BLINN_PHONG)
				{
					vec3 halfDir = normalize(lightDir + viewDir);
					float specAngle = max(dot(halfDir, normal), 0.0);
					specular = pow(specAngle, 4 * materialIn.shininess); // note that the exponent is different here
				}
			}
			else
			{
				specular = 0.0;
			}

			// add contributions from light sources
			color += vec4(materialIn.ambient  * lights[lightIndex].ambient                         , 0.0);
			color += vec4(diffuseColor        * lights[lightIndex].diffuse  * lambertian / distance, 0.0);
			color += vec4(materialIn.specular * lights[lightIndex].specular * specular   / distance, 0.0);
		}
		color += vec4(materialIn.emission, 0.0); // finally light emitting from material itself

		color += vec4(texture(textures.sky, vertexIn.texcoord).rgb, 0.0);
	}
	else
	{
		discard;
	}
}
