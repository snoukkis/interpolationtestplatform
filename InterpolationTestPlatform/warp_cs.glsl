#version 400

#define ID gl_InvocationID

layout (vertices = 4) out;

uniform sampler2D cachedDepth;

in PatchData
{
	uvec2 gridIndex;
} PatchIn[];

out PatchData
{
	uvec2 gridIndex;
} PatchOut[];

uniform ivec2 gridSize;

bool scanForDepthDiff(int constantValue, int first, int last, float depthThreshold, uint cai /* constant axis index */, uint vai /* variable axis index */)
{
	// get screen size
	ivec2 screenSize = textureSize(cachedDepth, 0);
	
	// clamp range values to screen
	constantValue = clamp(constantValue, 0, screenSize[cai] - 1);
	first = clamp(first, 0, screenSize[vai] - 1);
	last = clamp(last, 0, screenSize[vai] - 1);
	
	// preset A position and depth
	ivec2 positionA;
	float depthA;
	positionA[cai] = constantValue;
	positionA[vai] = first;
	depthA = texelFetch(cachedDepth, positionA, 0).r;
	
	// compare values
	for(int variable = positionA[vai] + 1; variable <= last; variable++)
	{
		// preset B position and depth
		ivec2 positionB;
		positionB[cai] = positionA[cai];
		positionB[vai] = variable;
		float depthB = texelFetch(cachedDepth, positionB, 0).r; // range 0.0 ... 1.0
		
		// calculate depth ration
		float ratio = min(depthA, depthB) / max(depthA, depthB); // range 0.0 ... 1.0
		
		if(ratio < depthThreshold)
		{
			// an edge was found
			return true;
		}
		
		// shift current B values to be next A values
		positionA = positionB;
		depthA = depthB;
	}
	
	// there were no edges
	return false;
}

bool verScanForDepthDiff(int x, int y1, int y2, float depthThreshold)
{
	return scanForDepthDiff(x, y1, y2, depthThreshold, 0, 1);
}

bool horScanForDepthDiff(int y, int x1, int x2, float depthThreshold)
{
	return scanForDepthDiff(y, x1, x2, depthThreshold, 1, 0);
}

void main()
{
	ivec2 screenSize = textureSize(cachedDepth, 0);
	int multiplier = screenSize.x / gridSize.x;
	
	PatchOut[ID].gridIndex = PatchIn[ID].gridIndex * multiplier; // 0.0 ... 16.0
	
	// do only once per patch
	if (ID == 0)
	{
		ivec2 patchMin;
		patchMin.x = int(min(min(PatchIn[0].gridIndex.x, PatchIn[1].gridIndex.x), min(PatchIn[2].gridIndex.x, PatchIn[3].gridIndex.x)));
		patchMin.y = int(min(min(PatchIn[0].gridIndex.y, PatchIn[1].gridIndex.y), min(PatchIn[2].gridIndex.y, PatchIn[3].gridIndex.y)));
		patchMin *= multiplier;
		
		ivec2 patchMax;
		patchMax.x = int(max(max(PatchIn[0].gridIndex.x, PatchIn[1].gridIndex.x), max(PatchIn[2].gridIndex.x, PatchIn[3].gridIndex.x)));
		patchMax.y = int(max(max(PatchIn[0].gridIndex.y, PatchIn[1].gridIndex.y), max(PatchIn[2].gridIndex.y, PatchIn[3].gridIndex.y)));
		patchMax *= multiplier;
		
		int tessLevel = multiplier;
		int noTessLevel = 1;
		
		gl_TessLevelOuter[0] = gl_TessLevelOuter[1] = gl_TessLevelOuter[2] = gl_TessLevelOuter[3] = gl_TessLevelInner[0] = gl_TessLevelInner[1] = noTessLevel;
		
		float depthThreshold = 0.9999;
		
		
		
		// horizontal scan current patch for vertical edge
		for(int y = patchMin.y; y <= patchMax.y; y++)
		{
			if(horScanForDepthDiff(y, patchMin.x, patchMax.x, depthThreshold))
			{
				gl_TessLevelOuter[1] = gl_TessLevelOuter[3] = gl_TessLevelInner[0] = tessLevel;
				break;
			}
		}
		
		if(gl_TessLevelInner[0] == noTessLevel)
		{
			// horizontal scan bottom patch for vertical edge
			for(int y = patchMin.y - multiplier; y <= patchMin.y - 1; y++)
			{
				if(horScanForDepthDiff(y, patchMin.x, patchMax.x, depthThreshold))
				{
					gl_TessLevelOuter[1] = tessLevel;
					break;
				}
			}
			
			// horizontal scan top patch for vertical edge
			for(int y = patchMax.y + 1; y <= patchMax.y + multiplier; y++)
			{
				if(horScanForDepthDiff(y, patchMin.x, patchMax.x, depthThreshold))
				{
					gl_TessLevelOuter[3] = tessLevel;
					break;
				}
			}
		}
		
		
		
		// vertical scan current patch for horizontal edge
		for(int x = patchMin.x; x <= patchMax.x; x++)
		{
			if(verScanForDepthDiff(x, patchMin.y, patchMax.y, depthThreshold))
			{
				gl_TessLevelOuter[0] = gl_TessLevelOuter[2] = gl_TessLevelInner[1] = tessLevel;
				break;
			}
		}
		
		if(gl_TessLevelInner[1] == noTessLevel)
		{
			// vertical scan left patch for horizontal edge
			for(int x = patchMin.x - multiplier; x <= patchMin.x - 1; x++)
			{
				if(verScanForDepthDiff(x, patchMin.y, patchMax.y, depthThreshold))
				{
					gl_TessLevelOuter[0] = tessLevel;
					break;
				}
			}
			
			// vertical scan right patch for horizontal edge
			for(int x = patchMax.x + 1; x <= patchMax.x + multiplier; x++)
			{
				if(verScanForDepthDiff(x, patchMin.y, patchMax.y, depthThreshold))
				{
					gl_TessLevelOuter[2] = tessLevel;
					break;
				}
			}
		}
	}
}
