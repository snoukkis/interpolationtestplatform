#pragma once

#include <gl\glew.h>

#define GLM_SWIZZLE
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
//#include <glm/gtc/matrix_access.hpp>
#include <glm/gtc/type_ptr.hpp>

struct Vertex
{
	glm::vec3 position;
	glm::vec3 normal;
	glm::vec3 texcoord;
	glm::vec2 texcoordScale;

	glm::vec3 emission;
	glm::vec3 ambient;
	glm::vec3 diffuse;
	glm::vec3 specular;
	float shininess;
};

struct AttributeLocations
{
	GLint position = -1;
	GLint normal = -1;
	GLint texcoord = -1;
	GLint texcoordScale = -1;

	GLint emission = -1;
	GLint ambient = -1;
	GLint diffuse = -1;
	GLint specular = -1;
	GLint shininess = -1;
};

struct Texture
{
	GLsizei layer;
	glm::ivec2 size;
	glm::vec2 scale;
	uint32_t* pixels;
};

struct Light
{
	GLint positionUniformLocation = -1;
	GLint ambientUniformLocation = -1;
	GLint diffuseUniformLocation = -1;
	GLint specularUniformLocation = -1;

	glm::vec3 position = glm::vec3(0.0, 0.0, 0.0);

	glm::vec3 ambient = glm::vec3(0.0, 0.0, 0.0);
	glm::vec3 diffuse = glm::vec3(0.0, 0.0, 0.0);
	glm::vec3 specular = glm::vec3(0.0, 0.0, 0.0);
};

struct GuiUniformLocations
{
	GLint guiTexture = -1;
	GLint textOffset = -1;
	GLint textScale = -1;
};

struct RenderUniformLocations
{
	// material
	GLint diffuseTexture = -1;
	GLint skyTexture = -1;

	// velocity
	GLint velocity = -1;

	// matrices
	GLint fromModelToViewMatrix = -1;
	GLint fromModelToViewNormalMatrix = -1;
	GLint fromViewToProjectionMatrix = -1;

	// hit id
	GLint hitId = -1;
};
