#pragma once

#define GLM_SWIZZLE
#include <glm/glm.hpp>
#include "Integrator.h"

class EulerIntegrator: public Integrator
{
public:
	EulerIntegrator(double timeStep, PhysicalObject& physicalObject) : Integrator(timeStep, physicalObject) {}
	~EulerIntegrator() {}

	void stepSystem();
};
