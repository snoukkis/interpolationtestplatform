#include "PhysicsModule.h"

#include "PhysicalObject.h"

#include "IntegratorEuler.h"
#include "IntegratorTrapezoidal.h"
#include "IntegratorRungeKutta.h"

PhysicsModule::PhysicsModule(SimplePhysicalObject& simplePhysicalObject, PhysicalObject& physicalObject) : simplePhysicalObject(simplePhysicalObject), physicalObject(physicalObject)
{
	resetPhysics();
	resetCamera();
}

void PhysicsModule::resetPhysics()
{
	resetSystem(RungeKutta);
}

void PhysicsModule::resetCamera()
{
	cameraX_rotation_angle = 0.0;
	cameraY_rotation_angle = 0.0;

	cameraX_position = 0.0f;
	cameraY_position = 4.02344f;
	cameraZ_position = 0.0f;
}

PhysicsModule::~PhysicsModule()
{
	if (integrator)
	{
		delete integrator;
	}
}

bool PhysicsModule::update(double frametime)
{
	timeBuffer += frametime;

	while (timeBuffer >= timeStep)
	{
		stepSystem();

		timeBuffer -= timeStep;
	}

	return simplePhysicalObject.stepSystem(frametime);
}

void PhysicsModule::stepSystem()
{
	if (integrator)
	{
		integrator->stepSystem();
	}
}

void PhysicsModule::shiftSystem(bool forward)
{
	simplePhysicalObject.shiftSystem(forward);

	physicalObject.shiftSystem(forward);
}

void PhysicsModule::resetSystem(IntegratorMode integratorMode)
{
	if (integrator)
	{
		delete integrator;
	}

	if (integratorMode == Euler)
	{
		integrator = new EulerIntegrator(timeStep, physicalObject);
	}
	else if (integratorMode == Trapezoidal)
	{
		integrator = new TrapezoidalIntegrator(timeStep, physicalObject);
	}
	else if (integratorMode == RungeKutta)
	{
		integrator = new RungeKuttaIntegrator(timeStep, physicalObject);
	}
	else
	{
		integrator = new EulerIntegrator(timeStep, physicalObject);
	}

	timeBuffer = 0;
}
