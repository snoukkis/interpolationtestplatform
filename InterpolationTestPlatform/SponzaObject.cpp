#include "SponzaObject.h"
#include "Forces.h"

#define M_PI 3.14159265358979323846264338327950288

SponzaObject::SponzaObject()
{
}

SponzaObject::~SponzaObject()
{
}

glm::vec3 SponzaObject::getScale()
{
	glm::vec3 ret = glm::vec3(0.21240234375f * aspectRatio, 0.5f, 0.5f);

	return ret;
}

glm::mat4 SponzaObject::getRotation()
{
	float angle = -((float)M_PI);

	glm::mat4 rotMat;
	rotMat = glm::rotate(rotMat, angle, glm::vec3(0.0f, 1.0f, 0.0f));

	return rotMat;
}

glm::vec3 SponzaObject::getPosition()
{
	return glm::vec3(0.0f, 0.0f, 0.0f) * getScale();
}

glm::vec3 SponzaObject::getVelocity()
{
	return glm::vec3(0.0f);
}

std::string SponzaObject::getFilePath()
{
	return "models/SulacoHanger/";
}

std::string SponzaObject::getFileName()
{
	return "SulacoHanger";
}

void SponzaObject::changeAspectRatio(float aspectRatio)
{
	this->aspectRatio = aspectRatio;
}

bool SponzaObject::shouldOverlay()
{
	return false;
}
