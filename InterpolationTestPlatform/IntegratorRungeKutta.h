#pragma once

#define GLM_SWIZZLE
#include <glm/glm.hpp>
#include "Integrator.h"

class RungeKuttaIntegrator: public Integrator
{
public:
	RungeKuttaIntegrator(double timeStep, PhysicalObject& physicalObject) : Integrator(timeStep, physicalObject) {}
	~RungeKuttaIntegrator() {}

	void stepSystem();
};
