#pragma once

#define GLM_SWIZZLE
#include <glm/glm.hpp>
#include "Integrator.h"

class TrapezoidalIntegrator: public Integrator
{
public:
	TrapezoidalIntegrator(double timeStep, PhysicalObject& physicalObject) : Integrator(timeStep, physicalObject) {}
	~TrapezoidalIntegrator() {}

	void stepSystem();
};
