#version 410

in vec2 vertexPositionAttribute;
out vec2 texCoord;

uniform vec2 textOffset;
uniform vec2 textScale;

void main()
{
	gl_Position = vec4(vertexPositionAttribute * 2.0 * textScale - 1.0 + textOffset * 2.0, -1.0, 1.0);

	texCoord = vertexPositionAttribute;
}
