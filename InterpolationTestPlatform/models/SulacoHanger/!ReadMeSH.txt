SulacoHanger for Poser (PC/ Mac)
Poser figure model by: Arjin  rjnehrlich@tds.net

Notes:
	1) Only tested with Poser7 & 8 on a MAC OS
	2) The Power Loader (PWL2) is NON-Conforming, but can be parented to a figure.
	3) For ease of operation the Sulaco hanger has body parameters to control some parts.

Directions:
Extract the files into the specified folders:
Put the obj files (geometry files) into Runtime:Geometries:Arjin:SulacoHanger
Put the cr2/ png (figure files) into Runtime:Libraries:Characters:Arjin:SulacoHanger
Put the mc6/ png (material files) into Runtime:Libraries:Materials:Arjin:SulacoHanger
Put the cm2/ png (camera files) into Runtime:Libraries:Camera:Arjin:SulacoHanger
Put the it2/ png (light files) into Runtime:Libraries:Light:Arjin:SulacoHanger
Put the pz2/ png (pose files) into Runtime:Libraries:Pose:Arjin:SulacoHanger
Put the pp2/ png (prop files) into Runtime:Libraries:Props:Arjin:SulacoHanger
Put the jpg files (textures) into Runtime:Textures:Arjin:SulacoHanger

Using the figure:
1) Start Poser.
2) Go to the Figures > Arjin:SulacoHanger:.
3) Add the SulacoHanger or PWL2010 to your scene.



-----------------------------------
These Poser figures are based on the Sulaco hanger and the power loader which is seen in the movie Aliens.
Sulaco and PWL, and all related likenesses are copyright of Twentieth Century Fox.
-----------------------------------

Usage License:
Non-Commercial use only. 

Files contained in this package:
--------------------------------
Geometries Files:
	CargoBox.obj
	GenBox.obj
	LoaderHarnessV3.obj
	MedBox.obj
	PWL2.obj
	SmallBox.obj
	SulacoHanger.obj
	WelderTorch.obj

Camera files:
	HangerAux.cm2
	HangerAux.png
	HangerMain.cm2
	HangerMain.png

Characters files:
	SulacoHanger.png	SulacoHanger.cr2	PWL2.cr2	PWL2.png	LoaderHarnessV3.png	LoaderHarnessV3.cr2
	
Light files:
	CageLite.lt2
	CageLite.png
	SulacoHanger.lt2
	SulacoHanger.png
	WarningLite.lt2	WarningLite.png

Materials files:
	CargoBox.mc6	CargoBox.png	LoaderHarnessV3.mc6
	LoaderHarnessV3.png
	PWL2.mc6
	PWL2.png
	SulacoHanger.mc6
	SulacoHanger.png
	WelderTorch.mc6
	WelderTorch.png

Pose files:	Loader1.pz2
	Loader1.png
	Loader2.pz2
	Loader2.png
	Riply1.pz2
	Riply1.png
	Riply2.pz2
	Riply2.png

Props Files:
	CargoBox.pp2
	CargoBox.png
	GenBox.pp2	GenBox.png
	MedBox.pp2
	MedBox.png
	SmallBox.pp2
	SmallBox.png
	WelderTorch.pp2
	WelderTorch.png

Textures files:
	AngledDecking.jpg
	BayNumber00.jpg
	BayNumber01.jpg
	BayNumber02.jpg
	BayNumber03.jpg
	BayNumber04.jpg
	BayNumber05.jpg
	BayNumber06.jpg
	BayNumber07.jpg
	BayNumber08.jpg	BayNumber09.jpg
	BayNumber10.jpg	BayNumber11.jpg	BayNumber12.jpg	BayNumber13.jpg	BayNumber14.jpg	BayNumber15.jpg	BayNumber16.jpg	BayNumber17.jpg	BayNumber18.jpg
	BayNumberBlank.jpg
	BuckleLabel.jpg
	CargoBox.jpg
	CargoM20.jpg
	CargoM21.jpg
	CargoM22.jpg	CargoM23.jpg	CargoM24.jpg	CargoM25.jpg	EWalls.jpg	HangerNames.jpg	InnerEDoors.jpg	LineGrill.jpg	LoaderLabelsDV.jpg
	LoaderWarning.jpg	OuterEDoors.jpg
	SulacoTermenal.jpg
	WarningRed.jpg	WarningYellow.jpg
	WelderTorch.jpg
!ReadMeSH.txt (This File)
