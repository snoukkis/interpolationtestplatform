#include "ObjObject.h"
#include <iostream>
#include <map>
#include "UtilModule.h"

#define GLM_SWIZZLE
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
//#include <glm/gtc/matrix_access.hpp>
#include <glm/gtc/type_ptr.hpp>

void ObjObject::initDraw(AttributeLocations& attributeLocations)
{
	//loadObj("models/", "garg");
	//loadObj("models/", "sphere");
	//loadObj("models/", "torus");
	//loadObj("models/ju-87_obj/", "ju-87");
	//loadObj("models/lego_people_obj/", "lego_poeople_obj");
	//loadObj("models/pumpkinseed_obj/", "pumpkinseed");
	//loadObj("models/Key_B.obj/", "Key_B_02");
	//loadObj("models/crytek-sponza/", "sponza");
	
	loadObj(getFilePath(), getFileName());

	glGenVertexArrays(1, &objVertexArrayObject);
	glBindVertexArray(objVertexArrayObject);
	{
		glGenBuffers(1, &objVertexBufferObject);
		glBindBuffer(GL_ARRAY_BUFFER, objVertexBufferObject);
		{
			glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex) * objVerticesCount, objVertices.data(), GL_STATIC_DRAW);
			objVertices.clear();

			glEnableVertexAttribArray(attributeLocations.position);
			glVertexAttribPointer(attributeLocations.position, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)offsetof(Vertex, position));

			glEnableVertexAttribArray(attributeLocations.normal);
			glVertexAttribPointer(attributeLocations.normal, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)offsetof(Vertex, normal));

			glEnableVertexAttribArray(attributeLocations.texcoord);
			glVertexAttribPointer(attributeLocations.texcoord, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)offsetof(Vertex, texcoord));

			glEnableVertexAttribArray(attributeLocations.texcoordScale);
			glVertexAttribPointer(attributeLocations.texcoordScale, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)offsetof(Vertex, texcoordScale));

			glEnableVertexAttribArray(attributeLocations.emission);
			glVertexAttribPointer(attributeLocations.emission, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)offsetof(Vertex, emission));

			glEnableVertexAttribArray(attributeLocations.ambient);
			glVertexAttribPointer(attributeLocations.ambient, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)offsetof(Vertex, ambient));

			glEnableVertexAttribArray(attributeLocations.diffuse);
			glVertexAttribPointer(attributeLocations.diffuse, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)offsetof(Vertex, diffuse));

			glEnableVertexAttribArray(attributeLocations.specular);
			glVertexAttribPointer(attributeLocations.specular, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)offsetof(Vertex, specular));

			glEnableVertexAttribArray(attributeLocations.shininess);
			glVertexAttribPointer(attributeLocations.shininess, 1, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)offsetof(Vertex, shininess));
		}
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}
	glBindVertexArray(0);
}

void ObjObject::loadObj(std::string path, std::string objFileName)
{
	std::vector<tinyobj::shape_t> shapes;
	std::vector<tinyobj::material_t> materials;

	std::string err = tinyobj::LoadObj(shapes, materials, (path + objFileName + ".obj").c_str(), path.c_str());

	if (!err.empty())
	{
		std::cerr << err << std::endl;
		exit(1);
	}

	std::cout << "# of shapes    : " << shapes.size() << std::endl;
	std::cout << "# of materials : " << materials.size() << std::endl;

	loadTextures(path, materials);

	objVertices.clear();

	objMinPosition = glm::vec3(std::numeric_limits<float>::max());
	objMaxPosition = glm::vec3(std::numeric_limits<float>::min());

	for (size_t shapeNumber = 0; shapeNumber < shapes.size(); shapeNumber++)
	{
		std::string name = shapes[shapeNumber].name;

		/*
		if (name != "sponza_18" && name != "sponza_117")
		{
			continue;
		}
		*/

		//std::cout << name << std::endl;

		const tinyobj::shape_t& shape = shapes[shapeNumber];
		const tinyobj::mesh_t& mesh = shape.mesh;

		//std::map<float, int> yHistogram;

		for (size_t indexNumber = 0; indexNumber < mesh.indices.size(); indexNumber++)
		{
			Vertex vertex;

			unsigned int vertexIndex = mesh.indices[indexNumber];
			int material_id = mesh.material_ids[indexNumber / 3];

			float positionX = mesh.positions[3 * vertexIndex + 0];
			float positionY = mesh.positions[3 * vertexIndex + 1];
			float positionZ = mesh.positions[3 * vertexIndex + 2];
			vertex.position = glm::vec3(positionX, positionY, positionZ);

			/*
			if (yHistogram.count(positionY) > 0)
			{
				yHistogram[positionY]++;
			}
			else
			{
				yHistogram[positionY] = 1;
			}
			*/

			float normalX = mesh.normals[3 * vertexIndex + 0];
			float normalY = mesh.normals[3 * vertexIndex + 1];
			float normalZ = mesh.normals[3 * vertexIndex + 2];
			vertex.normal = glm::vec3(normalX, normalY, normalZ);

			if (material_id >= 0)
			{
				Texture& texture = objTextureLayers[material_id];

				float texcoordS = mesh.texcoords[2 * vertexIndex + 0];
				float texcoordT = mesh.texcoords[2 * vertexIndex + 1];
				vertex.texcoord = glm::vec3(texcoordS, texcoordT, 0.0f);

				if (objTextureLayers.count(material_id) > 0)
				{
					vertex.texcoord.z = (float)texture.layer;
					vertex.texcoordScale = texture.scale;
				}
				else
				{
					vertex.texcoord.z = -1;
				}

				const tinyobj::material_t& material = materials[material_id];

				vertex.emission = glm::vec3(material.emission[0], material.emission[1], material.emission[2]);
				vertex.ambient = glm::vec3(material.ambient[0], material.ambient[1], material.ambient[2]);
				vertex.diffuse = glm::vec3(material.diffuse[0], material.diffuse[1], material.diffuse[2]);
				vertex.specular = glm::vec3(material.specular[0], material.specular[1], material.specular[2]);
				vertex.shininess = material.shininess;
			}
			else
			{
				vertex.texcoord.z = -1;
				vertex.emission = glm::vec3(0.0, 0.0, 0.0);
				vertex.ambient = glm::vec3(0.0, 0.0, 0.0);
				vertex.diffuse = glm::vec3(0.0, 0.0, 0.0);
				vertex.specular = glm::vec3(0.0, 0.0, 0.0);
				vertex.shininess = 1.0;
			}

			objVertices.push_back(vertex);

			if (vertex.position.x < objMinPosition.x)
			{
				objMinPosition.x = vertex.position.x;
			}
			if (vertex.position.y < objMinPosition.y)
			{
				objMinPosition.y = vertex.position.y;
			}
			if (vertex.position.z < objMinPosition.z)
			{
				objMinPosition.z = vertex.position.z;
			}

			if (vertex.position.x > objMaxPosition.x)
			{
				objMaxPosition.x = vertex.position.x;
			}
			if (vertex.position.y > objMaxPosition.y)
			{
				objMaxPosition.y = vertex.position.y;
			}
			if (vertex.position.z > objMaxPosition.z)
			{
				objMaxPosition.z = vertex.position.z;
			}
		}

		/*
		for (std::pair <const float, int> &bla : yHistogram)
		{
			std::cout << "count of " << bla.first << " is " << bla.second << std::endl;
		}
		*/
	}

	objVerticesCount = (GLsizei)objVertices.size();
}

void ObjObject::loadTextures(std::string path, const std::vector<tinyobj::material_t>& materials)
{
	objTextureLayers.clear();

	glm::ivec2 maxSize(0, 0);

	GLsizei layerCounter = 0;
	for (int materialIndex = 0; materialIndex < materials.size(); materialIndex++)
	{
		const tinyobj::material_t& material = materials[materialIndex];

		if (material.diffuse_texname.length() > 0)
		{
			Texture texture;

			texture.layer = layerCounter;
			texture.pixels = UtilModule::loadImage(path + material.diffuse_texname, texture.size.x, texture.size.y);

			maxSize.x = glm::max(maxSize.x, texture.size.x);
			maxSize.y = glm::max(maxSize.y, texture.size.y);

			objTextureLayers[materialIndex] = texture;

			layerCounter++;
		}
	}

	if (!objTextureLayers.empty())
	{
		glGenTextures(1, &objTextureObject);
		glBindTexture(GL_TEXTURE_2D_ARRAY, objTextureObject);
		{
			const int mipLevelCount = 4;
			glTexStorage3D(GL_TEXTURE_2D_ARRAY, mipLevelCount, GL_RGBA8, maxSize.x, maxSize.y, (GLsizei)objTextureLayers.size());
			glTexParameterf(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			glTexParameterf(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
			glTexParameterf(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
			glTexParameterf(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
			glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_GENERATE_MIPMAP, GL_TRUE);

			for (std::pair <const int, Texture> &textureLayer : objTextureLayers)
			{
				const int& materialIndex = textureLayer.first;
				Texture& texture = textureLayer.second;
				const tinyobj::material_t& material = materials[materialIndex];

				texture.scale = glm::vec2(texture.size) / glm::vec2(maxSize);

				glm::ivec2 repeat = maxSize / texture.size;
				for (int x = 0; x < repeat.x; x++)
				{
					for (int y = 0; y < repeat.y; y++)
					{
						// GLenum target, GLint level, GLint xoffset, GLint yoffset, GLint zoffset, GLsizei width, GLsizei height, GLsizei depth, GLenum format, GLenum type, const void *pixels
						glTexSubImage3D(GL_TEXTURE_2D_ARRAY, 0, x * texture.size.x, y * texture.size.y, texture.layer, texture.size.x, texture.size.y, 1, GL_RGBA, GL_UNSIGNED_BYTE, texture.pixels);
					}
				}

				delete texture.pixels;
				texture.pixels = NULL;
			}

			glGenerateMipmap(GL_TEXTURE_2D_ARRAY);
		}

		glBindTexture(GL_TEXTURE_2D_ARRAY, 0);
	}
}

ObjObject::~ObjObject()
{
	glDeleteTextures(1, &objTextureObject);
	glDeleteVertexArrays(1, &objVertexArrayObject);
	glDeleteBuffers(1, &objVertexBufferObject);

}

// render the system (ie draw the particles)
void ObjObject::drawDerived(const RenderUniformLocations& uniformLocations, const glm::mat4& fromWorldToLastRenderViewMatrix)
{
	glm::mat4 objFromModelToWorldMatrix = glm::mat4(1.0f);
	objFromModelToWorldMatrix = glm::translate(objFromModelToWorldMatrix, getPosition());
	objFromModelToWorldMatrix = getRotation() * objFromModelToWorldMatrix;
	objFromModelToWorldMatrix = glm::scale(objFromModelToWorldMatrix, getScale());

	glm::mat4 objFromModelToViewMatrix = fromWorldToLastRenderViewMatrix * objFromModelToWorldMatrix;
	glm::mat4 objFromModelToViewNormalMatrix = glm::transpose(glm::inverse(objFromModelToViewMatrix));;

	glUniform3fv(uniformLocations.velocity, 1, glm::value_ptr(getVelocity()));

	glUniformMatrix4fv(uniformLocations.fromModelToViewMatrix, 1, GL_FALSE, glm::value_ptr(objFromModelToViewMatrix));
	glUniformMatrix4fv(uniformLocations.fromModelToViewNormalMatrix, 1, GL_FALSE, glm::value_ptr(objFromModelToViewNormalMatrix));
	glBindVertexArray(objVertexArrayObject);
	{
		glUniform1i(uniformLocations.diffuseTexture, 0);
		glActiveTexture(GL_TEXTURE0); glBindTexture(GL_TEXTURE_2D_ARRAY, objTextureObject);
		{
			if (shouldOverlay())
			{
				glDepthFunc(GL_ALWAYS);
				{
					glDrawArrays(GL_TRIANGLES, 0, (GLsizei)objVerticesCount);
				}
				glDepthFunc(GL_LESS);
			}

			glDrawArrays(GL_TRIANGLES, 0, (GLsizei)objVerticesCount);
		}
		glActiveTexture(GL_TEXTURE0); glBindTexture(GL_TEXTURE_2D_ARRAY, 0);
	}
	glBindVertexArray(0);
}
