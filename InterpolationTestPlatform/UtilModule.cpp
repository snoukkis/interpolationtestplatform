#include "UtilModule.h"

#include <stdio.h>

#include <time.h>
#include <iomanip>
#include <sstream>

UtilModule::UtilModule()
{
}


UtilModule::~UtilModule()
{
}

const char* UtilModule::readFile(const char* filename)
{
	SDL_RWops* rwops = SDL_RWFromFile(filename, "r+b");
	if (rwops == NULL)
	{
		printf("Warning: Unable to open file! SDL Error: %s\n", SDL_GetError());
		return false;
	}

	Sint64 rwops_size = SDL_RWsize(rwops);
	char* buffer = new char[(size_t)rwops_size + 1];

	Sint64 i = 0;
	while (i < rwops_size)
	{
		i += SDL_RWread(rwops, buffer + i, sizeof(char), rwops_size - i);
	}
	buffer[rwops_size] = '\0';

	//Close file handler
	SDL_RWclose(rwops);

	return buffer;
}

Uint32* UtilModule::surfaceToTexture(SDL_Surface* surface)
{
	int width = surface->w;
	int height = surface->h;

	Uint32* ret = new Uint32[width * height];

	for (size_t y = 0; y < height; y++)
	{
		for (size_t x = 0; x < width; x++)
		{
			Uint8 r, g, b;
			Uint8 a = 0xFF;

			Uint32* pixel = (Uint32*)((Uint8*)surface->pixels + (height - 1 - y) * surface->pitch + x * surface->format->BytesPerPixel);

			if (surface->format->BytesPerPixel == 4)
			{
				SDL_GetRGBA(*pixel, surface->format, &r, &g, &b, &a);
			}
			else
			{
				SDL_GetRGB(*pixel, surface->format, &r, &g, &b);
				a = 0xFF;
			}

			ret[y * width + x] = r << 0 | g << 8 | b << 16 | a << 24;
		}
	}

	return ret;
}

Uint32* UtilModule::loadImage(std::string filename, int& width, int& height)
{
	SDL_Surface* surface = IMG_Load(filename.c_str());

	Uint32* ret = surfaceToTexture(surface);

	width = surface->w;
	height = surface->h;

	SDL_FreeSurface(surface);

	return ret;
}

Uint32* UtilModule::makeText(std::string text, UtilColor textColor, int& width, int& height, int size, int wrapPixels)
{
	TTF_Font *font = TTF_OpenFont("fonts/Vera-Bold.ttf", size);
	if (!font)
	{
		printf("TTF_OpenFont: %s\n", TTF_GetError());
		// handle error
	}

	SDL_Surface* surface = TTF_RenderText_Blended_Wrapped(font, text.c_str(), textColor, wrapPixels);

	Uint32* ret = surfaceToTexture(surface);

	width = surface->w;
	height = surface->h;

	SDL_FreeSurface(surface);

	TTF_CloseFont(font);

	return ret;
}

std::string UtilModule::getDateTimeString()
{
	std::stringstream dateTimeStringStream;
	std::string dateTimeString;

	time_t now = time(0);

	tm gmtm;
	gmtime_s(&gmtm, &now);

	dateTimeStringStream << std::setw(4) << std::setfill('0') << (1900 + gmtm.tm_year);
	dateTimeStringStream << "-" << std::setw(2) << std::setfill('0') << (gmtm.tm_mon + 1);
	dateTimeStringStream << "-" << std::setw(2) << std::setfill('0') << (gmtm.tm_mday);
	dateTimeStringStream << "T" << std::setw(2) << std::setfill('0') << (gmtm.tm_hour);
	dateTimeStringStream << "-" << std::setw(2) << std::setfill('0') << (gmtm.tm_min); // "-", not ":" due to filename restrictions
	dateTimeStringStream << "-" << std::setw(2) << std::setfill('0') << (gmtm.tm_sec); // "-", not ":" due to filename restrictions

	dateTimeString = dateTimeStringStream.str();

	return dateTimeString;
}
