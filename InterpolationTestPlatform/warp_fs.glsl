#version 330

in FragmentData
{
	vec2 texCoord;
	vec2 realTexCoord;
} FragmentIn;

in FragmentFlatData
{
	flat vec4 nearPosition1;
	flat vec4 nearPosition2;
} FragmentFlatIn;

uniform sampler2D cachedColor;

//layout(origin_upper_left) in vec4 gl_FragCoord;
//layout(pixel_center_integer) in vec4 gl_FragCoord;

layout(location = 0) out vec4 diffuseColor;

uniform float offsetten;

float distanceFromLine(vec2 pt1, vec2 pt2, vec2 testPt)
{
	vec2 line = pt2 - pt1;
	float lineLength = length(line);
	vec2 lineDirection = line / lineLength;

	float projectedLength = dot(testPt - pt1, lineDirection);
	float paramVal = projectedLength / lineLength;

	if(paramVal < 0.0)
	{
		float dist = abs(length(pt1 - testPt));

		return dist;
	}
	else if(paramVal > 1.0)
	{
		float dist = abs(length(pt2 - testPt));

		return dist;
	}
	else
	{
		vec2 n = normalize(pt2 - pt1);

		vec2 shortestPath = (pt1 - testPt) - (dot(pt1 - testPt, n)) * n;

		float dist = abs(length(shortestPath));

		return dist;
	}
}

void main()
{
	if(FragmentIn.texCoord == FragmentIn.realTexCoord)
	{
		diffuseColor = textureLod(cachedColor, FragmentIn.texCoord, 0.0);
		//diffuseColor = textureLod(cachedColor, FragmentIn.realTexCoord, 0.0);
	}
	else
	{
		ivec2 screenSize = textureSize(cachedColor, 0);
	
		vec2 nearPosition1 = screenSize.xy * (1.0 + FragmentFlatIn.nearPosition1.xy / FragmentFlatIn.nearPosition1.w) / 2.0;
		vec2 nearPosition2 = screenSize.xy * (1.0 + FragmentFlatIn.nearPosition2.xy / FragmentFlatIn.nearPosition2.w) / 2.0;
	
		if(distanceFromLine(nearPosition1, nearPosition2, gl_FragCoord.xy) < 1)
		{
			diffuseColor = textureLod(cachedColor, FragmentIn.realTexCoord, 0.0);
		}
		else
		{
			diffuseColor = textureLod(cachedColor, FragmentIn.texCoord, 0.0);
		}
	}
}
