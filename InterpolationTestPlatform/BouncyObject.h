#pragma once

#include "SimplePhysicalObject.h"
#include "PhysicalObject.h"

#include "ObjObject.h"

class BouncyObject : public SimplePhysicalObject, public PhysicalObject, public ObjObject
{
public:
	BouncyObject();
	virtual ~BouncyObject();

	virtual std::vector<glm::dvec3> evalF(std::vector<glm::dvec3> state);
	virtual void resetState();
	virtual void resetState(glm::dvec2 limitMin, glm::dvec2 limitMax, glm::dvec2 position, double depth, glm::dvec2 velocity, double diameter);

	virtual void shiftSystem(bool forward);

private:
	virtual bool shouldOverlay();

	virtual glm::vec3 getScale();
	virtual glm::mat4 getRotation();
	virtual glm::vec3 getPosition();
	virtual glm::vec3 getVelocity();

	virtual std::string getFilePath();
	virtual std::string getFileName();
};
