Interpolation Test Platform readme
----------------------------------

See also license.txt for license information.

This app demonstrates viewport reprojection. The scene consists of cubemapped sky cube, crytec-sponza and bouncing lego dudes. Camera isn't clipped except to the ground.
The lego dudes are affected by gravity, drag, and bounce from the floor. Runge-Kutta integration with 10 ms timestep is used. Additionaly camera can move. Rest of the scene is static.
When interpolation is turned on a fixed ratio of frames is rendered and rest are interpolated. Physics is simulated only during real frames, however per-pixel velocities are saved in a buffer.
Interpolation reprojects each rendered pixel by first multipling with old MVP matrix and then reprojecting with new MVP matrix. In between time dependent translation from velocity buffer is applied.
Thus while forces aren't updated during interpolation, smooth motion is.


Basic controls:
	ESC
		Quit!
	mouse movement
		Rotate the camera.
	left mouse
		Shoot. At this point just prints the object under crosshair to console.
	W
		Hold to move forward.
	A
		Hold to move left.
	S
		Hold to move back.
	D
		Hold to move right.
	SPACE
		Hold to move up.
	LEFT CTRL
		Hold to move down.
	LEFT SHIFT
		Hold to sneak at quarter speed.

Configuration buttons:
	y
		Toggle invert mouse y-axis.
	F11
		Toggle fullscreen.
	c
		Toggle capture mouse. Use to release/capture mouse cursor.
	KEYPAD DIVIDE
		Toggle interpolation on/off.
	1
		Render all frames normally. Real FPS to display refresh rate ratio is 1:1.
	2 ... 9
		Set real FPS to refresh rate ratio to 1:N.
		E.g. 1:4 means render 1 real frame per each 4 screen refreshes. Rest of frames are interpolated or skipped.
		E.g. 120 Hz display with 1:8 ratio will render real frames at 15 FPS rate.
	0
		Set real FPS to refresh rate ratio to 1:inf (0 FPS). This is an extreme example of interpolation. Turning interpolation off in this mode will freeze the screen.
		Note that 1 more real frame is always rendered before entering this mode.
	KEYPAD PLUS / MINUS
		Increase / decrease real FPS ratio. Use these to set a custom ratio.
	F5, F6, F7, F8
		Attempt to set real FPS to refresh rate ratio such that resulting real frame rate will match 15, 30, 60, or 120 FPS as close as possible. This is display refresh rate dependent.
		E.g. pressing F5 with...
		... a  60 Hz display 1:4  ratio is set (15.0 FPS).
		... a  75 Hz display 1:5  ratio is set (15.0 FPS).
		... a 120 Hz display 1:8  ratio is set (15.0 FPS).
		... a 144 Hz display 1:10 ratio is set (14.4 FPS).
	BACKSPACE
		Toggle wireframe mode.
		If 1:1 frame ratio is set, real frames are rendered in wireframe. This allows inspecting the scene.
		If other ratio is set, only interpolated frames are drawn in wireframe. This allows inspecting the interpolation grid. 1:inf ratio is recomended to avoid flickering and grid repositioning.
	h
		Toggle hide hud.
	UP
		Rise lego dudes a bit to boost their bouncing.
	DOWN
		Lower lego dudes a bit to dampen their bouncing. Warning: Lowering them below the floor will give them immense spring force rocketing them upward.

Debugging buttons:
	TAB
		Print camera position in world coordinates to console.
	k
		Decrease an obscure floating point number that might or might not do anything. Better not press it. ;)
	l
		Increase an obscure floating point number that might or might not do anything. Better not press it. ;)
	

Command line options:
	screenWidth=<width>
		Set window width. Does not affect fullscreen.
		Default: 1024
	screenHeight=<height>
		Set window height. Does not affect fullscreen.
		Default: 768
	vsync={true,false}
		Synchronize frame rate to screne refresh rate?
		Default: true
	inverseY={true,false}
		Invert mouse y-axis?
		Default: false
	fullscreen={true,false}
		Start in fullscreen?
		Default: false
	captureMouse={true,false}
		Capture mouse?
		Default: true
	interpolate={true,false}
		Use interpolation?
		Default: true
	frameSkip={1...N,inf,infinite}
		Set real FPS to refresh rate ratio to 1:N. E.g. frameSkip=4 on a 60 Hz display results in 60/4=15 FPS. Must be an integer larger than 1 or inf or infinite.
		Default: 1
	targetFps={0...N}
		Set the prefered real frame rate. Results might not be exact if refresh is not divisible by given number. Zero is valid, but freezes the screen if interpolation is disabled.
		This actually sets the same value as "frameSkip" parameter. If both are passed, targetFps takes precedence.
		Similar to F5, F6, F7, F8 keys.
		Default: screen refresh rate (same as frameSkip=1)
	wireframe={true,false}
		Start in wireframe mode?
		Default: false
	drawHudInfo={true,false}
		Hide hud?
		Default: true
	
	E.g.
		> InterpolationTestPlatform.exe screenWidth=1280 screenHeight=720 vsync=true inverseY=false fullscreen=true captureMouse=true interpolate=true wireframe=false drawHudInfo=true targetFps=15
hud help:
	"object in sight" line prints the name of the object currently in crosshair.
	"interpolation" line prints whether frame interpolation is enabled.
	"wireframe mode" line prints whether wireframe mode is enabled.
	"target fps" line prints current target FPS and real frame ratio to refresh rate
