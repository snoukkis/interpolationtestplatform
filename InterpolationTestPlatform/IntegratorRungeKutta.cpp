#include "IntegratorRungeKutta.h"

void RungeKuttaIntegrator::stepSystem()
{
	std::vector<glm::dvec3>  nextState(physicalObject.getState().size());
	std::vector<glm::dvec3>  finalState(physicalObject.getState().size());

	// CALCULATE k1
	std::vector<glm::dvec3> f = physicalObject.evalF(physicalObject.getState());
	for (unsigned valueIndex = 0; valueIndex < physicalObject.getState().size(); valueIndex++)
	{
		glm::dvec3 k = timeStep * f[valueIndex];
		finalState[valueIndex] = physicalObject.getState()[valueIndex] + k / 6.0;

		nextState[valueIndex] = physicalObject.getState()[valueIndex] + 0.5 * k;
	}

	// CALCULATE k2
	f = physicalObject.evalF(nextState);
	for (unsigned valueIndex = 0; valueIndex < physicalObject.getState().size(); valueIndex++)
	{
		glm::dvec3 k = timeStep * f[valueIndex];
		finalState[valueIndex] += k / 3.0;

		nextState[valueIndex] = physicalObject.getState()[valueIndex] + 0.5 * k;
	}

	// CALCULATE k3
	f = physicalObject.evalF(nextState);
	for (unsigned valueIndex = 0; valueIndex < physicalObject.getState().size(); valueIndex++)
	{
		glm::dvec3 k = timeStep * f[valueIndex];
		finalState[valueIndex] += k / 3.0;

		nextState[valueIndex] = physicalObject.getState()[valueIndex] + k;
	}

	// CALCULATE k4
	f = physicalObject.evalF(nextState);
	for (unsigned valueIndex = 0; valueIndex < physicalObject.getState().size(); valueIndex++)
	{
		glm::dvec3 k = timeStep * f[valueIndex];
		finalState[valueIndex] += k / 6.0;
	}

	physicalObject.setState(finalState);
}
