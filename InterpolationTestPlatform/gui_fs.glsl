#version 410

layout(location = 0) out vec4 color;

in vec2 texCoord;

struct Textures
{
	sampler2D gui;
};
uniform Textures textures;

void main()
{
	color = texture2D(textures.gui, texCoord);
}
