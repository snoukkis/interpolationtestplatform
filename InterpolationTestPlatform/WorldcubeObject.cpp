#include "WorldcubeObject.h"
#include "Forces.h"
#include <iostream>
#include <map>
#include "UtilModule.h"

#define GLM_SWIZZLE
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
//#include <glm/gtc/matrix_access.hpp>
#include <glm/gtc/type_ptr.hpp>

WorldcubeObject::WorldcubeObject()
{
}

void WorldcubeObject::initDraw(AttributeLocations& attributeLocations)
{
	const GLshort worldcubeIndices[] =
	{
		// front: 0, 1, 2, 3
		0, 1, 2,
		3, 2, 1,

		// back: 4, 5, 6, 7
		6, 5, 4,
		5, 6, 7,

		// left: 1, 3, 5, 7
		5, 3, 1,
		3, 5, 7,

		// right: 0, 2, 4, 6
		0, 2, 4,
		6, 4, 2,

		// top: 0, 1, 4, 5
		4, 1, 0,
		1, 4, 5,

		// bottom: 8, 9, 10, 11
		8, 9, 10,
		11, 10, 9,
	};

	worldcubeIndexCount = sizeof(worldcubeIndices) / sizeof(GLshort);

	const float worldcubePositions[] =
	{
		+1.0f, +1.0f, -1.0f, // 0: left, top, front
		-1.0f, +1.0f, -1.0f, // 1: right, top, front
		+1.0f, -1.001f, -1.0f, // 2: left, bottom, front
		-1.0f, -1.001f, -1.0f, // 3: right, bottom, front
		+1.0f, +1.0f, +1.0f, // 4: left, top, back
		-1.0f, +1.0f, +1.0f, // 5: right, top, back
		+1.0f, -1.001f, +1.0f, // 6: left, bottom, back
		-1.0f, -1.001f, +1.0f, // 7: right, bottom, back

		// floor
		+1.0f, -1.001f, -1.0f, // 8: left, bottom, front
		-1.0f, -1.001f, -1.0f, // 9: right, bottom, front
		+1.0f, -1.001f, +1.0f, // 10: left, bottom, back
		-1.0f, -1.001f, +1.0f, // 11: right, bottom, back
	};

	const float worldcubeNormals[] =
	{
		-1.0f, -1.0f, +1.0f, // 0: left, top, front
		+1.0f, -1.0f, +1.0f, // 1: right, top, front
		-1.0f, +1.0f, +1.0f, // 2: left, bottom, front
		+1.0f, +1.0f, +1.0f, // 3: right, bottom, front
		-1.0f, -1.0f, -1.0f, // 4: left, top, back
		+1.0f, -1.0f, -1.0f, // 5: right, top, back
		-1.0f, +1.0f, -1.0f, // 6: left, bottom, back
		+1.0f, +1.0f, -1.0f, // 7: right, bottom, back

		// floor
		-1.0f, +1.0f, +1.0f, // 8: left, bottom, front
		+1.0f, +1.0f, +1.0f, // 9: right, bottom, front
		-1.0f, +1.0f, -1.0f, // 10: left, bottom, back
		+1.0f, +1.0f, -1.0f, // 11: right, bottom, back
	};

	const float worldcubeTextureCoordinates[] =
	{
		+1.0f, +1.0f, -1.0f, // 0: left, top, front
		-1.0f, +1.0f, -1.0f, // 1: right, top, front
		+1.0f, -1.0f, -1.0f, // 2: left, bottom, front
		-1.0f, -1.0f, -1.0f, // 3: right, bottom, front
		+1.0f, +1.0f, +1.0f, // 4: left, top, back
		-1.0f, +1.0f, +1.0f, // 5: right, top, back
		+1.0f, -1.0f, +1.0f, // 6: left, bottom, back
		-1.0f, -1.0f, +1.0f, // 7: right, bottom, back

		// floor
		+1.0f, -1.0f, -1.0f, // 8: left, bottom, front
		-1.0f, -1.0f, -1.0f, // 9: right, bottom, front
		+1.0f, -1.0f, +1.0f, // 10: left, bottom, back
		-1.0f, -1.0f, +1.0f, // 11: right, bottom, back
	};

	const float worldcubeTextureCoordinateScales[] =
	{
		1.0f, 1.0f, // 0: left, top, front
		1.0f, 1.0f, // 1: right, top, front
		1.0f, 1.0f, // 2: left, bottom, front
		1.0f, 1.0f, // 3: right, bottom, front
		1.0f, 1.0f, // 4: left, top, back
		1.0f, 1.0f, // 5: right, top, back
		1.0f, 1.0f, // 6: left, bottom, back
		1.0f, 1.0f, // 7: right, bottom, back

		// floor
		1.0f, 1.0f, // 8: left, bottom, front
		1.0f, 1.0f, // 9: right, bottom, front
		1.0f, 1.0f, // 10: left, bottom, back
		1.0f, 1.0f, // 11: right, bottom, back
	};

	/*
	// emit all the color
	const float worldcubeEmissionColors[] =
	{
	0.0f, 0.0f, 1.0f, // 0: left, top, front
	0.0f, 0.0f, 1.0f, // 1: right, top, front
	1.0f, 0.0f, 0.0f, // 2: left, bottom, front
	1.0f, 0.0f, 0.0f, // 3: right, bottom, front
	0.0f, 0.0f, 1.0f, // 4: left, top, back
	0.0f, 0.0f, 1.0f, // 5: right, top, back
	1.0f, 0.0f, 0.0f, // 6: left, bottom, back
	1.0f, 0.0f, 0.0f, // 7: right, bottom, back

	// floor
	0.0f, 1.0f, 0.0f, // 8: left, bottom, front
	0.0f, 1.0f, 0.0f, // 9: right, bottom, front
	0.0f, 1.0f, 0.0f, // 10: left, bottom, back
	0.0f, 1.0f, 0.0f, // 11: right, bottom, back
	};
	*/
	// zero emission, use ambient instead...
	const float worldcubeEmissionColors[] =
	{
		0.0f, 0.0f, 0.0f, // 0: left, top, front
		0.0f, 0.0f, 0.0f, // 1: right, top, front
		0.0f, 0.0f, 0.0f, // 2: left, bottom, front
		0.0f, 0.0f, 0.0f, // 3: right, bottom, front
		0.0f, 0.0f, 0.0f, // 4: left, top, back
		0.0f, 0.0f, 0.0f, // 5: right, top, back
		0.0f, 0.0f, 0.0f, // 6: left, bottom, back
		0.0f, 0.0f, 0.0f, // 7: right, bottom, back

		// floor
		0.0f, 0.0f, 0.0f, // 8: left, bottom, front
		0.0f, 0.0f, 0.0f, // 9: right, bottom, front
		0.0f, 0.0f, 0.0f, // 10: left, bottom, back
		0.0f, 0.0f, 0.0f, // 11: right, bottom, back
	};

	const float worldcubeAmbientColors[] =
	{
		0.0f, 0.0f, 0.0f, // 0: left, top, front
		0.0f, 0.0f, 0.0f, // 1: right, top, front
		0.0f, 0.0f, 0.0f, // 2: left, bottom, front
		0.0f, 0.0f, 0.0f, // 3: right, bottom, front
		0.0f, 0.0f, 0.0f, // 4: left, top, back
		0.0f, 0.0f, 0.0f, // 5: right, top, back
		0.0f, 0.0f, 0.0f, // 6: left, bottom, back
		0.0f, 0.0f, 0.0f, // 7: right, bottom, back

		// floor
		0.0f, 0.0f, 0.0f, // 8: left, bottom, front
		0.0f, 0.0f, 0.0f, // 9: right, bottom, front
		0.0f, 0.0f, 0.0f, // 10: left, bottom, back
		0.0f, 0.0f, 0.0f, // 11: right, bottom, back
	};

	const float worldcubeDiffuseColors[] =
	{
		0.0f, 0.0f, 0.0f, // 0: left, top, front
		0.0f, 0.0f, 0.0f, // 1: right, top, front
		0.0f, 0.0f, 0.0f, // 2: left, bottom, front
		0.0f, 0.0f, 0.0f, // 3: right, bottom, front
		0.0f, 0.0f, 0.0f, // 4: left, top, back
		0.0f, 0.0f, 0.0f, // 5: right, top, back
		0.0f, 0.0f, 0.0f, // 6: left, bottom, back
		0.0f, 0.0f, 0.0f, // 7: right, bottom, back

		// floor
		0.0f, 0.0f, 0.0f, // 8: left, bottom, front
		0.0f, 0.0f, 0.0f, // 9: right, bottom, front
		0.0f, 0.0f, 0.0f, // 10: left, bottom, back
		0.0f, 0.0f, 0.0f, // 11: right, bottom, back
	};

	const float worldcubeSpecularColors[] =
	{
		0.0f, 0.0f, 0.0f, // 0: left, top, front
		0.0f, 0.0f, 0.0f, // 1: right, top, front
		0.0f, 0.0f, 0.0f, // 2: left, bottom, front
		0.0f, 0.0f, 0.0f, // 3: right, bottom, front
		0.0f, 0.0f, 0.0f, // 4: left, top, back
		0.0f, 0.0f, 0.0f, // 5: right, top, back
		0.0f, 0.0f, 0.0f, // 6: left, bottom, back
		0.0f, 0.0f, 0.0f, // 7: right, bottom, back

		// floor
		0.0f, 0.0f, 0.0f, // 8: left, bottom, front
		0.0f, 0.0f, 0.0f, // 9: right, bottom, front
		0.0f, 0.0f, 0.0f, // 10: left, bottom, back
		0.0f, 0.0f, 0.0f, // 11: right, bottom, back
	};

	const float worldcubeShininess[] =
	{
		1.0f, // 0: left, top, front
		1.0f, // 1: right, top, front
		1.0f, // 2: left, bottom, front
		1.0f, // 3: right, bottom, front
		1.0f, // 4: left, top, back
		1.0f, // 5: right, top, back
		1.0f, // 6: left, bottom, back
		1.0f, // 7: right, bottom, back

		// floor
		1.0f, // 8: left, bottom, front
		1.0f, // 9: right, bottom, front
		1.0f, // 10: left, bottom, back
		1.0f, // 11: right, bottom, back
	};


	glGenVertexArrays(1, &worldcubeVertexArrayObject);
	glBindVertexArray(worldcubeVertexArrayObject);
	{
		glGenBuffers(1, &worldcubeIndexBufferObject);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, worldcubeIndexBufferObject);
		{
			glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(worldcubeIndices), worldcubeIndices, GL_STATIC_DRAW);
		}
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);



		glGenBuffers(1, &worldcubePositionsVertexBufferObject);
		glBindBuffer(GL_ARRAY_BUFFER, worldcubePositionsVertexBufferObject);
		{
			glBufferData(GL_ARRAY_BUFFER, sizeof(worldcubePositions), worldcubePositions, GL_STATIC_DRAW);
			glEnableVertexAttribArray(attributeLocations.position);
			glVertexAttribPointer(attributeLocations.position, 3, GL_FLOAT, GL_FALSE, 0, NULL);
		}
		glBindBuffer(GL_ARRAY_BUFFER, 0);

		glGenBuffers(1, &worldcubeNormalsVertexBufferObject);
		glBindBuffer(GL_ARRAY_BUFFER, worldcubeNormalsVertexBufferObject);
		{
			glBufferData(GL_ARRAY_BUFFER, sizeof(worldcubeNormals), worldcubeNormals, GL_STATIC_DRAW);
			glEnableVertexAttribArray(attributeLocations.normal);
			glVertexAttribPointer(attributeLocations.normal, 3, GL_FLOAT, GL_FALSE, 0, NULL);
		}
		glBindBuffer(GL_ARRAY_BUFFER, 0);

		glGenBuffers(1, &worldcubeTextureCoordinatesVertexBufferObject);
		glBindBuffer(GL_ARRAY_BUFFER, worldcubeTextureCoordinatesVertexBufferObject);
		{
			glBufferData(GL_ARRAY_BUFFER, sizeof(worldcubeTextureCoordinates), worldcubeTextureCoordinates, GL_STATIC_DRAW);
			glEnableVertexAttribArray(attributeLocations.texcoord);
			glVertexAttribPointer(attributeLocations.texcoord, 3, GL_FLOAT, GL_FALSE, 0, NULL);
		}
		glBindBuffer(GL_ARRAY_BUFFER, 0);

		glGenBuffers(1, &worldcubeTextureCoordinateScalesVertexBufferObject);
		glBindBuffer(GL_ARRAY_BUFFER, worldcubeTextureCoordinateScalesVertexBufferObject);
		{
			glBufferData(GL_ARRAY_BUFFER, sizeof(worldcubeTextureCoordinateScales), worldcubeTextureCoordinateScales, GL_STATIC_DRAW);
			glEnableVertexAttribArray(attributeLocations.texcoordScale);
			glVertexAttribPointer(attributeLocations.texcoordScale, 2, GL_FLOAT, GL_FALSE, 0, NULL);
		}
		glBindBuffer(GL_ARRAY_BUFFER, 0);



		glGenBuffers(1, &worldcubeEmissionColorsVertexBufferObject);
		glBindBuffer(GL_ARRAY_BUFFER, worldcubeEmissionColorsVertexBufferObject);
		{
			glBufferData(GL_ARRAY_BUFFER, sizeof(worldcubeEmissionColors), worldcubeEmissionColors, GL_STATIC_DRAW);
			glEnableVertexAttribArray(attributeLocations.emission);
			glVertexAttribPointer(attributeLocations.emission, 3, GL_FLOAT, GL_FALSE, 0, NULL);
		}
		glBindBuffer(GL_ARRAY_BUFFER, 0);

		glGenBuffers(1, &worldcubeAmbientColorsVertexBufferObject);
		glBindBuffer(GL_ARRAY_BUFFER, worldcubeAmbientColorsVertexBufferObject);
		{
			glBufferData(GL_ARRAY_BUFFER, sizeof(worldcubeAmbientColors), worldcubeAmbientColors, GL_STATIC_DRAW);
			glEnableVertexAttribArray(attributeLocations.ambient);
			glVertexAttribPointer(attributeLocations.ambient, 3, GL_FLOAT, GL_FALSE, 0, NULL);
		}
		glBindBuffer(GL_ARRAY_BUFFER, 0);

		glGenBuffers(1, &worldcubeDiffuseColorsVertexBufferObject);
		glBindBuffer(GL_ARRAY_BUFFER, worldcubeDiffuseColorsVertexBufferObject);
		{
			glBufferData(GL_ARRAY_BUFFER, sizeof(worldcubeDiffuseColors), worldcubeDiffuseColors, GL_STATIC_DRAW);
			glEnableVertexAttribArray(attributeLocations.diffuse);
			glVertexAttribPointer(attributeLocations.diffuse, 3, GL_FLOAT, GL_FALSE, 0, NULL);
		}
		glBindBuffer(GL_ARRAY_BUFFER, 0);

		glGenBuffers(1, &worldcubeSpecularColorsVertexBufferObject);
		glBindBuffer(GL_ARRAY_BUFFER, worldcubeSpecularColorsVertexBufferObject);
		{
			glBufferData(GL_ARRAY_BUFFER, sizeof(worldcubeSpecularColors), worldcubeSpecularColors, GL_STATIC_DRAW);
			glEnableVertexAttribArray(attributeLocations.specular);
			glVertexAttribPointer(attributeLocations.specular, 3, GL_FLOAT, GL_FALSE, 0, NULL);
		}
		glBindBuffer(GL_ARRAY_BUFFER, 0);

		glGenBuffers(1, &worldcubeShininessVertexBufferObject);
		glBindBuffer(GL_ARRAY_BUFFER, worldcubeShininessVertexBufferObject);
		{
			glBufferData(GL_ARRAY_BUFFER, sizeof(worldcubeShininess), worldcubeShininess, GL_STATIC_DRAW);
			glEnableVertexAttribArray(attributeLocations.shininess);
			glVertexAttribPointer(attributeLocations.shininess, 1, GL_FLOAT, GL_FALSE, 0, NULL);
		}
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}
	glBindVertexArray(0);


	glEnable(GL_TEXTURE_CUBE_MAP);
	glEnable(GL_TEXTURE_CUBE_MAP_SEAMLESS);

	//const std::string imageFiles[] = { "images/xpos.png", "images/xneg.png", "images/xneg.png", "images/xneg.png", "images/xneg.png", "images/xneg.png", };

	glm::ivec2 xposSize; uint32_t* xpos = UtilModule::loadImage("images/xpos.png", xposSize.x, xposSize.y);
	glm::ivec2 xnegSize; uint32_t* xneg = UtilModule::loadImage("images/xneg.png", xnegSize.x, xnegSize.y);
	glm::ivec2 yposSize; uint32_t* ypos = UtilModule::loadImage("images/ypos.png", yposSize.x, yposSize.y);
	glm::ivec2 ynegSize; uint32_t* yneg = UtilModule::loadImage("images/yneg.png", ynegSize.x, ynegSize.y);
	glm::ivec2 zposSize; uint32_t* zpos = UtilModule::loadImage("images/zpos.png", zposSize.x, zposSize.y);
	glm::ivec2 znegSize; uint32_t* zneg = UtilModule::loadImage("images/zneg.png", znegSize.x, znegSize.y);

	assert(xposSize == xnegSize && xposSize == yposSize && xposSize == ynegSize && xposSize == zposSize && xposSize == znegSize);

	glGenTextures(1, &worldcubeTextureObject);
	glBindTexture(GL_TEXTURE_CUBE_MAP, worldcubeTextureObject);
	{
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

		/*
		glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X, 0, GL_RGBA8, xposSize.x, xposSize.y, 0, GL_RGBA, GL_UNSIGNED_BYTE, xpos);
		glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_X, 0, GL_RGBA8, xnegSize.x, xnegSize.y, 0, GL_RGBA, GL_UNSIGNED_BYTE, xneg);
		glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Y, 0, GL_RGBA8, yposSize.x, yposSize.y, 0, GL_RGBA, GL_UNSIGNED_BYTE, ypos);
		glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Y, 0, GL_RGBA8, ynegSize.x, ynegSize.y, 0, GL_RGBA, GL_UNSIGNED_BYTE, yneg);
		glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Z, 0, GL_RGBA8, zposSize.x, zposSize.y, 0, GL_RGBA, GL_UNSIGNED_BYTE, zpos);
		glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Z, 0, GL_RGBA8, znegSize.x, znegSize.y, 0, GL_RGBA, GL_UNSIGNED_BYTE, zneg);
		*/
		glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_X, 0, GL_RGBA8, xposSize.x, xposSize.y, 0, GL_RGBA, GL_UNSIGNED_BYTE, xpos);
		glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X, 0, GL_RGBA8, xnegSize.x, xnegSize.y, 0, GL_RGBA, GL_UNSIGNED_BYTE, xneg);
		glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Y, 0, GL_RGBA8, yposSize.x, yposSize.y, 0, GL_RGBA, GL_UNSIGNED_BYTE, ypos);
		glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Y, 0, GL_RGBA8, ynegSize.x, ynegSize.y, 0, GL_RGBA, GL_UNSIGNED_BYTE, yneg);
		glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Z, 0, GL_RGBA8, zposSize.x, zposSize.y, 0, GL_RGBA, GL_UNSIGNED_BYTE, zpos);
		glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Z, 0, GL_RGBA8, znegSize.x, znegSize.y, 0, GL_RGBA, GL_UNSIGNED_BYTE, zneg);
	}
	glBindTexture(GL_TEXTURE_CUBE_MAP, 0);

	delete zneg;
	delete zpos;
	delete yneg;
	delete ypos;
	delete xneg;
	delete xpos;
}

WorldcubeObject::~WorldcubeObject()
{
	glDeleteVertexArrays(1, &worldcubeVertexArrayObject);
	glDeleteBuffers(1, &worldcubePositionsVertexBufferObject);
	glDeleteBuffers(1, &worldcubeNormalsVertexBufferObject);
	glDeleteBuffers(1, &worldcubeEmissionColorsVertexBufferObject);
	glDeleteBuffers(1, &worldcubeAmbientColorsVertexBufferObject);
	glDeleteBuffers(1, &worldcubeDiffuseColorsVertexBufferObject);
	glDeleteBuffers(1, &worldcubeSpecularColorsVertexBufferObject);
	glDeleteBuffers(1, &worldcubeShininessVertexBufferObject);
	glDeleteBuffers(1, &worldcubeTextureCoordinatesVertexBufferObject);
	glDeleteBuffers(1, &worldcubeTextureCoordinateScalesVertexBufferObject);
	glDeleteBuffers(1, &worldcubeIndexBufferObject);
	glDeleteTextures(1, &worldcubeTextureObject);
}

void WorldcubeObject::drawDerived(const RenderUniformLocations& uniformLocations, const glm::mat4& fromWorldToLastRenderViewMatrix)
{
	glm::mat4 worldcubeFromModelToWorldMatrix(1.0f);
	worldcubeFromModelToWorldMatrix = glm::scale(worldcubeFromModelToWorldMatrix, glm::vec3(4096.0f, 4096.0f, 4096.0f));
	worldcubeFromModelToWorldMatrix = glm::translate(worldcubeFromModelToWorldMatrix, glm::vec3(0.0f, 0.0f, 0.0f));

	glm::mat4 worldcubeFromModelToViewMatrix = fromWorldToLastRenderViewMatrix * worldcubeFromModelToWorldMatrix;
	glm::mat4 worldcubeFromModelToViewNormalMatrix = glm::transpose(glm::inverse(worldcubeFromModelToViewMatrix));;

	glm::vec3 velocity = glm::vec3(0.0, 0.0, 0.0); // the world is not moving
	glUniform3fv(uniformLocations.velocity, 1, glm::value_ptr(velocity));

	glUniformMatrix4fv(uniformLocations.fromModelToViewMatrix, 1, GL_FALSE, glm::value_ptr(worldcubeFromModelToViewMatrix));
	glUniformMatrix4fv(uniformLocations.fromModelToViewNormalMatrix, 1, GL_FALSE, glm::value_ptr(worldcubeFromModelToViewNormalMatrix));
	glBindVertexArray(worldcubeVertexArrayObject);
	{
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, worldcubeIndexBufferObject);
		{
			glUniform1i(uniformLocations.skyTexture, 1);
			glActiveTexture(GL_TEXTURE1); glBindTexture(GL_TEXTURE_CUBE_MAP, worldcubeTextureObject);
			{
				glDrawElements(GL_TRIANGLES, worldcubeIndexCount, GL_UNSIGNED_SHORT, 0);
			}
			glActiveTexture(GL_TEXTURE1); glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
		}
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	}
	glBindVertexArray(0);
}
