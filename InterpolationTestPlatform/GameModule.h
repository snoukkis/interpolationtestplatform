#pragma once

#include <vector>
#include <deque>
#include <string>
#include <iostream>
#include <fstream>

#include "Timer.h"

#define TIMEOUT_DURATION 3.0 // seconds
#define INTERLUDE_DURATION 1.0 // seconds

#define STAGES 1
#define SHOTS_PER_TEST_PER_STAGE 225

#define PRACTICE_FULL_EVALUATION_WINDOW 32 // number of practice shot to remember
#define PRACTICE_INITIAL_QUICK_EVALUATION_WINDOW 1 // number of practice successful practice shot to level up after

struct Shot
{
	enum Status
	{
		PENDING,
		HIT,
		MISS,
		TIME_OUT
	};
	Status status = PENDING;

	enum ShotType
	{
		TUTORIAL,
		EASY_PRACTICE,
		HARD_PRACTICE,
		OTHER_PRACTICE,
		TEST
	};
	ShotType shotType;

	bool interpolating;
	int frameSkip;
	
	double difficulty;
	
	// [0...1] in normalized screen space
	double targetInitialX;
	double targetInitialY;

	// Screens per second. Normalized to maximum screen dimension (width or height, whichever is larger).
	double targetVelocity;

	// [0 == 360[ in degress, 0 == right
	double targetDirection;

	double timeOut;
	double aimingStarted = -1.0; // absolute app time
	double shotTaken = -1.0; // relative time
	double shotRegistered = -1.0; // relative time
};

struct Stage
{
	int difficulty;

	std::deque<Shot> remainingShots;
	std::vector<Shot> takenShots;
};

class GameModule
{
public:
	GameModule(std::string reportFileName);
	virtual ~GameModule();
	void reset(std::string reportFileName);

	Shot beginShot();
	void registerShot(Shot shot);
	void reportShot(int stage, int shot, double difficulty, int quickEvaluationWindowSize, double timeOut, Shot::Status status, double aimingStarted, double shotTaken, double shotRegistered, bool practice, bool interpolating, int frameSkip);

	bool isGameEnded();

	double getInterludeDuration();

	bool isPractice();
	bool isTutorial();
	void endTutorial();

private:
	bool tutorial;

	struct TestType
	{
		bool interpolating;
		int frameskip;

		bool operator==(const TestType& other)
		{
			return interpolating == other.interpolating && frameskip == other.frameskip;
		}

		bool operator!=(const TestType& other)
		{
			return !(*this == other);
		}
	};

	double practiceEvaluation(int historyLookBackDepth);
	void startNextStage();

	static double getTargetVelocityForDifficulty(double difficulty);

	std::ofstream file;

	std::string reportFileName;

	bool shotPending;

	int difficulty;

	int tutorialShotsTaken;

	bool practice;
	int quickEvaluationWindow;
	std::deque<Shot> practiceShots;
	int practiceShotsTaken;

	std::vector<Stage> stages;

	// let's think of the following as poor mans of java enums
	std::vector<TestType> possibleInterpolatingAndFrameskipPairs;
	TestType easiestInterpolatingAndFrameskipPair;
	TestType hardestInterpolatingAndFrameskipPair;

	Timer practiceTimer;
};
