#include "DrawableObject.h"

GLubyte DrawableObject::nextUnusedHitId = 1; // 0 == undefined

DrawableObject::DrawableObject() : hitId(nextUnusedHitId)
{
	nextUnusedHitId++;
}

// render the system (ie draw the particles)
void DrawableObject::draw(const RenderUniformLocations& uniformLocations, const glm::mat4& fromWorldToLastRenderViewMatrix)
{
	if (drawEnabled)
	{
		glUniform1ui(uniformLocations.hitId, hitId);

		drawDerived(uniformLocations, fromWorldToLastRenderViewMatrix);
	}
}

GLubyte DrawableObject::getHitId()
{
	return hitId;
}

void DrawableObject::setDrawEnabled(bool enabled)
{
	drawEnabled = enabled;
}

bool DrawableObject::getDrawEnabled()
{
	return drawEnabled;
}
