#pragma once

#include <vector>
#include <map>
#include <gl\glew.h>
#include "DrawableObject.h"
#include "PhysicsModule.h"
#include "UtilModule.h"
#define GLM_SWIZZLE
#include <glm/glm.hpp>

#include "GLStructs.h"

class GLModule
{
private:
	// references to other modules
	PhysicsModule* physicsModule;

	// current viewport size
	int screenWidth;
	int screenHeight;

	// render shader attribute locations
	AttributeLocations attributeLocations;

	// warp shader attribute locations
	GLint warpPositionAttributeLocation = -1;

	// render shader attribute locations
	AttributeLocations guiAttributeLocations;

	// render shader material uniform locations
	RenderUniformLocations uniformLocations;
	
	// 2D gui shader uniform locations
	GuiUniformLocations guiUniformLocations;
	
	// warp shader grid uniform locations
	GLuint warpGridSizeUniformLocation = -1;
	GLuint warpFlipGridBorderUniformLocation = -1;

	// warp shader velocity interpolation uniform locations
	GLuint warpTimeSinceRealFrameUniformLocation = -1;
	
	GLubyte hitId = 0;

	// debug
public:
	void changeOffsetten(float amount);
private:
	float warpOffsetten = 0.0f;
	GLuint warpOffsettenUniformLocation = -1;

	// warp shader framebuffer uniform locations
	GLint warpCachedColorTextureUniformLocation = -1;
	GLint warpCachedVelocityTextureUniformLocation = -1;
	GLint warpCachedDepthTextureUniformLocation = -1;

	// warp shader matrix uniform locations
	GLint warpFromWorldToViewMatrixUniformLocation = -1;
	GLint warpFromViewToProjectionMatrixUniformLocation = -1;
	GLint warpFromProjectionToViewMatrixUniformLocation = -1;
	GLint warpFromLastRenderViewToWorldMatrixUniformLocation = -1;

	// transformation matrices
	glm::mat4 fromProjectionToViewMatrix;
	glm::mat4 fromLastRenderViewToWorldMatrix;
	bool fromLastRenderViewToWorldMatrixStale = true;
	glm::mat4 fromWorldToLastRenderViewMatrix;
	glm::mat4 fromViewToProjectionMatrix;

	float lastCameraXRotationAngle;

	// scene lights
	std::vector<Light> lights;

	// drawable objects
	std::vector<DrawableObject*>& drawableObjects;

	// warp grid
	GLuint warpVertexArrayObject = 0;
	GLuint warpGridPositionsVertexBufferObject = 0;
	GLuint warpIndexBufferObject = 0;
	GLsizei warpIndexCount = 0;
	GLuint warpGridWidth = 0;
	GLuint warpGridHeight = 0;

	// gui
	GLuint guiVertexArrayObject = 0;
	GLuint guiPositionsVertexBufferObject = 0;
	GLuint guiIndexBufferObject = 0;
	GLsizei guiIndexCount = 0;
	GLuint guiTextureObject = 0;
	GLuint hitTextureObject = 0;
	GLuint missTextureObject = 0;
	GLuint timeoutTextureObject = 0;
	GLuint crosshairTextureObject = 0;
	int textWidth;
	int textHeight;
	int hitWidth;
	int hitHeight;
	int missWidth;
	int missHeight;
	int timeoutWidth;
	int timeoutHeight;
	int crosshairWidth;
	int crosshairHeight;
	static const UtilColor hudColor;
	static const UtilColor successColor;
	static const UtilColor failColor;

	// shader programs
	GLuint renderShaderProgram = 0;
	GLuint renderVertexShader = 0;
	GLuint renderFragmentShader = 0;

	GLuint warpShaderProgram = 0;
	GLuint warpVertexShader = 0;
	GLuint warpTessControlShader = 0;
	GLuint warpTessEvaluationShader = 0;
	GLuint warpGeometryShader = 0;
	GLuint warpFragmentShader = 0;

	GLuint guiShaderProgram = 0;
	GLuint guiVertexShader = 0;
	GLuint guiFragmentShader = 0;

	// offscreen framebuffer
	GLuint offscreenFramebufferObject = 0;
	GLuint offscreenColorTextureObject = 0;
	GLuint offscreenVelocityTextureObject = 0;
	GLuint offscreenHitTextureObject = 0;
	GLuint offscreenDepthTextureObject = 0;

	// wireframe mode
	bool wireframeMode = false;

public:
	enum FeedbackType
	{
		HIT,
		MISS,
		TIMEOUT,
		INVISIBLE
	};

	GLModule(PhysicsModule* physicsModule, std::vector<DrawableObject*>& drawableObjects, int screenWidth, int screenHeight);
	~GLModule();

	void renderScene();
	void setupLights();
	void updateLights();
	void readHit();
	GLubyte getHitId();
	void blitOffscreenToScreen();
	void renderHudCrosshair();
	void renderHudInfo();
	void renderHudFeedback(FeedbackType feedbackType);
	void warp(float timeSinceRealFrame); // warp last render

	void updateHud(std::string text);

	void setPolygonMode(bool wireframeMode);
	void togglePolygonMode();

	void resizeWindow(int screenWidth, int screenHeight);

	/* This depends on current viewport! (screen size, fov, camera position, etc.)
	 * Use PhysicsModule::resetCamera to get consistent results.
	 * Recompute after resizeWindow!
	 */
	glm::dvec3 fromDevicePositionToWorldPosition(glm::dvec3 devicePosition);

private:
	// initialization methods
	void initOpenGLBasics();
	void initDrawableObjects();
	void initWarpGrid();
	void initFbo();
	void initRenderShader();
	void initWarpShader();
	void initGuiShader();
	void initProjectionMatrix();
	void initLights();
	void initGui();

	GLuint createShaderFromFile(std::string file, GLenum type);
	void printProgramLog(GLuint program);
	void printShaderLog(GLuint shader);

	glm::mat4 calculateFromViewToProjectionMatrix();
	glm::mat4 calculateFromWorldToViewMatrix();
};
