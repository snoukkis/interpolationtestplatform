#pragma once

#define GLM_SWIZZLE
#include <glm/glm.hpp>
#include <gl\glew.h>
#include <vector>

#include "DrawableObject.h"

#include "GLStructs.h"

class WorldcubeObject : public DrawableObject
{
public:
	WorldcubeObject();
	virtual void initDraw(AttributeLocations& attributeLocations);
	virtual ~WorldcubeObject();

protected:
	virtual void drawDerived(const RenderUniformLocations& uniformLocations, const glm::mat4& fromWorldToLastRenderViewMatrix);

private:
	GLuint worldcubeVertexArrayObject = 0;
	GLuint worldcubePositionsVertexBufferObject = 0;
	GLuint worldcubeNormalsVertexBufferObject = 0;
	GLuint worldcubeEmissionColorsVertexBufferObject = 0;
	GLuint worldcubeAmbientColorsVertexBufferObject = 0;
	GLuint worldcubeDiffuseColorsVertexBufferObject = 0;
	GLuint worldcubeSpecularColorsVertexBufferObject = 0;
	GLuint worldcubeShininessVertexBufferObject = 0;
	GLuint worldcubeTextureCoordinatesVertexBufferObject = 0;
	GLuint worldcubeTextureCoordinateScalesVertexBufferObject = 0;
	GLuint worldcubeIndexBufferObject = 0;
	GLsizei worldcubeIndexCount = 0;

	GLuint worldcubeTextureObject = 0;
};
