#pragma once

#define GLM_SWIZZLE
#include <glm/glm.hpp>
#include <vector>

class SimplePhysicalObject
{
private:
	struct Intersection
	{
		enum Axis
		{
			VERTICAL,
			HORIZONTAL
		};
		Axis axis;

		enum Result
		{
			FOREVER,
			NEVER,
			FUTURE,
			PAST
		};
		Result result;

		double time;
	};

	Intersection intersectsAt(glm::dvec2 l0, glm::dvec2 v, glm::dvec2 p0, glm::dvec2 p1);

protected:

	glm::dvec2 position;
	glm::dvec2 velocity;
	double z;
	double radius;

	glm::dvec2 limitMin;
	glm::dvec2 limitMax;

public:

	void shiftSystem(bool forward);
	bool stepSystem(double frametime);

	virtual void resetState(glm::dvec2 limitMin, glm::dvec2 limitMax, glm::dvec2 position, double depth, glm::dvec2 velocity, double radius) = 0;
};
