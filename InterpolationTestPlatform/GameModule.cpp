#include "GameModule.h"

#include <cstdlib>
#include <cmath>
#include <ctime>
#include <cassert>

#include <algorithm>

GameModule::GameModule(std::string reportFileName) : reportFileName(reportFileName)
{
	srand((unsigned int)time(0));

	std::cout << std::fixed << std::showpos;
	std::cout.precision(2);

	// these are assuming base FPS is 120

	possibleInterpolatingAndFrameskipPairs.push_back(TestType{ false, 1 }); // 120.0 FPS
	easiestInterpolatingAndFrameskipPair = possibleInterpolatingAndFrameskipPairs.back();

	//possibleInterpolatingAndFrameskipPairs.push_back(TestType{ false, 2 }); //  60.0 FPS, with no interpolation
	//possibleInterpolatingAndFrameskipPairs.push_back(TestType{ false, 4 }); //  30.0 FPS, with no interpolation
	possibleInterpolatingAndFrameskipPairs.push_back(TestType{ false, 8 }); //  15.0 FPS, with no interpolation
	//possibleInterpolatingAndFrameskipPairs.push_back(TestType{ false,16 }); //   7.5 FPS, with no interpolation
	hardestInterpolatingAndFrameskipPair = possibleInterpolatingAndFrameskipPairs.back();

	//possibleInterpolatingAndFrameskipPairs.push_back(TestType{ true, 2 }); //  60.0 FPS, with interpolation
	//possibleInterpolatingAndFrameskipPairs.push_back(TestType{ true, 4 }); //  30.0 FPS, with interpolation
	possibleInterpolatingAndFrameskipPairs.push_back(TestType{ true, 8 }); //  15.0 FPS, with interpolation
	//possibleInterpolatingAndFrameskipPairs.push_back(TestType{  true,16 }); //   7.5 FPS, with interpolation

	reset(reportFileName);
}

GameModule::~GameModule()
{
	if (file.is_open())
	{
		file.close();
	}
}

void GameModule::reset(std::string reportFileName)
{
	if (file.is_open())
	{
		file.close();
	}

	this->reportFileName = reportFileName;

	file.open(reportFileName, std::ios::out | std::ios::app);

	if (file.is_open())
	{
		file << "stage";
		file << ";" << "shot";
		file << ";" << "difficulty";
		file << ";" << "quickEvaluationWindow";
		file << ";" << "time limit";
		file << ";" << "result";
		file << ";" << "timestamp";
		file << ";" << "shot taken";
		file << ";" << "shot registered";
		file << ";" << "practice stage";
		file << ";" << "using interpolation";
		file << ";" << "frame repeat/interpolation ratio";
		file << std::endl;
	}

	tutorial = true;
	practiceShotsTaken = 0;
	shotPending = false;
	difficulty = 0;
	practice = true;
	quickEvaluationWindow = PRACTICE_INITIAL_QUICK_EVALUATION_WINDOW;
	practiceShots.clear();
	stages.clear();
	//startNextStage();
}

Shot GameModule::beginShot()
{
	assert(!shotPending);

	shotPending = true;

	Shot shot;

	if (tutorial)
	{
		size_t index = tutorialShotsTaken % possibleInterpolatingAndFrameskipPairs.size();

		shot.shotType = Shot::ShotType::TUTORIAL;

		TestType tutorialTestType = possibleInterpolatingAndFrameskipPairs[index];
		shot.interpolating = tutorialTestType.interpolating;
		shot.frameSkip = tutorialTestType.frameskip;

		shot.difficulty = difficulty;
		shot.timeOut = TIMEOUT_DURATION;

		shot.targetInitialX = (double)rand() / RAND_MAX;
		shot.targetInitialY = (double)rand() / RAND_MAX;
		shot.targetDirection = (360.0 * rand()) / (RAND_MAX + 1);

		shot.targetVelocity = getTargetVelocityForDifficulty(shot.difficulty);
	}
	else if (practice)
	{
		size_t index = practiceShotsTaken % possibleInterpolatingAndFrameskipPairs.size();

		TestType practiceTestType = possibleInterpolatingAndFrameskipPairs[index];
		
		if (practiceTestType == easiestInterpolatingAndFrameskipPair)
		{
			shot.shotType = Shot::ShotType::EASY_PRACTICE;
		}
		else if (practiceTestType == hardestInterpolatingAndFrameskipPair)
		{
			shot.shotType = Shot::ShotType::HARD_PRACTICE;
		}
		else
		{
			shot.shotType = Shot::ShotType::OTHER_PRACTICE;
		}

		shot.interpolating = practiceTestType.interpolating;
		shot.frameSkip = practiceTestType.frameskip;

		shot.difficulty = difficulty;
		shot.timeOut = TIMEOUT_DURATION;

		shot.targetInitialX = (double)rand() / RAND_MAX;
		shot.targetInitialY = (double)rand() / RAND_MAX;
		shot.targetDirection = (360.0 * rand()) / (RAND_MAX + 1);

		shot.targetVelocity = getTargetVelocityForDifficulty(shot.difficulty);
	}
	else
	{
		assert(!stages.empty());
		Stage& stage = stages.back();

		assert(!stage.remainingShots.empty());

		shot = stage.remainingShots.front();
		stage.remainingShots.pop_front();
	}

	return shot;
}

void GameModule::registerShot(Shot shot)
{
	assert(shotPending);

	shotPending = false;

	if (shot.shotType == Shot::ShotType::TUTORIAL)
	{
		tutorialShotsTaken++;

		std::cout << "tutorial shot: " << tutorialShotsTaken << std::endl;
	}
	else if (shot.shotType == Shot::ShotType::EASY_PRACTICE || shot.shotType == Shot::ShotType::HARD_PRACTICE || shot.shotType == Shot::ShotType::OTHER_PRACTICE)
	{
		practiceShots.push_back(shot);

		practiceShotsTaken++;

		while (practiceShots.size() > PRACTICE_FULL_EVALUATION_WINDOW * possibleInterpolatingAndFrameskipPairs.size())
		{
			practiceShots.pop_front();
		}

		reportShot(0, practiceShotsTaken, shot.difficulty, quickEvaluationWindow, shot.timeOut, shot.status, shot.aimingStarted, shot.shotTaken, shot.shotRegistered, true, shot.interpolating, shot.frameSkip);
		
		if (practiceShotsTaken % possibleInterpolatingAndFrameskipPairs.size() == 0)
		{
			assert(practiceShots.size() >= possibleInterpolatingAndFrameskipPairs.size());

			Shot& first = *(practiceShots.end() - 3);
			Shot& beforeLast = *(practiceShots.end() - 2);
			Shot& last = *(practiceShots.end() - 1);

			std::cout << (practiceShotsTaken / possibleInterpolatingAndFrameskipPairs.size() - 1) << "\t" << (first.status == Shot::Status::HIT ? 1 : 0) << "\t" << (beforeLast.status == Shot::Status::HIT ? 1 : 0) << "\t" << (last.status == Shot::Status::HIT ? 1 : 0) << "\t" << last.difficulty;

			if (practiceShots.size() >= quickEvaluationWindow * possibleInterpolatingAndFrameskipPairs.size())
			{
				double evaluation = practiceEvaluation(quickEvaluationWindow);

				std::cout << "\t" << evaluation << std::endl;

				if (evaluation > 0.0)
				{
					// evaluation passed => increase difficulty

					practiceShots.clear();

					difficulty++;

					// shrink the window because user was doing well... but only up to a minimun to guard agains accidental success at harder difficulty
					if (quickEvaluationWindow > PRACTICE_INITIAL_QUICK_EVALUATION_WINDOW * 2 * 2)
					{
						quickEvaluationWindow = PRACTICE_INITIAL_QUICK_EVALUATION_WINDOW * 2 * 2; // shrink the window to 2^2
						std::cout << "\t\t\t\t\tshrinking window to " << quickEvaluationWindow << std::endl;
					}

					std::cout << "\t\t\t\t\tincreasing difficulty to " << difficulty << std::endl;
				}
				
				bool practiceTimeout = practiceTimer.peek() > 12 * 60; // 12 min timeout

				// if practice took more than 
				if (practiceTimeout || (evaluation <= 0.0 && quickEvaluationWindow == PRACTICE_FULL_EVALUATION_WINDOW))
				{
					if (practiceTimeout)
					{
						std::cout << "\t\t\t\t\tpractice timeout" << std::endl;
					}

					practice = false;
					difficulty--;

					std::cout << "\t\t\t\t\tending practice at difficulty " << difficulty << std::endl;
					startNextStage();
				}

				if (quickEvaluationWindow < PRACTICE_FULL_EVALUATION_WINDOW)
				{
					// quick searching user level

					if (evaluation <= 0.0)
					{
						// user is not doing very well, grow window to slow optimum difficulty search
						quickEvaluationWindow *= 2;
						std::cout << "\t\t\t\t\tgrowing window to " << quickEvaluationWindow << std::endl;
					}
					/* else
					{
					// user doing very well, continue quick searching
					} */
				}
				/* else
				{
				// evaluation window is already full size
				} */
			}
			else
			{
				// not enough data
				std::cout << "\t" << std::endl;
			}
		}
	}
	else // if (shot.shotType == Shot::ShotType::TEST)
	{
		assert(!stages.empty());
		Stage& stage = stages.back();

		stage.takenShots.push_back(shot);

		reportShot((int)stages.size(), (int)stage.takenShots.size(), shot.difficulty, 0, shot.timeOut, shot.status, shot.aimingStarted, shot.shotTaken, shot.shotRegistered, false, shot.interpolating, shot.frameSkip);

		if (stages.back().remainingShots.empty() && !isGameEnded())
		{
			startNextStage();
		}
	}
}

void GameModule::reportShot(int stage, int shot, double difficulty, int quickEvaluationWindowSize, double timeOut, Shot::Status status, double aimingStarted, double shotTaken, double shotRegistered, bool practice, bool interpolating, int frameSkip)
{
	if (file.is_open())
	{
		file << stage;
		file << ";" << shot;
		file << ";" << difficulty;
		file << ";" << quickEvaluationWindowSize;
		file << ";" << timeOut;
		if (status == Shot::Status::PENDING)
		{
			file << ";" << "PENDING";
		}
		else if (status == Shot::Status::HIT)
		{
			file << ";" << "HIT";
		}
		else if (status == Shot::Status::MISS)
		{
			file << ";" << "MISS";
		}
		else if (status == Shot::Status::TIME_OUT)
		{
			file << ";" << "TIMEOUT";
		}
		file << ";" << aimingStarted;
		file << ";" << shotTaken;
		file << ";" << shotRegistered;
		file << ";" << practice;
		file << ";" << interpolating;
		file << ";" << frameSkip;
		file << std::endl;
	}
}

double GameModule::getInterludeDuration()
{
	return INTERLUDE_DURATION;
}

double GameModule::getTargetVelocityForDifficulty(double difficulty)
{
	// nice divisors for 30: 1 2 3 5 6 10 15 30
	// e.g.  6: velocity multiplier range [1, 32[
	// e.g. 10: velocity multiplier range [1, 8[

	return 0.25 * pow(2, difficulty / 6.0);
}

bool GameModule::isGameEnded()
{
	//std::cout << stages.size() << " == " << STAGES << " && " << stages.back().remainingShots.size() << " == 0" << std::endl;

	return stages.size() == STAGES && stages.back().remainingShots.size() == 0;
}

// ] 0, 1] => easy
//     0   => balanced
// [-1, 0[ => hard
double GameModule::practiceEvaluation(int historyLookBackDepth)
{
	assert(practiceShots.size() >= historyLookBackDepth * possibleInterpolatingAndFrameskipPairs.size());

	std::vector<Shot> hitCheckShots(practiceShots.end() - (historyLookBackDepth * possibleInterpolatingAndFrameskipPairs.size()), practiceShots.end());

	double easyMissCount = 0;
	double hardHitCount = 0;

	for (Shot practiceShot : hitCheckShots)
	{
		if (practiceShot.shotType == Shot::ShotType::EASY_PRACTICE && practiceShot.status != Shot::Status::HIT)
		{
			easyMissCount += 1;
		}
		else if (practiceShot.shotType == Shot::ShotType::HARD_PRACTICE && practiceShot.status == Shot::Status::HIT)
		{
			hardHitCount += 1;
		}
	}

	easyMissCount /= historyLookBackDepth;
	hardHitCount /= historyLookBackDepth;

	return hardHitCount - easyMissCount;
}

void GameModule::startNextStage()
{
	Stage stage;

	stage.difficulty = difficulty;

	// if user did well: MAKE IT HARDER!
	for (TestType possibleInterpolatingAndFrameskipPair : possibleInterpolatingAndFrameskipPairs)
	{
		for (int i = 0; i < SHOTS_PER_TEST_PER_STAGE; i++)
		{
			Shot shot;

			shot.shotType = Shot::ShotType::TEST;

			shot.interpolating = possibleInterpolatingAndFrameskipPair.interpolating;
			shot.frameSkip = possibleInterpolatingAndFrameskipPair.frameskip;

			shot.difficulty = stage.difficulty;
			shot.timeOut = TIMEOUT_DURATION;

			shot.targetInitialX = (double)rand() / RAND_MAX;
			shot.targetInitialY = (double)rand() / RAND_MAX;
			shot.targetDirection = (360.0 * rand()) / (RAND_MAX + 1);

			shot.targetVelocity = getTargetVelocityForDifficulty(shot.difficulty);

			stage.remainingShots.push_back(shot);
		}
	}

	std::random_shuffle(stage.remainingShots.begin(), stage.remainingShots.end());

	stages.push_back(stage);

	std::cout << "\t\t\t\t\tstarting stage " << stages.size() << " with " << stage.remainingShots.size() << " shots" << std::endl;
}

bool GameModule::isPractice()
{
	return practice;
}

bool GameModule::isTutorial()
{
	return tutorial;
}

void GameModule::endTutorial()
{
	tutorial = false;
	practiceTimer.restart();
}
