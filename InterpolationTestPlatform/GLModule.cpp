#include "GLModule.h"

#include <gl\glew.h>
#include <stdio.h>
#include <sstream>
#include <iostream>

#define GLM_SWIZZLE
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/matrix_access.hpp>
#include <glm/gtc/type_ptr.hpp>

const UtilColor GLModule::hudColor{ 0x00, 0x00, 0x00, 0xFF };
const UtilColor GLModule::successColor{ 0x00, 0xFF, 0x00, 0xFF };
const UtilColor GLModule::failColor{ 0xFF, 0x00, 0x00, 0xFF };

GLModule::GLModule(PhysicsModule* physicsModule, std::vector<DrawableObject*>& drawableObjects, int screenWidth, int screenHeight) : physicsModule(physicsModule), drawableObjects(drawableObjects), screenWidth(screenWidth), screenHeight(screenHeight)
{
	lastCameraXRotationAngle = physicsModule->cameraX_rotation_angle;

	initLights();

	initOpenGLBasics();

	initRenderShader();
	initWarpShader();
	initGuiShader();

	initFbo();

	initProjectionMatrix();

	initDrawableObjects();
	initWarpGrid();
	initGui();

	setupLights();
}

void GLModule::initOpenGLBasics()
{
	// tell GL to only draw onto a pixel if the shape is closer to the viewer
	glClearColor(0.0, 0.0, 0.0, 1.0);
	glEnable(GL_DEPTH_TEST); // enable depth-testing
	glDepthFunc(GL_LESS); // depth-testing interprets a smaller value as "closer"

	glDisable(GL_CULL_FACE); // we can do this in the shader if we want to
	glFrontFace(GL_CCW); // GL_CCW for counter clock-wise

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}

void GLModule::resizeWindow(int screenWidth, int screenHeight)
{
	glDeleteVertexArrays(1, &guiVertexArrayObject);
	glDeleteBuffers(1, &guiPositionsVertexBufferObject);
	glDeleteBuffers(1, &guiIndexBufferObject);
	glDeleteTextures(1, &guiTextureObject);
	glDeleteTextures(1, &hitTextureObject);
	glDeleteTextures(1, &missTextureObject);
	glDeleteTextures(1, &timeoutTextureObject);
	glDeleteTextures(1, &crosshairTextureObject);

	glDeleteVertexArrays(1, &warpVertexArrayObject);
	glDeleteBuffers(1, &warpGridPositionsVertexBufferObject);
	glDeleteBuffers(1, &warpIndexBufferObject);

	glDeleteFramebuffers(1, &offscreenFramebufferObject);
	glDeleteTextures(1, &offscreenColorTextureObject);
	glDeleteTextures(1, &offscreenVelocityTextureObject);
	glDeleteTextures(1, &offscreenHitTextureObject);
	glDeleteTextures(1, &offscreenDepthTextureObject);

	this->screenWidth = screenWidth;
	this->screenHeight = screenHeight;

	initFbo();

	initProjectionMatrix();

	initWarpGrid();
	initGui();

	glViewport(0, 0, screenWidth, screenHeight);
}

glm::dvec3 GLModule::fromDevicePositionToWorldPosition(glm::dvec3 devicePosition)
{
	glm::dvec3 normalizedDevicePosition = devicePosition;
	
	normalizedDevicePosition.x *= 2.0;
	normalizedDevicePosition.x /= screenWidth;
	normalizedDevicePosition.x -= 1.0;

	normalizedDevicePosition.y *= 2.0;
	normalizedDevicePosition.y /= screenHeight;
	normalizedDevicePosition.y -= 1.0;

	glm::mat4 fromWorldToViewMatrix = calculateFromWorldToViewMatrix();
	glm::mat4 fromViewToWorldMatrix = glm::inverse(fromWorldToViewMatrix);

	glm::dvec4 hWorldPosition = fromViewToWorldMatrix * fromProjectionToViewMatrix * glm::dvec4(normalizedDevicePosition, 1.0);
	glm::dvec3 worldPosition = hWorldPosition.xyz() / hWorldPosition.w;

	return worldPosition;
}

GLuint GLModule::createShaderFromFile(std::string file, GLenum type)
{
	GLint status;

	const char* buffer = UtilModule::readFile(file.c_str());

	std::string str(buffer);
	delete buffer;
	std::string oldStr("%NUM_LIGHTS%");
	std::string newStr = std::to_string(this->lights.size());

	for (size_t pos = str.find(oldStr, 0); pos != std::string::npos; pos = str.find(oldStr, pos))
	{
		str.replace(pos, oldStr.length(), newStr);
		pos += newStr.length();
	}
	/*
	size_t pos = 0;
	while ((pos = str.find(oldStr, pos)) != std::string::npos)
	{
		str.replace(pos, oldStr.length(), newStr);
		pos += newStr.length();
	}*/
	buffer = str.c_str();

	GLuint shader = glCreateShader(type);
	glShaderSource(shader, 1, &buffer, NULL);
	glCompileShader(shader);
	printShaderLog(shader);

	//error check
	glGetShaderiv(shader, GL_COMPILE_STATUS, &status);
	if (status == GL_FALSE)
	{
		return  -1;
	}
	else
	{
		return shader;
	}
}

void GLModule::initRenderShader()
{
	// create shaders
	renderVertexShader = createShaderFromFile("test_vs.glsl", GL_VERTEX_SHADER);
	renderFragmentShader = createShaderFromFile("test_fs.glsl", GL_FRAGMENT_SHADER);

	// create program
	renderShaderProgram = glCreateProgram();

	// attach shaders to program
	glAttachShader(renderShaderProgram, renderVertexShader);
	glAttachShader(renderShaderProgram, renderFragmentShader);

	// link program
	glLinkProgram(renderShaderProgram);
	printProgramLog(renderShaderProgram);

	// get attribute locations
	attributeLocations.position = glGetAttribLocation(renderShaderProgram, "vertexPositionAttribute");
	attributeLocations.texcoord = glGetAttribLocation(renderShaderProgram, "vertexTexcoordAttribute");
	attributeLocations.texcoordScale = glGetAttribLocation(renderShaderProgram, "vertexTexcoordScaleAttribute");
	attributeLocations.normal = glGetAttribLocation(renderShaderProgram, "vertexNormalAttribute");

	attributeLocations.emission = glGetAttribLocation(renderShaderProgram, "materialEmissionAttribute");
	attributeLocations.ambient = glGetAttribLocation(renderShaderProgram, "materialAmbientAttribute");
	attributeLocations.diffuse = glGetAttribLocation(renderShaderProgram, "materialDiffuseAttribute");
	attributeLocations.specular = glGetAttribLocation(renderShaderProgram, "materialSpecularAttribute");
	attributeLocations.shininess = glGetAttribLocation(renderShaderProgram, "materialShininessAttribute");

	// get uniform locations
	uniformLocations.diffuseTexture = glGetUniformLocation(renderShaderProgram, "textures.diffuse");
	uniformLocations.skyTexture = glGetUniformLocation(renderShaderProgram, "textures.sky");
	uniformLocations.velocity = glGetUniformLocation(renderShaderProgram, "velocity");
	uniformLocations.fromModelToViewMatrix = glGetUniformLocation(renderShaderProgram, "transformations.fromModelToView");
	uniformLocations.fromModelToViewNormalMatrix = glGetUniformLocation(renderShaderProgram, "transformations.fromModelToViewNormal");
	uniformLocations.fromViewToProjectionMatrix = glGetUniformLocation(renderShaderProgram, "transformations.fromViewToProjection");
	uniformLocations.hitId = glGetUniformLocation(renderShaderProgram, "hitId");
}

void GLModule::initWarpShader()
{
	// create shaders
	warpVertexShader = createShaderFromFile("warp_vs.glsl", GL_VERTEX_SHADER);
	warpTessControlShader = createShaderFromFile("warp_cs.glsl", GL_TESS_CONTROL_SHADER);
	warpTessEvaluationShader = createShaderFromFile("warp_es.glsl", GL_TESS_EVALUATION_SHADER);
	warpGeometryShader = createShaderFromFile("warp_gs.glsl", GL_GEOMETRY_SHADER);
	warpFragmentShader = createShaderFromFile("warp_fs.glsl", GL_FRAGMENT_SHADER);

	// create program
	warpShaderProgram = glCreateProgram();

	// attach shaders to program
	glAttachShader(warpShaderProgram, warpVertexShader);
	glAttachShader(warpShaderProgram, warpTessControlShader);
	glAttachShader(warpShaderProgram, warpTessEvaluationShader);
	glAttachShader(warpShaderProgram, warpGeometryShader);
	glAttachShader(warpShaderProgram, warpFragmentShader);

	// link program
	glLinkProgram(warpShaderProgram);
	printProgramLog(warpShaderProgram);

	// get attribute locations
	warpPositionAttributeLocation = glGetAttribLocation(warpShaderProgram, "vertexPositionAttribute");

	// get uniform locations
	warpCachedColorTextureUniformLocation = glGetUniformLocation(warpShaderProgram, "cachedColor");
	warpCachedVelocityTextureUniformLocation = glGetUniformLocation(warpShaderProgram, "cachedVelocity");
	warpCachedDepthTextureUniformLocation = glGetUniformLocation(warpShaderProgram, "cachedDepth");
	warpFromProjectionToViewMatrixUniformLocation = glGetUniformLocation(warpShaderProgram, "fromProjectionToViewMatrix");
	warpFromLastRenderViewToWorldMatrixUniformLocation = glGetUniformLocation(warpShaderProgram, "fromLastRenderViewToWorldMatrix");
	warpFromWorldToViewMatrixUniformLocation = glGetUniformLocation(warpShaderProgram, "fromWorldToViewMatrix");
	warpFromViewToProjectionMatrixUniformLocation = glGetUniformLocation(warpShaderProgram, "fromViewToProjectionMatrix");
	warpGridSizeUniformLocation = glGetUniformLocation(warpShaderProgram, "gridSize");
	warpFlipGridBorderUniformLocation = glGetUniformLocation(warpShaderProgram, "flipGridBorder");
	warpTimeSinceRealFrameUniformLocation = glGetUniformLocation(warpShaderProgram, "timeSinceRealFrame");
	warpOffsettenUniformLocation = glGetUniformLocation(warpShaderProgram, "offsetten");
}

void GLModule::initGuiShader()
{
	// create shaders
	guiVertexShader = createShaderFromFile("gui_vs.glsl", GL_VERTEX_SHADER);
	guiFragmentShader = createShaderFromFile("gui_fs.glsl", GL_FRAGMENT_SHADER);

	// create program
	guiShaderProgram = glCreateProgram();

	// attach shaders to program
	glAttachShader(guiShaderProgram, guiVertexShader);
	glAttachShader(guiShaderProgram, guiFragmentShader);

	// link program
	glLinkProgram(guiShaderProgram);
	printProgramLog(guiShaderProgram);

	// get attribute locations
	guiAttributeLocations.position = glGetAttribLocation(guiShaderProgram, "vertexPositionAttribute");

	// get uniform locations
	guiUniformLocations.guiTexture = glGetUniformLocation(guiShaderProgram, "textures.gui");
	guiUniformLocations.textOffset = glGetUniformLocation(guiShaderProgram, "textOffset");
	guiUniformLocations.textScale = glGetUniformLocation(guiShaderProgram, "textScale");
}

void GLModule::initFbo()
{
	// create a color texture object
	glGenTextures(1, &offscreenColorTextureObject);
	glBindTexture(GL_TEXTURE_2D, offscreenColorTextureObject);
	{
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_GENERATE_MIPMAP, GL_FALSE);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, screenWidth, screenHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);
	}
	glBindTexture(GL_TEXTURE_2D, 0);

	// create a velocity texture object
	glGenTextures(1, &offscreenVelocityTextureObject);
	glBindTexture(GL_TEXTURE_2D, offscreenVelocityTextureObject);
	{
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_GENERATE_MIPMAP, GL_FALSE);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB32F, screenWidth, screenHeight, 0, GL_RGB, GL_FLOAT, 0);
	}
	glBindTexture(GL_TEXTURE_2D, 0);

	// create a hit texture object
	glGenTextures(1, &offscreenHitTextureObject);
	glBindTexture(GL_TEXTURE_2D, offscreenHitTextureObject);
	{
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_GENERATE_MIPMAP, GL_FALSE);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_R8UI, screenWidth, screenHeight, 0, GL_RED_INTEGER, GL_UNSIGNED_BYTE, 0);
	}
	glBindTexture(GL_TEXTURE_2D, 0);

	// create a depth texture object
	glGenTextures(1, &offscreenDepthTextureObject);
	glBindTexture(GL_TEXTURE_2D, offscreenDepthTextureObject);
	{
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_GENERATE_MIPMAP, GL_FALSE);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT32F, screenWidth, screenHeight, 0, GL_DEPTH_COMPONENT, GL_FLOAT, 0);
	}
	glBindTexture(GL_TEXTURE_2D, 0);

	// create a framebuffer object
	glGenFramebuffers(1, &offscreenFramebufferObject);
	glBindFramebuffer(GL_FRAMEBUFFER, offscreenFramebufferObject);
	{
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, offscreenColorTextureObject, 0);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_2D, offscreenVelocityTextureObject, 0);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT2, GL_TEXTURE_2D, offscreenHitTextureObject, 0);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, offscreenDepthTextureObject, 0);

		GLenum statusEnum = glCheckFramebufferStatus(GL_FRAMEBUFFER);
		if (statusEnum == GL_FRAMEBUFFER_COMPLETE)
		{
			//std::cout << "Framebuffer complete." << std::endl;
		}
	}
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void GLModule::initProjectionMatrix()
{
	fromViewToProjectionMatrix = calculateFromViewToProjectionMatrix();
	fromProjectionToViewMatrix = glm::inverse(fromViewToProjectionMatrix);
}

void GLModule::initDrawableObjects()
{
	for (DrawableObject* drawableObject : drawableObjects)
	{
		drawableObject->initDraw(attributeLocations);
	}
}

void GLModule::initWarpGrid()
{
	// valid values for 1920x1080 & 1920x1200 and rough empirical FPS on Radeon HD 7950 with lego ppl scene:
	//  1:  35.0
	//  2:  79.0
	//  3: 105.0
	//  4: 121.5
	//  5: 129.0
	//  6: 135.0
	//  8: 142.0
	// 10: 137.5
	// 12: 138.5
	// 15: 121.0
	// 20: 121.5
	// 24: 123.0
	// 30: 105.0
	// 40:  80.0
	// 60:  54.0

	warpGridWidth = screenWidth / 8;
	warpGridHeight = screenHeight / 8;

	GLsizei warpGridPositionsLength = (warpGridWidth + 1) * (warpGridHeight + 1) * 2;
	GLuint* warpGridPositions = new GLuint[warpGridPositionsLength];

	for (size_t ix = 0; ix < warpGridWidth + 1; ix++)
	{
		for (size_t iy = 0; iy < warpGridHeight + 1; iy++)
		{
			size_t i = (ix * (warpGridHeight + 1) + iy) * 2;

			warpGridPositions[i + 0] = (GLuint)ix; // x
			warpGridPositions[i + 1] = (GLuint)iy; // y
		}
	}

	warpIndexCount = (warpGridWidth) * (warpGridHeight) * 4;
	GLuint* warpGridIndices = new GLuint[warpIndexCount];

	for (GLuint ix = 0; ix < (GLuint)warpGridWidth; ix++)
	{
		for (GLuint iy = 0; iy < (GLuint)warpGridHeight; iy++)
		{
			GLuint i = (ix * warpGridHeight + iy) * 4;

			GLuint leftBottomVertexIndex = ((ix + 0) * (warpGridHeight + 1) + (iy + 0));
			GLuint leftTopVertexIndex = ((ix + 0) * (warpGridHeight + 1) + (iy + 1));
			GLuint rightBottomVertexIndex = ((ix + 1) * (warpGridHeight + 1) + (iy + 0));
			GLuint rightTopVertexIndex = ((ix + 1) * (warpGridHeight + 1) + (iy + 1));

			warpGridIndices[i + 0] = leftBottomVertexIndex;
			warpGridIndices[i + 1] = rightBottomVertexIndex;
			warpGridIndices[i + 2] = rightTopVertexIndex;
			warpGridIndices[i + 3] = leftTopVertexIndex;
		}
	}

	glGenVertexArrays(1, &warpVertexArrayObject);
	glBindVertexArray(warpVertexArrayObject);
	{
		glGenBuffers(1, &warpGridPositionsVertexBufferObject);
		glBindBuffer(GL_ARRAY_BUFFER, warpGridPositionsVertexBufferObject);
		{
			glBufferData(GL_ARRAY_BUFFER, warpGridPositionsLength * sizeof(GLuint), warpGridPositions, GL_STATIC_DRAW);
			delete warpGridPositions;
			glEnableVertexAttribArray(warpPositionAttributeLocation);
			glVertexAttribIPointer(warpPositionAttributeLocation, 2, GL_UNSIGNED_INT, 0, NULL);
		}
		glBindBuffer(GL_ARRAY_BUFFER, 0);

		glGenBuffers(1, &warpIndexBufferObject);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, warpIndexBufferObject);
		{
			glBufferData(GL_ELEMENT_ARRAY_BUFFER, warpIndexCount * sizeof(GLuint), warpGridIndices, GL_STATIC_DRAW);
			delete warpGridIndices;
			glPatchParameteri(GL_PATCH_VERTICES, 4);
		}
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	}
	glBindVertexArray(0);
}

void GLModule::initGui()
{
	uint32_t* textPixels = UtilModule::makeText("jope\nadfgdfg", hudColor, textWidth, textHeight, (int)(0.015f * screenHeight + 0.5));
	glGenTextures(1, &guiTextureObject);
	glBindTexture(GL_TEXTURE_2D, guiTextureObject);
	{
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_GENERATE_MIPMAP, GL_FALSE);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, textWidth, textHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, textPixels);
	}
	glBindTexture(GL_TEXTURE_2D, 0);
	delete textPixels;
	textPixels = NULL;


	uint32_t* hitPixels = UtilModule::makeText("HIT", successColor, hitWidth, hitHeight, (int)(0.075f * screenHeight + 0.5));
	glGenTextures(1, &hitTextureObject);
	glBindTexture(GL_TEXTURE_2D, hitTextureObject);
	{
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_GENERATE_MIPMAP, GL_FALSE);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, hitWidth, hitHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, hitPixels);
	}
	glBindTexture(GL_TEXTURE_2D, 0);
	delete hitPixels;
	hitPixels = NULL;


	uint32_t* missPixels = UtilModule::makeText("MISS", failColor, missWidth, missHeight, (int)(0.075f * screenHeight + 0.5));
	glGenTextures(1, &missTextureObject);
	glBindTexture(GL_TEXTURE_2D, missTextureObject);
	{
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_GENERATE_MIPMAP, GL_FALSE);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, missWidth, missHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, missPixels);
	}
	glBindTexture(GL_TEXTURE_2D, 0);
	delete missPixels;
	missPixels = NULL;


	uint32_t* timeoutPixels = UtilModule::makeText("TIMEOUT", failColor, timeoutWidth, timeoutHeight, (int)(0.075f * screenHeight + 0.5));
	glGenTextures(1, &timeoutTextureObject);
	glBindTexture(GL_TEXTURE_2D, timeoutTextureObject);
	{
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_GENERATE_MIPMAP, GL_FALSE);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, timeoutWidth, timeoutHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, timeoutPixels);
	}
	glBindTexture(GL_TEXTURE_2D, 0);
	delete timeoutPixels;
	timeoutPixels = NULL;


	uint32_t* crosshairPixels = UtilModule::loadImage("images/smallCrosshairBordered.png", crosshairWidth, crosshairHeight);
	glGenTextures(1, &crosshairTextureObject);
	glBindTexture(GL_TEXTURE_2D, crosshairTextureObject);
	{
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_GENERATE_MIPMAP, GL_FALSE);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, crosshairWidth, crosshairHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, crosshairPixels);
	}
	glBindTexture(GL_TEXTURE_2D, 0);
	delete crosshairPixels;
	crosshairPixels = NULL;


	const GLshort guiIndices[] =
	{
		0, 1, 2,
		3, 2, 1,
	};

	guiIndexCount = sizeof(guiIndices) / sizeof(GLshort);

	const float guiPositions[] =
	{
		1.0f, 1.0f,
		0.0f, 1.0f,
		1.0f, 0.0f,
		0.0f, 0.0f,
	};

	glGenVertexArrays(1, &guiVertexArrayObject);
	glBindVertexArray(guiVertexArrayObject);
	{
		glGenBuffers(1, &guiIndexBufferObject);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, guiIndexBufferObject);
		{
			glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(guiIndices), guiIndices, GL_STATIC_DRAW);
		}
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);



		glGenBuffers(1, &guiPositionsVertexBufferObject);
		glBindBuffer(GL_ARRAY_BUFFER, guiPositionsVertexBufferObject);
		{
			glBufferData(GL_ARRAY_BUFFER, sizeof(guiPositions), guiPositions, GL_STATIC_DRAW);
			glEnableVertexAttribArray(guiAttributeLocations.position);
			glVertexAttribPointer(guiAttributeLocations.position, 2, GL_FLOAT, GL_FALSE, 0, NULL);
		}
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}
	glBindVertexArray(0);
}

void GLModule::updateHud(std::string text)
{
	uint32_t* textPixels = UtilModule::makeText(text, hudColor, textWidth, textHeight, (int)(0.015f * screenHeight + 0.5));

	glBindTexture(GL_TEXTURE_2D, guiTextureObject);
	{
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, textWidth, textHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, textPixels);
		//glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, textWidth, textHeight, GL_RGBA, GL_UNSIGNED_BYTE, textPixels);
	}
	glBindTexture(GL_TEXTURE_2D, 0);

	delete textPixels;
}

void GLModule::initLights()
{
	lights.clear();

	// ambient light

	float ambient = 0.05f;

	{
		Light light;
		light.ambient = glm::vec3(ambient, ambient, ambient);
		lights.push_back(light);
	}

	// positional lights

	float diffuse = 15.0f;
	float specular = 0.0f;

	{
		// ceiling 1
		Light light;
		light.position = glm::vec3(0.0f, +5.4f, +22.0f);
		light.diffuse = glm::vec3(diffuse, diffuse, diffuse);
		light.specular = glm::vec3(specular, specular, specular);
		lights.push_back(light);
	}
	{
		// ceiling 1
		Light light;
		light.position = glm::vec3(0.0f, +5.4f, +11.0f);
		light.diffuse = glm::vec3(diffuse, diffuse, diffuse);
		light.specular = glm::vec3(specular, specular, specular);
		lights.push_back(light);
	}
	{
		// ceiling 2
		Light light;
		light.position = glm::vec3(0.0f, +5.4f, 0.0f);
		light.diffuse = glm::vec3(diffuse, diffuse, diffuse);
		light.specular = glm::vec3(specular, specular, specular);
		lights.push_back(light);
	}
	{
		// ceiling 3
		Light light;
		light.position = glm::vec3(0.0f, +5.4f, -11.0f);
		light.diffuse = glm::vec3(diffuse, diffuse, diffuse);
		light.specular = glm::vec3(specular, specular, specular);
		lights.push_back(light);
	}
	{
		// ceiling 3
		Light light;
		light.position = glm::vec3(0.0f, +5.4f, -22.0f);
		light.diffuse = glm::vec3(diffuse, diffuse, diffuse);
		light.specular = glm::vec3(specular, specular, specular);
		lights.push_back(light);
	}
}

GLModule::~GLModule()
{
	glDeleteProgram(renderShaderProgram);
	glDeleteShader(renderVertexShader);
	glDeleteShader(renderFragmentShader);

	glDeleteProgram(warpShaderProgram);
	glDeleteShader(warpVertexShader);
	glDeleteShader(warpTessControlShader);
	glDeleteShader(warpTessEvaluationShader);
	glDeleteShader(warpGeometryShader);
	glDeleteShader(warpFragmentShader);

	glDeleteProgram(guiShaderProgram);
	glDeleteShader(guiVertexShader);
	glDeleteShader(guiFragmentShader);


	glDeleteFramebuffers(1, &offscreenFramebufferObject);
	glDeleteTextures(1, &offscreenColorTextureObject);
	glDeleteTextures(1, &offscreenVelocityTextureObject);
	glDeleteTextures(1, &offscreenHitTextureObject);
	glDeleteTextures(1, &offscreenDepthTextureObject);


	glDeleteVertexArrays(1, &guiVertexArrayObject);
	glDeleteBuffers(1, &guiPositionsVertexBufferObject);
	glDeleteBuffers(1, &guiIndexBufferObject);
	glDeleteTextures(1, &guiTextureObject);
	glDeleteTextures(1, &hitTextureObject);
	glDeleteTextures(1, &missTextureObject);
	glDeleteTextures(1, &timeoutTextureObject);
	glDeleteTextures(1, &crosshairTextureObject);

	glDeleteVertexArrays(1, &warpVertexArrayObject);
	glDeleteBuffers(1, &warpGridPositionsVertexBufferObject);
	glDeleteBuffers(1, &warpIndexBufferObject);
}

void GLModule::renderScene()
{
	lastCameraXRotationAngle = physicsModule->cameraX_rotation_angle;
	fromWorldToLastRenderViewMatrix = calculateFromWorldToViewMatrix();
	fromLastRenderViewToWorldMatrixStale = true; // the inverse matrix needs to be recalculated

	// set the rendering destination to FBO
	glBindFramebuffer(GL_FRAMEBUFFER, offscreenFramebufferObject);
	{
		// wipe the drawing surface clear
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		GLenum bufs[] = { GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1, GL_COLOR_ATTACHMENT2 };
		glDrawBuffers(3, bufs);

		glUseProgram(renderShaderProgram);
		{
			// update lighting
			updateLights();

			// set projection matrix
			glUniformMatrix4fv(uniformLocations.fromViewToProjectionMatrix, 1, GL_FALSE, glm::value_ptr(fromViewToProjectionMatrix));

			// draw objects
			for (DrawableObject* drawableObject : drawableObjects)
			{
				drawableObject->draw(uniformLocations, fromWorldToLastRenderViewMatrix);
			}
		}
		glUseProgram(0);
	}
	glBindFramebuffer(GL_FRAMEBUFFER, 0); // back to normal window-system-provided framebuffer
}

void GLModule::readHit()
{
	glBindFramebuffer(GL_READ_FRAMEBUFFER, offscreenFramebufferObject);
	{
		glReadBuffer(GL_COLOR_ATTACHMENT2);

		GLint x = screenWidth / 2;
		GLint y = screenHeight / 2;
		GLsizei width = 1;
		GLsizei height = 1;
		GLenum format = GL_RED_INTEGER;
		GLenum type = GL_UNSIGNED_BYTE;
		GLsizei bufSize = sizeof(hitId);
		void *data = &hitId;

		glReadPixels(x, y, width, height, format, type, data);
	}
	glBindFramebuffer(GL_READ_FRAMEBUFFER, 0);
}

GLubyte GLModule::getHitId()
{
	return hitId;
}

void GLModule::blitOffscreenToScreen()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glBindFramebuffer(GL_READ_FRAMEBUFFER, offscreenFramebufferObject);
	{
		glReadBuffer(GL_COLOR_ATTACHMENT0);
		GLenum bufs[] = { GL_COLOR_ATTACHMENT0 };
		glDrawBuffers(1, bufs);


		glBlitFramebuffer(0, 0, screenWidth, screenHeight, 0, 0, screenWidth, screenHeight, GL_COLOR_BUFFER_BIT, GL_LINEAR);
	}
	glBindFramebuffer(GL_READ_FRAMEBUFFER, 0);
}

void GLModule::renderHudCrosshair()
{
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	{
		GLenum bufs[] = { GL_COLOR_ATTACHMENT0 };
		glDrawBuffers(1, bufs);

		glUseProgram(guiShaderProgram);
		{
			glBindVertexArray(guiVertexArrayObject);
			{
				glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, guiIndexBufferObject);
				{
					glm::vec2 offset;

					// screen center
					offset.x = screenWidth / 2.0f;
					offset.y = screenHeight / 2.0f;

					// offset by half the crosshair size (rounddown because hit pixel is in the top right quadrant of the screen)
					offset.x -= (int)(crosshairWidth / 2.0);
					offset.y -= (int)(crosshairHeight / 2.0);

					// to range 0.0 ... 1.0
					offset.x /= screenWidth;
					offset.y /= screenHeight;

					glUniform2f(guiUniformLocations.textOffset, offset.x, offset.y);

					glm::vec2 scale;
					scale.x = crosshairWidth / (float)screenWidth;
					scale.y = crosshairHeight / (float)screenHeight;

					glUniform2f(guiUniformLocations.textScale, scale.x, scale.y);

					glUniform1i(guiUniformLocations.guiTexture, 0);

					glActiveTexture(GL_TEXTURE0); glBindTexture(GL_TEXTURE_2D, crosshairTextureObject);
					{
						glDrawElements(GL_TRIANGLES, guiIndexCount, GL_UNSIGNED_SHORT, 0);
					}
					glActiveTexture(GL_TEXTURE0); glBindTexture(GL_TEXTURE_2D, 0);
				}
				glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
			}
			glBindVertexArray(0);
		}
		glUseProgram(0);
	}
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void GLModule::renderHudFeedback(FeedbackType feedbackType)
{
	GLuint feedbackTextureObject;
	int feedbackWidth;
	int feedbackHeight;

	if (feedbackType == FeedbackType::HIT)
	{
		feedbackTextureObject = hitTextureObject;
		feedbackWidth = hitWidth;
		feedbackHeight = hitHeight;
	}
	else if (feedbackType == FeedbackType::MISS)
	{
		feedbackTextureObject = missTextureObject;
		feedbackWidth = missWidth;
		feedbackHeight = missHeight;
	}
	else if (feedbackType == FeedbackType::TIMEOUT)
	{
		feedbackTextureObject = timeoutTextureObject;
		feedbackWidth = timeoutWidth;
		feedbackHeight = timeoutHeight;
	}
	else // if (feedbackType == FeedbackType::INVISIBLE)
	{
		return;
	}

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	{
		GLenum bufs[] = { GL_COLOR_ATTACHMENT0 };
		glDrawBuffers(1, bufs);

		glUseProgram(guiShaderProgram);
		{
			glBindVertexArray(guiVertexArrayObject);
			{
				glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, guiIndexBufferObject);
				{
					glm::vec2 offset;

					// screen top center
					offset.x = screenWidth / 2.0f;
					offset.y = screenHeight * (7.0f / 8.0f);

					// offset by half the crosshair size (rounddown because hit pixel is in the top right quadrant of the screen)
					offset.x -= (int)(feedbackWidth / 2.0);
					offset.y -= (int)(feedbackHeight / 2.0);

					// to range 0.0 ... 1.0
					offset.x /= screenWidth;
					offset.y /= screenHeight;

					glUniform2f(guiUniformLocations.textOffset, offset.x, offset.y);

					glm::vec2 scale;
					scale.x = feedbackWidth / (float)screenWidth;
					scale.y = feedbackHeight / (float)screenHeight;

					glUniform2f(guiUniformLocations.textScale, scale.x, scale.y);

					glUniform1i(guiUniformLocations.guiTexture, 0);

					glActiveTexture(GL_TEXTURE0); glBindTexture(GL_TEXTURE_2D, feedbackTextureObject);
					{
						glDrawElements(GL_TRIANGLES, guiIndexCount, GL_UNSIGNED_SHORT, 0);
					}
					glActiveTexture(GL_TEXTURE0); glBindTexture(GL_TEXTURE_2D, 0);
				}
				glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
			}
			glBindVertexArray(0);
		}
		glUseProgram(0);
	}
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void GLModule::renderHudInfo()
{
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	{
		GLenum bufs[] = { GL_COLOR_ATTACHMENT0 };
		glDrawBuffers(1, bufs);

		glUseProgram(guiShaderProgram);
		{
			glBindVertexArray(guiVertexArrayObject);
			{
				glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, guiIndexBufferObject);
				{
					glUniform2f(guiUniformLocations.textOffset, 20 / (float)screenWidth, 20 / (float)screenHeight);

					glUniform2f(guiUniformLocations.textScale,
						textWidth / (float)screenWidth,
						textHeight / (float)screenHeight
						);

					glUniform1i(guiUniformLocations.guiTexture, 0);

					glActiveTexture(GL_TEXTURE0); glBindTexture(GL_TEXTURE_2D, guiTextureObject);
					{
						glDrawElements(GL_TRIANGLES, guiIndexCount, GL_UNSIGNED_SHORT, 0);
					}
					glActiveTexture(GL_TEXTURE0); glBindTexture(GL_TEXTURE_2D, 0);
				}
				glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
			}
			glBindVertexArray(0);
		}
		glUseProgram(0);
	}
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void GLModule::setupLights()
{
	std::stringstream selectorStringstream;
	std::string selectorString;
	for (int lightIndex = 0; lightIndex < lights.size(); lightIndex++)
	{
		Light& light = lights[lightIndex];

		selectorStringstream.str(std::string());

		selectorStringstream << "lights[" << lightIndex << "].position";
		selectorString = selectorStringstream.str();
		light.positionUniformLocation = glGetUniformLocation(renderShaderProgram, selectorString.c_str());
		glm::vec4 positionH = fromWorldToLastRenderViewMatrix * glm::vec4(light.position, 1.0);
		glm::vec3 position = positionH.xyz() / positionH.w;
		glUniform3f(light.positionUniformLocation, position.x, position.y, position.z);

		selectorStringstream.str(std::string());

		selectorStringstream << "lights[" << lightIndex << "].ambient";
		selectorString = selectorStringstream.str();
		light.ambientUniformLocation = glGetUniformLocation(renderShaderProgram, selectorString.c_str());
		glUniform3f(light.ambientUniformLocation, light.ambient.r, light.ambient.g, light.ambient.b);

		selectorStringstream.str(std::string());

		selectorStringstream << "lights[" << lightIndex << "].diffuse";
		selectorString = selectorStringstream.str();
		light.diffuseUniformLocation = glGetUniformLocation(renderShaderProgram, selectorString.c_str());
		glUniform3f(light.diffuseUniformLocation, light.diffuse.r, light.diffuse.g, light.diffuse.b);

		selectorStringstream.str(std::string());

		selectorStringstream << "lights[" << lightIndex << "].specular";
		selectorString = selectorStringstream.str();
		light.specularUniformLocation = glGetUniformLocation(renderShaderProgram, selectorString.c_str());
		glUniform3f(light.specularUniformLocation, light.specular.r, light.specular.g, light.specular.b);
	}
}

void GLModule::updateLights()
{
	for (int lightIndex = 0; lightIndex < lights.size(); lightIndex++)
	{
		Light light = lights[lightIndex];

		glm::vec4 positionH = fromWorldToLastRenderViewMatrix * glm::vec4(light.position, 1.0);
		glm::vec3 position = positionH.xyz() / positionH.w;
		glUniform3f(light.positionUniformLocation, position.x, position.y, position.z);

		glUniform3f(light.ambientUniformLocation, light.ambient.r, light.ambient.g, light.ambient.b);

		glUniform3f(light.diffuseUniformLocation, light.diffuse.r, light.diffuse.g, light.diffuse.b);

		glUniform3f(light.specularUniformLocation, light.specular.r, light.specular.g, light.specular.b);
	}
}

glm::mat4 GLModule::calculateFromViewToProjectionMatrix()
{
	// Worldcube sides are -500m ... 500m, i.e. 1000 m in length, except y axis is half. Diameter is sqrt(1000^2 + 500^2 + 1000^2)=1500. We use that as our far distance to ensure everything is visible.
	// Minimum distance is set to 5 cm, since humans rearly examin anything closer than that due to focusing issues.
	glm::mat4 Projection = glm::perspective((float)(67.5 * 2.0 * M_PI / 360.0), screenWidth / (float)screenHeight, 0.125f, 16384.0f);

	return Projection;
}


glm::mat4 GLModule::calculateFromWorldToViewMatrix()
{
	glm::vec3 Translate = glm::vec3(physicsModule->cameraX_position, physicsModule->cameraY_position, physicsModule->cameraZ_position);
	glm::vec3 Rotate = glm::vec3(physicsModule->cameraX_rotation_angle, physicsModule->cameraY_rotation_angle, 0.0);


	glm::mat4 View;
	View = glm::rotate(View, Rotate.y, glm::vec3(1.0f, 0.0f, 0.0f));
	View = glm::rotate(View, Rotate.x, glm::vec3(0.0f, 1.0f, 0.0f));
	View = glm::translate(View, -Translate);

	return View;
}

void GLModule::changeOffsetten(float amount)
{
	warpOffsetten += amount;
	std::cout << warpOffsetten << std::endl;
}

void GLModule::warp(float timeSinceRealFrame)
{
	if (fromLastRenderViewToWorldMatrixStale)
	{
		// the inverse matrix needs to be recalculated
		fromLastRenderViewToWorldMatrix = glm::inverse(fromWorldToLastRenderViewMatrix);
		fromLastRenderViewToWorldMatrixStale = false;
	}

	float diff = lastCameraXRotationAngle - physicsModule->cameraX_rotation_angle;

	if (diff > M_PI)
	{
		diff -= 2.0f * (float)M_PI;
	}
	else if (diff < -M_PI)
	{
		diff += 2.0f * (float)M_PI;
	}

	bool flip = diff > (float)M_PI / 2.0f || diff < (float)-M_PI / 2.0f;

	glm::mat4 fromWorldToViewMatrix = calculateFromWorldToViewMatrix();

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	GLenum bufs[] = { GL_COLOR_ATTACHMENT0 };
	glDrawBuffers(1, bufs);

	glUseProgram(warpShaderProgram);
	{
		glUniformMatrix4fv(warpFromProjectionToViewMatrixUniformLocation, 1, GL_FALSE, glm::value_ptr(fromProjectionToViewMatrix));
		glUniformMatrix4fv(warpFromLastRenderViewToWorldMatrixUniformLocation, 1, GL_FALSE, glm::value_ptr(fromLastRenderViewToWorldMatrix));

		glUniformMatrix4fv(warpFromWorldToViewMatrixUniformLocation, 1, GL_FALSE, glm::value_ptr(fromWorldToViewMatrix));
		glUniformMatrix4fv(warpFromViewToProjectionMatrixUniformLocation, 1, GL_FALSE, glm::value_ptr(fromViewToProjectionMatrix));

		glUniform2i(warpGridSizeUniformLocation, warpGridWidth, warpGridHeight);
		glUniform1i(warpFlipGridBorderUniformLocation, flip);

		glUniform1f(warpTimeSinceRealFrameUniformLocation, timeSinceRealFrame);
		glUniform1f(warpOffsettenUniformLocation, warpOffsetten);
		

		glBindVertexArray(warpVertexArrayObject);
		{
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, warpIndexBufferObject);
			{
				glUniform1i(warpCachedColorTextureUniformLocation, 0);
				glUniform1i(warpCachedVelocityTextureUniformLocation, 1);
				glUniform1i(warpCachedDepthTextureUniformLocation, 2);

				glActiveTexture(GL_TEXTURE0); glBindTexture(GL_TEXTURE_2D, offscreenColorTextureObject);
				glActiveTexture(GL_TEXTURE1); glBindTexture(GL_TEXTURE_2D, offscreenVelocityTextureObject);
				glActiveTexture(GL_TEXTURE2); glBindTexture(GL_TEXTURE_2D, offscreenDepthTextureObject);
				{
					glDrawElements(GL_PATCHES, warpIndexCount, GL_UNSIGNED_INT, 0);
				}
				glActiveTexture(GL_TEXTURE2); glBindTexture(GL_TEXTURE_2D, 0);
				glActiveTexture(GL_TEXTURE1); glBindTexture(GL_TEXTURE_2D, 0);
				glActiveTexture(GL_TEXTURE0); glBindTexture(GL_TEXTURE_2D, 0);
			}
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
		}
		glBindVertexArray(0);
	}
	glUseProgram(0);
}

void GLModule::setPolygonMode(bool wireframeMode)
{
	this->wireframeMode = wireframeMode;

	if (wireframeMode)
	{
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	}
	else
	{
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	}
}

void GLModule::togglePolygonMode()
{
	setPolygonMode(!this->wireframeMode);
}

void GLModule::printProgramLog(GLuint program)
{
	//Make sure name is shader
	if (glIsProgram(program))
	{
		//Program log length
		int infoLogLength = 0;
		int maxLength = infoLogLength;

		//Get info string length
		glGetProgramiv(program, GL_INFO_LOG_LENGTH, &maxLength);

		//Allocate string
		char* infoLog = new char[maxLength];

		//Get info log
		glGetProgramInfoLog(program, maxLength, &infoLogLength, infoLog);
		if (infoLogLength > 0)
		{
			//Print Log
			printf("%s\n", infoLog);
		}

		//Deallocate string
		delete[] infoLog;
	}
	else
	{
		printf("Name %d is not a program\n", program);
	}
}

void GLModule::printShaderLog(GLuint shader)
{
	//Make sure name is shader
	if (glIsShader(shader))
	{
		//Shader log length
		int infoLogLength = 0;
		int maxLength = infoLogLength;

		//Get info string length
		glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &maxLength);

		//Allocate string
		char* infoLog = new char[maxLength];

		//Get info log
		glGetShaderInfoLog(shader, maxLength, &infoLogLength, infoLog);
		if (infoLogLength > 0)
		{
			//Print Log
			printf("%s\n", infoLog);
		}

		//Deallocate string
		delete[] infoLog;
	}
	else
	{
		printf("Name %d is not a shader\n", shader);
	}
}
