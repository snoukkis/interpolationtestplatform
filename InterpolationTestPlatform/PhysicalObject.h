#pragma once

#define GLM_SWIZZLE
#include <glm/glm.hpp>
#include <vector>

class PhysicalObject
{
public:

	virtual ~PhysicalObject() {}

	// for a given state, evaluate f(X,t)
	virtual std::vector<glm::dvec3> evalF(std::vector<glm::dvec3> state) = 0;
	
	std::vector<glm::dvec3> getState();
	void setState(std::vector<glm::dvec3> newState);
	virtual void resetState() = 0;
	
	virtual void shiftSystem(bool forward) = 0;

protected:

	std::vector<glm::dvec3> state; //length 2*numParticles   x, v, x, v, ...

	double mass;
	double kDrag;
};
