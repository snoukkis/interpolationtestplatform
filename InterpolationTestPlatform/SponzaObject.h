#pragma once

#include "PhysicalObject.h"

#include "ObjObject.h"

class SponzaObject : public ObjObject
{
public:
	SponzaObject();
	virtual ~SponzaObject();
	void changeAspectRatio(float aspectRatio);
private:
	float aspectRatio = 1.0f;

	virtual glm::vec3 getScale();
	virtual glm::mat4 getRotation();
	virtual glm::vec3 getPosition();
	virtual glm::vec3 getVelocity();

	virtual std::string getFilePath();
	virtual std::string getFileName();

	virtual bool shouldOverlay();
};
