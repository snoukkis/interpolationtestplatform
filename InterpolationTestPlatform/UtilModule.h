#pragma once

#include <SDL.h>
#include "SDL_image.h"
#include <SDL_ttf.h> 
#include <string>

typedef SDL_Color UtilColor;

class UtilModule
{
public:
	UtilModule();
	~UtilModule();

	static const char* readFile(const char* filename);
	static Uint32* loadImage(std::string filename, int& width, int& height);
	static Uint32* makeText(std::string text, UtilColor textColor, int& width, int& height, int size, int wrapPixels = 600);
	static std::string getDateTimeString();
private:
	static Uint32* surfaceToTexture(SDL_Surface* surface);
};
