#pragma once

#include "SimplePhysicalObject.h"
#include "PhysicalObject.h"
#include "Integrator.h"
#include "Timer.h"

class PhysicsModule
{
public:
	PhysicsModule(SimplePhysicalObject& simplePhysicalObject, PhysicalObject& physicalObject);
	~PhysicsModule();

	//Per frame update
	bool update(double frametime);

	void shiftSystem(bool forward);

	float camera_rotation_sensitivity = 0.0016f;
	float cameraX_rotation_angle;
	float cameraY_rotation_angle;

	float movementSpeed = 6.0f; // m/s
	float cameraX_position;
	float cameraY_position;
	float cameraZ_position;

	void resetPhysics();
	void resetCamera();

private:
	enum IntegratorMode
	{
		Euler,
		Trapezoidal,
		RungeKutta
	};

	void resetSystem(IntegratorMode integratorMode);
	void PhysicsModule::stepSystem();

	IntegratorMode integratorMode = Euler;

	SimplePhysicalObject&     simplePhysicalObject;
	PhysicalObject&     physicalObject;
	Integrator*         integrator = 0;
	double               timeBuffer = 0;
	double               timeStep = 0.01f; // seconds
};

