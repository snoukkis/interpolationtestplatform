#pragma once

#define GLM_SWIZZLE
#include <glm/glm.hpp>
#include <vector>
#include "PhysicalObject.h"

class Integrator
{
public:
	Integrator(double timeStep, PhysicalObject& physicalObject) : timeStep(timeStep), physicalObject(physicalObject) { }
	virtual ~Integrator() { }

	virtual void stepSystem() = 0; //includes func pointer call back to evalF of psystem

	PhysicalObject& physicalObject;
	double timeStep;
};
