#include "IntegratorEuler.h"

void EulerIntegrator::stepSystem()
{
	std::vector<glm::dvec3> state = physicalObject.getState();

	std::vector<glm::dvec3> f = physicalObject.evalF(state);
	std::vector<glm::dvec3> newState(physicalObject.getState().size());

	for (unsigned stateIndex = 0; stateIndex < newState.size(); stateIndex++)
	{
		newState[stateIndex] = physicalObject.getState()[stateIndex] + timeStep * f[stateIndex];
	}

	physicalObject.setState(newState);
}
