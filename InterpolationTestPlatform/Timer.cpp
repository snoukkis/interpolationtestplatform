#include "Timer.h"
#include <SDL.h>

#include <iostream>

Timer::Timer()
{
	previous = SDL_GetPerformanceCounter();
}

Timer::Timer(Uint64 referenceValue)
{
	previous = referenceValue;
}

double Timer::restartAt(Uint64 current, double minimumIncrementInSeconds)
{
	double systemTime = peekAt(current);
	
	double elapsed;

	if (minimumIncrementInSeconds <= 0.0)
	{
		elapsed = systemTime;
	}
	else
	{
		if (systemTime < 0.0)
		{
			// this case is possible if we have skipped into the future and are catching up... in that case advance time the minimum amount

			// std::cout << (systemTime * 1000.0) << std::endl;

			elapsed = minimumIncrementInSeconds;
		}
		else
		{
			elapsed = ((Uint64)(systemTime / minimumIncrementInSeconds) + 1) * minimumIncrementInSeconds;

			if (elapsed < 0 || elapsed > 10)
			{
				elapsed += 0;
			}
		}
	}

	Uint64 elapsedTicks = (Uint64)(elapsed * SDL_GetPerformanceFrequency() + 0.5);

	previous += elapsedTicks;
	if (paused)
	{
		idle = previous;
	}

	return elapsed;
}

double Timer::restartToAtLeastMoreThan(double minimumIncrementInSeconds)
{
	return restartAt(SDL_GetPerformanceCounter(), minimumIncrementInSeconds);
}

double Timer::restart()
{
	return restartAt(SDL_GetPerformanceCounter(), 0.0);
}

double Timer::restartRelativeTo(Timer now)
{
	return restartAt(now.previous, 0.0);
}

double Timer::peekAt(Uint64 current)
{
	double elapsed = 0.0;

	bool invert;

	Uint64 ticks;
	if (paused)
	{
		if (idle >= previous)
		{
			ticks = idle - previous;
			invert = false;
		}
		else
		{
			ticks = previous - idle;
			invert = true;
		}
	}
	else
	{
		if (current >= previous)
		{
			ticks = current - previous;
			invert = false;
		}
		else
		{
			ticks = previous - current;
			invert = true;
		}
	}

	Uint64 frequency = SDL_GetPerformanceFrequency();
	elapsed = ticks / (double)frequency;

	elapsed *= invert ? -1.0 : 1.0;

	return elapsed;
}

double Timer::peek()
{
	return peekAt(SDL_GetPerformanceCounter());
}

double Timer::peekRelativeTo(Timer now)
{
	return peekAt(now.previous);
}

void Timer::pauseAt(Uint64 current)
{
	if (!paused)
	{
		paused = true;

		idle = current;
	}
}

void Timer::pause()
{
	return pauseAt(SDL_GetPerformanceCounter());
}

void Timer::pauseRelativeTo(Timer now)
{
	return pauseAt(now.previous);
}

void Timer::resumeAt(Uint64 current)
{
	if (paused)
	{
		paused = false;

		previous += current - idle;
	}
}

void Timer::resume()
{
	return resumeAt(SDL_GetPerformanceCounter());
}

void Timer::resumeRelativeTo(Timer now)
{
	return resumeAt(now.previous);
}
