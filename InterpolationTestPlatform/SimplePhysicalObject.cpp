#include "SimplePhysicalObject.h"

#include <limits>
#include <algorithm>
#include <iostream>
#include <iterator>

void SimplePhysicalObject::shiftSystem(bool forward)
{
	if (forward)
	{
		position.y += 1.0f;
	}
	else
	{
		position.y -= 1.0f;
	}
}

SimplePhysicalObject::Intersection SimplePhysicalObject::intersectsAt(glm::dvec2 position, glm::dvec2 velocity, glm::dvec2 p0, glm::dvec2 p1)
{
	Intersection ret;

	if (p0.x == p1.x && p0.y != p1.y)
	{
		ret.axis = Intersection::Axis::VERTICAL;
	}
	else if (p0.x != p1.x && p0.y == p1.y)
	{
		ret.axis = Intersection::Axis::HORIZONTAL;
	}
	else
	{
		assert(true);
	}

	glm::dvec2 pd = p1 - p0;
	glm::dvec2 posidiff = p0 - position;

	double dividend = pd.y * posidiff.x - pd.x * posidiff.y;
	double divisor = pd.y * velocity.x - pd.x * velocity.y;
	
	ret.time = dividend / divisor;

	if (divisor == 0)
	{
		if (dividend == 0)
		{
			ret.result = Intersection::Result::FOREVER;
		}
		else
		{
			ret.result = Intersection::Result::NEVER;
		}
	}
	else if (ret.time > 0)
	{
		ret.result = Intersection::Result::FUTURE;
	}
	else
	{
		ret.result = Intersection::Result::PAST;
	}

	return ret;
}

bool SimplePhysicalObject::stepSystem(double frametime)
{
	bool bounce = false;

	int debugcounter = 0;

	while (frametime > 0.0)
	{
		std::vector<Intersection> intersections;
		std::vector<Intersection> usefulIntersections;

		Intersection intersectionAtNegativeHorizontal = intersectsAt(position, velocity, glm::dvec2(limitMin.x, limitMin.y), glm::dvec2(limitMax.x, limitMin.y));
		Intersection intersectionAtPositiveHorizontal = intersectsAt(position, velocity, glm::dvec2(limitMin.x, limitMax.y), glm::dvec2(limitMax.x, limitMax.y));
		Intersection intersectionAtNegativeVertical   = intersectsAt(position, velocity, glm::dvec2(limitMin.x, limitMin.y), glm::dvec2(limitMin.x, limitMax.y));
		Intersection intersectionAtPositiveVertical   = intersectsAt(position, velocity, glm::dvec2(limitMax.x, limitMin.y), glm::dvec2(limitMax.x, limitMax.y));

		intersections.push_back(intersectionAtNegativeHorizontal);
		intersections.push_back(intersectionAtPositiveHorizontal);
		intersections.push_back(intersectionAtNegativeVertical);
		intersections.push_back(intersectionAtPositiveVertical);

		std::copy_if(
			intersections.begin(),
			intersections.end(),
			std::back_inserter(usefulIntersections),
			[](const Intersection& intersection)
			{
				return intersection.result == Intersection::Result::FUTURE;
			}
		);

		std::sort(
			usefulIntersections.begin(),
			usefulIntersections.end(),
			[](const Intersection & a, const Intersection & b) -> bool
			{
				return a.time < b.time;
			}
		);

		//std::cout << "stepSystem... debugcounter=" << debugcounter << "... usefulIntersections=" << usefulIntersections.size() << "..." << std::endl;

		if (usefulIntersections.size() > 0)
		{
			Intersection firstIntersection = usefulIntersections[0];

			bool collision = frametime >= firstIntersection.time;
			double simulationTime = glm::min(firstIntersection.time, frametime);

			position += velocity * simulationTime;
			frametime -= simulationTime;

			// fix rounding errors
			if (position.x < glm::min(limitMax.x, limitMin.x))
			{
				position.x = glm::min(limitMax.x, limitMin.x);
			}
			else if (position.x > glm::max(limitMax.x, limitMin.x))
			{
				position.x = glm::max(limitMax.x, limitMin.x);
			}
			// fix rounding errors
			if (position.y < glm::min(limitMax.y, limitMin.y))
			{
				position.y = glm::min(limitMax.y, limitMin.y);
			}
			else if (position.y > glm::max(limitMax.y, limitMin.y))
			{
				position.y = glm::max(limitMax.y, limitMin.y);
			}

			if (collision)
			{
				bounce = true;

				if (firstIntersection.axis == Intersection::Axis::VERTICAL)
				{
					velocity.x *= -1.0;

					if (usefulIntersections.size() > 1 && usefulIntersections[1].time == firstIntersection.time)
					{
						// corner case (literally ;)
						velocity.y *= -1.0;
					}
				}
				else // if (firstIntersection.axis == Intersection::Axis::HORIZONTAL)
				{
					velocity.y *= -1.0;

					if (usefulIntersections.size() > 1 && usefulIntersections[1].time == firstIntersection.time)
					{
						// corner case (literally ;)
						velocity.x *= -1.0;
					}
				}
			}
		}
		else
		{
			// must be stationary... do nothing
			frametime = 0;
		}
	}

	return bounce;
}
