#version 400

layout(quads, equal_spacing, ccw) in;

in PatchData
{
	uvec2 gridIndex;
} PatchIn[];

out VertexData
{
	vec3 viewPosition;
	vec2 texCoord;
} VertexOut;

out VertexFlatData
{
	flat bool stickyPosX;
	flat bool stickyNegX;
	flat bool stickyPosY;
	flat bool stickyNegY;
} VertexFlatOut;

uniform sampler2D cachedColor;
uniform sampler2D cachedVelocity;
uniform sampler2D cachedDepth;

uniform mat4 fromProjectionToViewMatrix;
uniform mat4 fromLastRenderViewToWorldMatrix;

uniform mat4 fromWorldToViewMatrix;
uniform mat4 fromViewToProjectionMatrix;

uniform ivec2 gridSize;
uniform bool flipGridBorder;

uniform float timeSinceRealFrame;

vec2 interpolate(vec2 bl, vec2 br, vec2 tr, vec2 tl)
{
	float u = gl_TessCoord.x;
	float v = gl_TessCoord.y;
	
	vec2 b = mix(bl,br,u);
	vec2 t = mix(tl,tr,u);
	return mix(b,t,v);
}

void main()
{
	ivec2 screenSize = textureSize(cachedDepth, 0);
	int multiplier = screenSize.x / gridSize.x;
	
	vec2 texCoord;
	texCoord = interpolate(PatchIn[0].gridIndex, PatchIn[1].gridIndex, PatchIn[2].gridIndex, PatchIn[3].gridIndex); // 0.0 ... 16.0
	
	ivec2 unormTexCoord = ivec2(texCoord + vec2(0.5, 0.5));
	
	VertexFlatOut.stickyPosX = unormTexCoord.x == screenSize.x;
	VertexFlatOut.stickyNegX = unormTexCoord.x == 0;
	VertexFlatOut.stickyPosY = unormTexCoord.y == screenSize.y;
	VertexFlatOut.stickyNegY = unormTexCoord.y == 0;
	
	texCoord += 0.5; // 0.5 ... 16.5
	
	texCoord.x /= (screenSize.x); // 0.03125 ... 1.03125
	texCoord.y /= (screenSize.y); // 0.03125 ... 1.03125
	
	/*
	// 0.0 ... 1.0
	if(VertexFlatOut.stickyNegX)
	{
		texCoord.x = 0.0;
	}
	else if(VertexFlatOut.stickyPosX)
	{
		texCoord.x = 1.0;
	}
	
	// 0.0 ... 1.0
	if(VertexFlatOut.stickyNegY)
	{
		texCoord.y = 0.0;
	}
	else if(VertexFlatOut.stickyPosY)
	{
		texCoord.y = 1.0;
	}
	*/
	
	vec3 normalizedDevicePosition = vec3(texCoord * 2.0 - 1.0, 0.0);
	
	unormTexCoord.x = clamp(unormTexCoord.x, 0, screenSize.x - 1);
	unormTexCoord.y = clamp(unormTexCoord.y, 0, screenSize.y - 1);
	normalizedDevicePosition.z = texelFetch(cachedDepth, unormTexCoord, 0).r * 2.0 - 1.0;
	
	VertexOut.texCoord = texCoord;
	
	vec4 hViewPosition = fromProjectionToViewMatrix * vec4(normalizedDevicePosition, 1.0);
	VertexOut.viewPosition = hViewPosition.xyz / hViewPosition.w;
	vec4 hWorldPosition = fromLastRenderViewToWorldMatrix * hViewPosition;
	vec3 velocity = texelFetch(cachedVelocity, unormTexCoord, 0).xyz;
	hWorldPosition.xyz += timeSinceRealFrame * velocity * hWorldPosition.w;
	gl_Position = fromViewToProjectionMatrix * fromWorldToViewMatrix * hWorldPosition;
}
