#pragma once

#include <sstream>

#include <SDL.h>
#include "SDL_mixer.h"

#include "WorldcubeObject.h"
#include "SponzaObject.h"
#include "BouncyObject.h"

#include "GameModule.h"
#include "PhysicsModule.h"
#include "GLModule.h"

class SDLModule
{
public:

	SDLModule(int argc, char* argv[]);

	//Frees media and shuts down SDL
	~SDLModule();

	void mainLoop();

private:
	enum GameState
	{
		INTERLUDE,
		AIMING,
		REGISTERING,
		ENDED
	};

	Timer appLifetimeTimer;
	Timer masterFrameTimer;
	Timer physicsUpdateTimer;

	GameState gameState = INTERLUDE;
	GLModule::FeedbackType feedbackType = GLModule::FeedbackType::INVISIBLE;
	Timer gameStateTimer;
	Shot shot;
	glm::dvec3 worldOuterPositionMin;
	glm::dvec3 worldOuterPositionMax;
	glm::dvec3 worldInnerPositionMin;
	glm::dvec3 worldInnerPositionMax;
	double worldPositionDepth = 0.958008;

	int frameSkipCounter = 1;
	bool forceRealFrame = false;

	GameModule* gameModule;
	PhysicsModule* physicsModule;
	GLModule* glModule;

	int screenWidth = 640;
	int screenHeight = 480;
	int refreshRate = 0;

	// init parameters are valid only during app initialization... these values can be overridden later.
	int initTargetFps = 0;

	int frameSkip = 1; // render scene only during every Nth frame
	bool drawHudInfo = false;
	bool inverseY = false;
	bool interpolate = true;
	bool interpolationInputUpdating = true;
	bool wireframe = false;
	bool fullscreen = true;
	bool vsync = true;
	bool captureMouse = true;
	GLubyte hitId = 0;
	bool developerMode = false;

	Mix_Music *music = NULL;
	Mix_Chunk *hitSound = NULL;
	Mix_Chunk *missSound = NULL;
	Mix_Chunk *timeoutSound = NULL;
	Mix_Chunk *bounceSound = NULL;
	Mix_Chunk *gunSound = NULL;

	void setScreenWidth(int newValue) { screenWidth = newValue; onHudInfoChanged(); }
	void setScreenHeight(int newValue) { screenHeight = newValue; onHudInfoChanged(); }
	void setRefreshRate(int newValue) { refreshRate = newValue; onHudInfoChanged(); }
	void setInitTargetFps(int newValue) { initTargetFps = newValue; onHudInfoChanged(); }
	void setFrameSkip(int newValue) { frameSkip = newValue; onHudInfoChanged(); }
	void setDrawHudInfo(bool newValue) { drawHudInfo = newValue; onHudInfoChanged(); }
	void setInverseY(bool newValue) { inverseY = newValue; onHudInfoChanged(); }
	void setInterpolate(bool newValue) { interpolate = newValue; onHudInfoChanged(); }
	void setInterpolationInputUpdating(bool newValue) { interpolationInputUpdating = newValue; onHudInfoChanged(); }
	void setWireframe(bool newValue) { wireframe = newValue; onHudInfoChanged(); }
	void setFullscreen(bool newValue) { fullscreen = newValue; onHudInfoChanged(); }
	void setVsync(bool newValue) { vsync = newValue; onHudInfoChanged(); }
	void setCaptureMouse(bool newValue) { captureMouse = newValue; onHudInfoChanged(); }
	void setHitId(GLubyte newValue) { hitId = newValue; onHudInfoChanged(); }
	void setDeveloperMode(bool newValue) { developerMode = newValue; onHudInfoChanged(); }

	int getScreenWidth() { return screenWidth; }
	int getScreenHeight() { return screenHeight; }
	int getRefreshRate() { return refreshRate; }
	int getInitTargetFps() { return initTargetFps; }
	int getFrameSkip() { return frameSkip; }
	bool getDrawHudInfo() { return drawHudInfo; }
	bool getInverseY() { return inverseY; }
	bool getInterpolate() { return interpolate; }
	bool getInterpolationInputUpdating() { return interpolationInputUpdating; }
	bool getWireframe() { return wireframe; }
	bool getFullscreen() { return fullscreen; }
	bool getVsync() { return vsync; }
	bool getCaptureMouse() { return captureMouse; }
	GLubyte getHitId() { return hitId; }
	bool getDeveloperMode() { return developerMode; }

	bool hudInfoChanged = true;
	void onHudInfoChanged();

	void readArgs(int argc, char* argv[]);
	//Starts up SDL, creates window, and initializes OpenGL
	bool init();

	//The window we'll be rendering to
	SDL_Window* gWindow = NULL;

	//OpenGL context
	SDL_GLContext gContext;

	bool quit = false;
	bool pause = false;

	enum FireState
	{
		NOT_FIRED,
		FIRED,
		HIT,
		MISS
	};
	FireState fireState = FireState::NOT_FIRED;

	WorldcubeObject worldcubeObject;
	SponzaObject sponzaObject;
	BouncyObject bouncyObject;

	std::vector<DrawableObject*> drawableObjects;

	std::string reportFileName;
	std::string logFileName;

	std::ofstream logFile;
	std::stringstream logBuffer;


	void getWindowSize(int &w, int &h);
	void updateWindowSize();

	int getNearestRefreshRateMultiplier(int fps);

	void handleEvents();

	void updateCamera(double time);

	void updateGameState();

	void resetScene(glm::dvec2 limitMin, glm::dvec2 limitMax, glm::dvec2 position, double depth, glm::dvec2 velocity, double diameter);
	void resetCamera();
	void resetGame();
	void calculateCorners();

	std::string hitIdToString(GLubyte hitId);
	
	bool hitTarget(GLubyte hitId);

	std::string getInfoString();
};
