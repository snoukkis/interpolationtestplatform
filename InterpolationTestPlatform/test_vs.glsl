#version 150

#define NUM_LIGHTS %NUM_LIGHTS%

in vec3 vertexPositionAttribute;
in vec3 vertexNormalAttribute;
in vec3 vertexTexcoordAttribute;
in vec2 vertexTexcoordScaleAttribute;

in vec3 materialEmissionAttribute;
in vec3 materialAmbientAttribute;
in vec3 materialDiffuseAttribute;
in vec3 materialSpecularAttribute;
in float materialShininessAttribute;

uniform vec3 velocity;

struct Transformations
{
	mat4 fromModelToViewNormal;

	mat4 fromModelToView;
	mat4 fromViewToProjection;
};
uniform Transformations transformations;

struct Textures
{
	// sampler2DArray normal;
	// sampler2DArray ambient;
	sampler2DArray diffuse;
	samplerCube sky;
	// sampler2DArray specular;
};
uniform Textures textures;

struct Light
{
	vec3 position;

	vec3 ambient;
	vec3 diffuse;
	vec3 specular;
};
uniform Light lights[NUM_LIGHTS];
uniform Light light;

out Vertex
{
    vec3 position;
    vec3 normal;
    vec3 texcoord;
    vec2 texcoordScale;
} vertexOut;

out Material
{
    vec3 emission;
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
    float shininess;
} materialOut;

uniform uint hitId;

void main()
{
	gl_Position = transformations.fromModelToView * vec4(vertexPositionAttribute, 1.0);
	vertexOut.position = gl_Position.xyz / gl_Position.w;
	gl_Position = transformations.fromViewToProjection * gl_Position;

	vertexOut.normal = vec3(transformations.fromModelToViewNormal * vec4(normalize(vertexNormalAttribute), 0.0));

	vertexOut.texcoord = vertexTexcoordAttribute;
	vertexOut.texcoordScale = vertexTexcoordScaleAttribute;

	materialOut.emission = materialEmissionAttribute;
	materialOut.ambient = materialAmbientAttribute;
	materialOut.diffuse = materialDiffuseAttribute;
	materialOut.specular = materialSpecularAttribute;
	materialOut.shininess = materialShininessAttribute;
}
