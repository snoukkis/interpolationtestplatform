#pragma once

#define GLM_SWIZZLE
#include <glm/glm.hpp>

class Forces
{
public:

    // Gravity divided by mass.
	static glm::dvec3 evaluateGravity() 
    {
		return glm::dvec3(0,-9.8,0);
	}

	static glm::dvec3 evaluateDrag(glm::dvec3 velocity, double mass, double dragConstant) 
    {
		return (-dragConstant * velocity) / mass;
	}

	static glm::dvec3 evaluateSpring(double dist0, double k, double nodeMass, glm::dvec3 node, glm::dvec3 neighbor) 
    {
		glm::dvec3 distanceFromNeighbor = node - neighbor;
		double distanceFromNeighborLength = distanceFromNeighbor.length();
		distanceFromNeighbor = glm::normalize(distanceFromNeighbor);

		return ((-k * (distanceFromNeighborLength - dist0)) * distanceFromNeighbor) / nodeMass;
	}
};
