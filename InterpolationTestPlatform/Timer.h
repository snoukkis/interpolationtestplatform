#pragma once

#include <SDL.h>

// unit is seconds
class Timer
{
	Uint64 previous;
	Uint64 idle;
	bool paused;

	double peekAt(Uint64 current);
	double restartAt(Uint64 referenceValue, double minimumIncrementInSeconds);
	void pauseAt(Uint64 current);
	void resumeAt(Uint64 current);

public:

	Timer();
	Timer(Uint64 referenceValue);

	double peek();
	double peekRelativeTo(Timer now);
	
	double restart();
	double restartToAtLeastMoreThan(double minimumIncrementInSeconds);
	double restartRelativeTo(Timer now);
	
	void pause();
	void pauseRelativeTo(Timer now);

	void resume();
	void resumeRelativeTo(Timer now);
};
