#include "IntegratorTrapezoidal.h"

void TrapezoidalIntegrator::stepSystem()
{
	std::vector<glm::dvec3> f0 = physicalObject.evalF(physicalObject.getState());
	std::vector<glm::dvec3> tempState(physicalObject.getState().size());

	for (unsigned stateIndex = 0; stateIndex < tempState.size(); stateIndex++)
	{
		tempState[stateIndex] = physicalObject.getState()[stateIndex] + timeStep * f0[stateIndex];
	}

	std::vector<glm::dvec3> f1 = physicalObject.evalF(tempState);
	std::vector<glm::dvec3> newState(physicalObject.getState().size());

	for (unsigned stateIndex = 0; stateIndex < newState.size(); stateIndex++)
	{
		newState[stateIndex] = physicalObject.getState()[stateIndex] + 0.5f * timeStep * f0[stateIndex] + 0.5f * timeStep * f1[stateIndex];
	}

	physicalObject.setState(newState);
}
