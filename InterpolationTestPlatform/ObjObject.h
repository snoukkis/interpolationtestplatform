#pragma once

#define GLM_SWIZZLE
#include <glm/glm.hpp>
#include <gl\glew.h>
#include <vector>

#include "DrawableObject.h"

#include "tiny_obj_loader.h"

#include "GLStructs.h"

class ObjObject : public DrawableObject
{
public:
	virtual void initDraw(AttributeLocations& attributeLocations);
	virtual ~ObjObject();
	
protected:

	virtual void drawDerived(const RenderUniformLocations& uniformLocations, const glm::mat4& fromWorldToLastRenderViewMatrix);

	virtual glm::vec3 getScale() = 0;
	virtual glm::mat4 getRotation() = 0;
	virtual glm::vec3 getPosition() = 0;
	virtual glm::vec3 getVelocity() = 0;

	virtual std::string getFilePath() = 0;
	virtual std::string getFileName() = 0;

	glm::vec3 objMinPosition;
	glm::vec3 objMaxPosition;
private:
	void loadObj(std::string path, std::string objFileName);
	void loadTextures(std::string path, const std::vector<tinyobj::material_t>& materials);

	virtual bool shouldOverlay() = 0;

	GLuint objVertexArrayObject = 0;
	GLuint objVertexBufferObject = 0;
	std::vector<Vertex> objVertices;
	GLsizei objVerticesCount = 0;
	std::map <int, Texture> objTextureLayers; // map material_id in mesh to texture array layer
	GLuint objTextureObject = 0;
};
