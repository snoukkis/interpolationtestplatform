#pragma once

#include "GLStructs.h"

class DrawableObject
{
public:
	DrawableObject();
	virtual ~DrawableObject() {}
	virtual void initDraw(AttributeLocations& attributeLocations) = 0;
	void draw(const RenderUniformLocations& uniformLocations, const glm::mat4& fromWorldToLastRenderViewMatrix);
	GLubyte getHitId();

	void setDrawEnabled(bool enabled);
	bool getDrawEnabled();
protected:
	virtual void drawDerived(const RenderUniformLocations& uniformLocations, const glm::mat4& fromWorldToLastRenderViewMatrix) = 0;

private:
	bool drawEnabled = true;

	GLubyte hitId;
	static GLubyte nextUnusedHitId;
};
