#include "BouncyObject.h"
#include "Forces.h"

#include <iostream>

BouncyObject::BouncyObject()
{
	kDrag = 1.0;
	mass = 240.0;

	// these make very little sense... but let's just initialize with something
	resetState(glm::dvec2(0.0, 0.0), glm::dvec2(1.0, 1.0), glm::dvec2(0.0, 0.0), 0, glm::dvec2(0.0, 0.0), 1);
	resetState();
}

BouncyObject::~BouncyObject()
{
}

std::vector<glm::dvec3> BouncyObject::evalF(std::vector<glm::dvec3> oldState)
{
	std::vector<glm::dvec3> changes = std::vector<glm::dvec3>(2);

	glm::dvec3 oldPosition = oldState[0];
	glm::dvec3 oldVelocity = oldState[1];

	glm::dvec3& positionChange = changes[0];
	glm::dvec3& velocityChange = changes[1];

	// POSITION CHANGE
	positionChange = oldVelocity;

	// TOTAL FORCE
	velocityChange = glm::dvec3(0.0f, 0.0f, 0.0f);
	velocityChange += Forces::evaluateGravity();
	velocityChange += Forces::evaluateDrag(oldVelocity, mass, kDrag);

	if (oldPosition.y <= 0.0)
	{
		float hardGroundSpringConstant = 10000.0f; // we are simulating hard ground, so this needs to be BIG


		velocityChange.y -= (hardGroundSpringConstant * oldPosition.y);
	}

	return changes;
}

void BouncyObject::resetState()
{
	state.clear();

	// initial position
	state.push_back(glm::dvec3(0.0, position.y, position.x));

	// initial velocity
	state.push_back(glm::dvec3(0.0, velocity.y, velocity.x));
}

void BouncyObject::resetState(glm::dvec2 limitMin, glm::dvec2 limitMax, glm::dvec2 position, double depth, glm::dvec2 velocity, double radius)
{
	this->radius = radius;

	this->limitMin = limitMin;
	this->limitMax = limitMax;

	// initial position
	this->position = position;
	this->z = depth;

	// initial velocity
	this->velocity = velocity;

	// precompute initial bounce if on border because it's assumed that velocity is updated as soon as intersection happens
	if ((position.x == limitMin.x && velocity.x < 0.0) || (position.x == limitMax.x && velocity.x > 0.0))
	{
		velocity.x *= -1.0;
	}
	if ((position.y == limitMin.y && velocity.y < 0.0) || (position.y == limitMax.y && velocity.y > 0.0))
	{
		velocity.y *= -1.0;
	}
}

void BouncyObject::shiftSystem(bool forward)
{
	if (forward)
	{
		state[0].y += 1.0f;
	}
	else
	{
		state[0].y -= 1.0f;
	}
}

glm::vec3 BouncyObject::getScale()
{
	glm::vec3 scaleVec = objMaxPosition - objMinPosition;
	float scale = (float)(2.0 * radius / glm::max(scaleVec.x, glm::max(scaleVec.y, scaleVec.z)));

	return glm::vec3(scale);
}

glm::mat4 BouncyObject::getRotation()
{
	glm::mat4 ret = glm::mat4(1.0f);  // return identity

	return ret;
}

glm::vec3 BouncyObject::getPosition()
{
	//return state[0] + glm::dvec3(0.0f, -objMinPosition.y, 0.0f) * glm::dvec3(getScale());
	return glm::dvec3(position.x, position.y, z) /*+ glm::dvec3(0.0f, -objMinPosition.y, 0.0f) * glm::dvec3(getScale())*/;
}

glm::vec3 BouncyObject::getVelocity()
{
	//return state[1];
	return glm::dvec3(velocity.x, velocity.y, 0.0);
}
std::string BouncyObject::getFilePath()
{
	return "models/sphere/";
}

std::string BouncyObject::getFileName()
{
	return "sphere";
}

bool BouncyObject::shouldOverlay()
{
	return true;
}
